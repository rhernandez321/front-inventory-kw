@extends('layouts.app')

@section('content')
<div id="content">

    @include('layouts.navbar')
    
    <div class="container-fluid p-md-4">  

        <div id="card-ppal" class="card mb-3">		        	
            <div class="card-header" id="cardd-hea">
                <h1>Presentations </h1>
            </div> 

            <div name= "card-body-ppal" class="card-body">
                <div name="row1-card-body-ppal" class="row">
                    <div name="col1-row1-card-body-ppal" class="col-5">                                                                              
                        <div class="form-group">
                            <form id="form-research" method="post" class="form-inline" action="">
                                <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                                <input  id="input_find_presentations" class="form-control" type="text" size="30" name="find_moneys"  placeholder="Name / Code / Description." value="{{$_data}}" />
                                <button id="btn-find-presentations" type="button" class="btn btn-info my-2 my-sm-0">
                                    <span class="fa fa-search  icon"></span>
                                </button>                                                       
                            </form>
                        </div>
                    </div>
                    
                    <div class="col-5">
                    </div> 

                    <div  id="div-btn-add" class="col-1"> 
                        <button id = "btn-add" type="button" class="btn btn-info" data-toggle="tooltip" title="Add presentations">
                            <span class="fa fa-plus" aria-hidden="true"></span>                                    
                        </button>                                                
                    </div>
                </div>             
            </div> <!--card-body-->
        </div>


        
        <!-----------------------------------Mostrar datos-------------------------------------->
        <div id="card-alloys" class="card mb-3">        
            <div class="card-header" id="cardd-hea">                                    
                <h5>Presentations</h5>
            </div>

            <div id="data" class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div id="div_table_presentations" class="table-responsive">                          
                        @if(!is_null($data_result))                                      
                            <table id="tablepresentations" class="table table-striped" cellspacing="0" width="100%">
                                <thead class="thead-dark">
                                    <tr>            
                                        <th>Id</th>     
                                        <th>Code</th>                                                             
                                        <th>Name</th>                                                                                               
                                        <th>Description</th>                                                
                                        <th>Enable</th>
                                        <th>Visible</th>
                                        <th></th>                                                                                                
                                    </tr>
                                </thead>
                        
                                <tfoot class="thead-dark">
                                    <tr>
                                        <th>Id</th>     
                                        <th>Code</th>                                                             
                                        <th>Name</th>                                                                                                                                               
                                        <th>Description</th>                                                
                                        <th>Enable</th>
                                        <th>Visible</th>
                                        <th></th>                                                                                                
                                    </tr>
                                </tfoot>
                            </table>
                            @endif                          
                        </div>
                    </div>             
                </div>  
            </div>
        </div>   
        <!-----------------------------------Mostrar datos-------------------------------------->
        
    </div><!--<div class="container-fluid">-->


    <!--------------------Modal Edit data------------------------------------------>        
    
    <div class="modal fade" id="modal-edit-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modall-he" >
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <h2 class="modal-title" id="exampleModalLabel">Presentations</h2>
                </div>
                <div id="edit-data" class="modal-body">                      
                    <form id="form-modal-edit-data">
                        <input  id="input-modal-edit-data-idpresentations" type="hidden" />
                        <div class="row">
                            <div class="col-8">
                                <div class="form-group">
                                    <label for="input-modal-edit-data-code" class="col-form-label">Presentations Code:</label>                                
                                    <input  id="input-modal-edit-data-code" class="form-control upper" type="text" size="45" maxlength="25"  placeholder="Presentations Code."/>
                                </div>
                            </div>
                            <div class="col-4">
                            </div>
                        </div>    
                        <div class="row">
                            <div class="col-8">
                                <div class="form-group">
                                    <label for="input-modal-edit-data-name" class="col-form-label">Presentations Name:</label>                                
                                    <input  id="input-modal-edit-data-name" class="form-control upper" type="text" size="45" maxlength="30"  placeholder="Presentations Name."/>
                                </div>
                            </div>
                            <div class="col-4">
                            </div>                                
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="input-modal-edit-data-description" class="col-form-label">Description:</label>                                
                                    <input  id="input-modal-edit-data-description" class="form-control" type="text" size="45" maxlength="30"  placeholder="Description."/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div id="div-modal-edit-select-enabled" class="form-group">
                                    <label for="modal-edit-enabled-select" class="col-form-label">Enabled</label> <br> 
                                    <select id="modal-edit-enabled-select" class="form-control input-sm">
                                        <option value="0">No</option> 
                                        <option value="1" selected>Yes</option>                                         
                                    </select>                                
                                </div>
                            </div>
                            <div class="col-6">
                                <div id="div-modal-edit-select-visible" class="form-group">
                                    <label for="modal-edit-visible-select" class="col-form-label">Visible</label> <br> 
                                    <select id="modal-edit-visible-select" class="form-control input-sm">
                                        <option value="0">No</option> 
                                        <option value="1" selected>Yes</option>                                    
                                    </select>
                                </div>
                            </div>
                        </div>
                   </form>
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-modal-edit-close" type="button" class="btn btn-danger btn-close" data-dismiss="modal">Close</button>
                    <button id="btt-modal-edit-save" type="button" class="btn btn-success">Save changes</button>
                    <button id="btt-modal-delete" type="button" class="btn btn-warning" style="display: none;">Delete</button>                      
                </div>
            </div>
        </div>
    </div>        
                                
    <!--------------------Fin Modal Edit------------------------------------------>
            
                    
    <!--------------------Modal add ------------------------------------------>        
    
    <div class="modal fade" id="modal-add-presentations" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
           <div class="modal-content">
               <div class="modal-header modall-he" >
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="exampleModalLabel">Presentations</h2>
                      
                </div>
                <div id="edit-data" class="modal-body">                      
                    <form id="form-modal-add-presentations" name="form_add_moneys" method="post" action="">
                        <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="input-modal-add-code" class="col-form-label">Presentations Code:</label>                                
                                    <input  id="input-modal-add-code" class="form-control upper" name="presentations_code" type="text" size="45" maxlength="25"  placeholder="Presentations Code." required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="input-modal-add-name" class="col-form-label">Presentations Name:</label>                                
                                    <input  id="input-modal-add-name" class="form-control upper" name="presentations_name" type="text" size="45" maxlength="30"  placeholder="Presentations Name." required/>
                                </div>
                            </div>                             
                        </div>     
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="input-modal-add-description" class="col-form-label">Description:</label>                                
                                    <input  id="input-modal-add-description" class="form-control" name="presentations_description" type="text" size="45" maxlength="30"  placeholder="Description." required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div id="div-modal-add-select-enabled" class="form-group">
                                    <label for="modal-add-enabled-select" class="col-form-label">Enabled</label> <br> 
                                    <select id="modal-add-enabled-select" name="presentations_enabled" class="form-control input-sm">
                                        <option value="0">No</option> 
                                        <option value="1" selected>Yes</option>                                         
                                    </select>                                
                                </div>
                            </div>
                            <div class="col-6">
                                <div id="div-modal-add-select-visible" class="form-group">
                                    <label for="modal-add-visible-select" class="col-form-label">Visible</label> <br> 
                                    <select id="modal-add-visible-select" name="presentations_visible" class="form-control input-sm">
                                        <option value="0">No</option> 
                                        <option value="1" selected>Yes</option>                                    
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-modal-add-close" type="button" class="btn btn-danger btn-close" data-dismiss="modal">Close</button>
                    <button id="btt-modal-add" type="button" class="btn btn-success">Add</button>                                            
                </div>
            </div>
        </div>
    </div>        
    
    <!--------------------Fin Modal add cities by countries------------------------------------------>


     <!--------------------Msg Modal------------------------------------------>             
     <div class="modal fade" id="modal-msg" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modall-he">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="exampleModalLabel">Inventory KW</h2>
                </div>
                <div id="error-msg" class="modal-body">
                   {{$error_msg}}
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-id-err-msg" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                      
                </div>
            </div>
        </div>
     </div>                   
    <!--------------------Fin Msg Modal------------------------------------------>       


    <!--------------------Modal confirm------------------------------------------>        
    <div class="modal fade" id="modal-confirm-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modall-he" >
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> 
                    <h2 class="modal-title" id="exampleModalLabel">Presentations</h2>
                </div>
                <div id="div-confirm-data-text" class="modal-body">                                                                     
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-modal-confirm-close" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button id="btt-modal-confirm" type="button" class="btn btn-success">Ok</button>                      
                </div>
            </div>
        </div>
    </div>                               
    <!--------------------Fin Modal confirm------------------------------------------>

</div> <!--div class="wrapper"--> 

@endsection

@section('scripts')
    <script src="{{ asset('js/jq/presentations_jq.js') }}" defer></script>                         
@show
