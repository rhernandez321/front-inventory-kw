@extends('layouts.app')

@section('content')
<div id="content">

    @include('layouts.navbar')
         
    <div class="container-fluid p-md-4">    

        <div id="card-ppal" class="card mb-3">            
            <div class="card-header" id="cardd-hea">
                <h1>Countries </h1>
            </div> 

            <div name= "card-body-ppal" class="card-body">
                <div name="row1-card-body-ppal" class="row">
                    <div name="col1-row1-card-body-ppal" class="col-5">                                            
                        <div class="form-group">
                            <form method="post" action="" class="form-inline">
                                <!--input name="_method" type="hidden" value="GET"-->
                                <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                                <input  id="input_find_country" class="form-control upper" type="search" size="30" name="find_country"  placeholder="Name / Code." value="{{ $_data ?? ''}}" /> 
                                <button id="btn-find-countries" class="btn btn-info my-2 my-sm-0" type="button">
                                    <span class="fa fa-search  icon"></span>
                                </button>
                            </form>
                        </div>
                    </div>

                    <div name="col2-row1-card-body-ppal" class="col-5">
                    </div>

                    <div name="col3-row1-card-body-ppal" class="col-1">                        
                        <button id = "btn-add-country" type="button" class="btn btn-info" data-toggle="tooltip" title="Add Country">
                            <span class="fa fa-plus" aria-hidden="true"></span>                                    
                        </button>                        
                    </div>             
                </div>             
            </div> <!--card-body-->
        </div>

        
        <!-----------------------------------Mostrar datos-------------------------------------->               
        <div id="countrie-data" class="card mb-3">
            <div id="cardd-hea" class="card-header">                                    
                <h3>Countries</h3>      
            </div>

            <div id="data" class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div id="div_table_coutries" class="table-responsive">                          
                            <!--table id="tbl_countries" class="table table-striped table-hover table-condensed"-->
                            <table id="tablecountries" class="table table-striped" cellspacing="0" width="100%">
                                <thead class="thead-dark">
                                    <tr>            
                                        <th>Id</th>
                                        <th>Country Code</th>
                                        <th>Name</th>
                                        <th>Enable</th>
                                        <th>Visible</th>
                                        <th></th>                                                                                                        
                                    </tr>
                                </thead>
                                <tfoot class="thead-dark">
                                    <tr>
                                        <th>Id</th>
                                        <th>Country Code</th>
                                        <th>Name</th>
                                        <th>Enable</th>
                                        <th>Visible</th>
                                        <th></th>                                                                                                        
                                    </tr>
                                </tfoot>
                            </table>                           
                        </div>
                    </div>        
                </div>        
            </div>
        </div>
        <!-----------------------------------Mostrar datos-------------------------------------->
        
    </div><!--<div class="container-fluid">-->
            
                    
    <!--------------------Modal Edit data------------------------------------------>        
    <div class="modal fade" id="modal-edit-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modall-he">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="exampleModalLabel">Countries</h2>
                </div>
                <div id="edit-data" class="modal-body">                      
                    <form id="form-modal-edit-data">
                        <input  id="input-modal-edit-data-idcountry" type="hidden" />
                        <div class="form-group">
                            <label for="input-modal-edit-data-countrycode" class="col-form-label">Country Code:</label>                                
                            <input  id="input-modal-edit-data-countrycode" class="form-control solonro" type="text" size="45" maxlength="30"  placeholder="Country Code."/>
                        </div>
                        <div class="form-group">
                            <label for="input-modal-edit-data-countryname" class="col-form-label">Country Name:</label>                                
                            <input  id="input-modal-edit-data-countryname" class="form-control upper" type="text" size="45" maxlength="30"  placeholder="Country Name."/>
                        </div>
                        <div id="div-modal-edit-select-enabled" class="form-group">
                            <label for="modal-edit-enabled-select" class="col-form-label">Enabled</label> <br> 
                            <select id="modal-edit-enabled-select" class="form-control input-sm">
                                <option value="0">No</option> 
                                <option value="1" selected>Yes</option>                                         
                            </select>                                
                        </div>
                        <div id="div-modal-edit-select-visible" class="form-group">
                            <label for="modal-edit-visible-select" class="col-form-label">Visible</label> <br> 
                            <select id="modal-edit-visible-select" class="form-control input-sm">
                                <option value="0">No</option> 
                                <option value="1" selected>Yes</option>                                    
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-modal-edit-close" type="button" class="btn btn-danger btn-close" data-dismiss="modal">Close</button>
                    <button id="btt-modal-edit-save" type="button" class="btn btn-success">Save changes</button>
                    <button id="btt-modal-delete" type="button" class="btn btn-warning" style="display: none;">Delete</button>                      
                </div>
            </div>
        </div>
    </div>        
    <!--------------------Fin Modal Edit------------------------------------------>
     
    <!--------------------Modal add countries------------------------------------------>        
    <div class="modal fade" id="modal-add-countries" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modall-he">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="exampleModalLabel">Countries</h2>
                </div>
                <div id="edit-data" class="modal-body">                      
                            
                            <form id="form-modal-add-countrie" name="form_add_country" method="post" action="{{ action('CountriesController@store') }}">
                                    <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <label for="input-modal-add-countrycode" class="col-form-label">Country Code:</label>                                
                                        <input  id="input-modal-add-countrycode" class="form-control solonro" name="country_code" type="text" size="45" maxlength="30"  placeholder="Country Code." required/>
                                    </div>

                                    <div class="form-group">
                                        <label for="input-modal-add-countryname" class="col-form-label">Country Name:</label>                                
                                        <input  id="input-modal-add-countryname" class="form-control upper" name="country_name" type="text" size="45" maxlength="30"  placeholder="Country Name." required/>
                                    </div>

                                    <div id="div-modal-add-select-enabled" class="form-group">
                                        <label for="modal-add-enabled-select" class="col-form-label">Enabled</label> <br> 
                                        <select id="modal-add-enabled-select" name="country_enabled" class="form-control input-sm">
                                            <option value="0">No</option> 
                                            <option value="1" selected>Yes</option>                                         
                                        </select>                                
                                    </div>

                                    <div id="div-modal-add-select-visible" class="form-group">
                                        <label for="modal-add-visible-select" class="col-form-label">Visible</label> <br> 
                                        <select id="modal-add-visible-select" name="country_visible" class="form-control input-sm">
                                            <option value="0">No</option> 
                                            <option value="1" selected>Yes</option>                                    
                                        </select>
                                    </div>
                                    
                            </form>
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-modal-close-add-countries" type="button" class="btn btn-danger btn-close" data-dismiss="modal">Close</button>
                    <button id="btt-modal-add-countries" type="button" class="btn btn-success">Add</button>                                            
                </div>
            </div>
        </div>
    </div>        
    <!--------------------Fin Modal add countries------------------------------------------>


    <!--------------------Msg Modal------------------------------------------>        
    <div class="modal fade" id="modal-msg" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modall-he">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="exampleModalLabel">Inventory KW</h2>
                </div>
                <div id="error-msg" class="modal-body">
                    {{ $error_msg ?? ''}}
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-id-err-msg" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                      
                </div>
            </div>
        </div>
    </div>           
    <!--------------------Fin Msg Modal------------------------------------------>       


    <!--------------------Modal confirm------------------------------------------>        
    <div class="modal fade" id="modal-confirm-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modall-he" >
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="exampleModalLabel">Countries</h2>
                </div>
                <div id="div-confirm-data-text" class="modal-body">                                                                  
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-modal-confirm-close" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button id="btt-modal-confirm" type="button" class="btn btn-success">Ok</button>                      
                </div>
            </div>
        </div>
    </div>        
    <!--------------------Fin Modal confirm------------------------------------------>

</div> <!--/div class="content" -->
    
 @endsection

 @section('scripts')
    <script src="{{ asset('js/jq/countries_jq.js') }}" defer></script>                         
 @show


