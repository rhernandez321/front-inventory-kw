@extends('layouts.app3')

@section('content')
<section id="back">
    <div class="container">
        <div class="row py-4">
            <div class="col-md-5">

                <div class="" style="background-color: #ffffff52;">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                            <div class="form-group has-feedback">
                                <label for="email" class="col-md-6 col-form-label"><h4>Email Adress</h4></label>

                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label for="password" class="col-md-6 col-form-label"><h4>Password</h4></label>

                                <div class="col-md-12">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <!--<div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>-->
                            <div class="mb-1 text-center">
                                <div class="form-group ">
                                    <div class="col-md-12 ">
                                        <button type="submit" class="btn btn-block btn-primary">
                                            <h4>{{ __('Login') }}</h4>
                                        </button>
                                    </div>

                                    @if (Route::has('password.request'))
                                    <div class="col-md-12">
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            <h4 class="color" >{{ __('Forgot Your Password?') }}</h4>
                                        </a>
                                    </div>
                                    @endif
                                    @guest
                                
                                    @if (Route::has('register'))
                                    <div class="col-md-12">
                                        <a class="btn btn-link" href="{{ route('register') }}">
                                            <h4 class="color" >{{ __('Register') }}</h4>
                                        </a>
                                    </div>
                                    @endif
                                
                                @endguest
                                    
                                </div>
                            </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
</section>

@endsection
