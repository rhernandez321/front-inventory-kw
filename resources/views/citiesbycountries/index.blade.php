@extends('layouts.app')

@section('content')
<div id="content">

    @include('layouts.navbar')

    <div class="container-fluid p-md-4">  

        <div id="card-ppal" class="card mb-3">		        	 
            <div class="card-header" id="cardd-hea">
                <h1>Cities by Countries </h1>
            </div> 
            
            <div name= "card-body-ppal" class="card-body">
                <div name="row1-card-body-ppal" class="row">
                    <div name="col1-row1-card-body-ppal" class="col-5">                                                                              
                        <div class="form-group">  
                            <form id="form-research-citiesbycountries" method="post" action="">
                                <input name="_method" type="hidden" value="GET">
                                <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                                <div class="form-group">                                                
                                    <select id="select-countries" name="idcountry" class="form-control input-sm">                                                    
                                        <!--option disabled selected value> Countries </option-->                                                        
                                        <option hidden value="-1" selected> Countries </option>
                                        @foreach($countries as $item)                                                                                                               
                                        <option value="{{$item->id}}"> {{$item->name}} </option>                                                                                                
                                        @endforeach
                                    </select>
                                </div>                                                           
                            </form>
                        </div>
                    </div><!--<div id="col1-row1-card-body-ppal" class="col-11">-->
                        
                    <div name="col2-row1-card-body-ppal" class="col-5">
                    </div>

                    <div name="col3-row1-card-body-ppal" class="col-1">                                                                                      
                        <button id = "btn-add-cities" type="button" class="btn btn-info" data-toggle="tooltip" title="Add City">
                            <span class="fa fa-plus" aria-hidden="true"></span>                                    
                        </button>                                     
                    </div>
                        
                </div><!--<div id="row1-card-body-ppal" class="row">-->                
            </div> <!--<div id= "card-body-ppal" class="card-body">-->
                <!------------------------------------>
        </div><!--<div id="card-ppal" class="card bg- mb-3">-->


            
            <!-----------------------------------Mostrar datos-------------------------------------->

            <div id="card-cities" class="card mb-3">  

                <div id="cardd-hea" class="card-header"> 
                    <h3>Cities</h3>
                </div>
                                            
                <div id="cities-data" class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div id="div_table_cities" class="table-responsive">                                                                              
                                <!--table id="tbl_countries" class="table table-striped table-hover table-condensed"-->   
                                <table id="tablecitiesbycountries" class="table table-striped">
                                    <thead class="thead-dark">
                                        <tr>            
                                            <th>Id</th>
                                            <th>Code</th>
                                            <th>Name</th>                                                
                                            <th>Postal Code</th> 
                                            <th>Enable</th>
                                            <th>Visible</th>
                                            <th></th>                                                
                                        </tr>
                                    </thead>
                                    <tfoot class="thead-dark">
                                        <tr>
                                            <th>Id</th>
                                            <th>Code</th>
                                            <th>Name</th>                                                
                                            <th>Postal Code</th> 
                                            <th>Enable</th>
                                            <th>Visible</th>
                                            <th></th>                                                
                                        </tr>
                                    </tfoot>
                                </table>                                
                            </div>
                        </div><!--<div class="col-12">-->               
                    </div> <!--<div class="row">-->       
                </div><!--<div id="cities-data" class="card-body">-->
            </div><!--<div id="card-cities" class="card bg- mb-3"> -->    
            <!-----------------------------------Mostrar datos-------------------------------------->

    </div><!--<div class="container-fluid">-->


    <!--------------------Modal Edit data------------------------------------------>        
    <div class="modal fade" id="modal-edit-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modall-he">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="exampleModalLabel">Cities by Countries</h2>
                </div>
                <div id="edit-data" class="modal-body">                       
                        <form id="form-modal-edit-data">
                            <input  id="input-modal-edit-data-idcity" type="hidden" />
                            <div class="form-group">
                                <label for="input-modal-edit-data-citycode" class="col-form-label">City Code:</label>                                
                                <input  id="input-modal-edit-data-citycode" class="form-control solonro" type="text" size="45" maxlength="30"  placeholder="City Code."/>
                            </div>
                            <div class="form-group">
                                <label for="input-modal-edit-data-cityname" class="col-form-label">City Name:</label>                                
                                <input  id="input-modal-edit-data-cityname" class="form-control" type="text" size="45" maxlength="30"  placeholder="City Name."/>
                            </div>
                            <div class="form-group">
                                <label for="input-modal-edit-data-postalcode" class="col-form-label">Postal Code:</label>                                
                                <input  id="input-modal-edit-data-postalcode" class="form-control" type="text" size="45" maxlength="30"  placeholder="City Postal Code."/>
                            </div>
                            <div id="div-modal-edit-select-enabled" class="form-group">
                                <label for="modal-edit-enabled-select" class="col-form-label">Enabled</label> <br> 
                                <select id="modal-edit-enabled-select" class="form-control input-sm">
                                    <option value="0">No</option> 
                                    <option value="1" selected>Yes</option>                                         
                                </select>                                
                            </div>
                            <div id="div-modal-edit-select-visible" class="form-group">
                                <label for="modal-edit-visible-select" class="col-form-label">Visible</label> <br> 
                                <select id="modal-edit-visible-select" class="form-control input-sm">
                                    <option value="0">No</option> 
                                    <option value="1" selected>Yes</option>                                    
                                </select>
                            </div>
                        </form>
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-modal-edit-close" type="button" class="btn btn-danger btn-close" data-dismiss="modal">Close</button>
                    <button id="btt-modal-edit-save" type="button" class="btn btn-success">Save changes</button>
                    <button id="btt-modal-delete" type="button" class="btn btn-warning" style="display: none;">Delete</button>                      
                </div>
            </div>
        </div>
    </div>                                
    <!--------------------Fin Modal Edit------------------------------------------>
                
                        
    <!--------------------Modal add cities by countries------------------------------------------>        
    <div class="modal fade" id="modal-add-cities" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modall-he">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h2 class="modal-title" id="exampleModalLabel">Cities</h2>
                </div>
                <div id="edit-data" class="modal-body">                      
                    <form id="form-modal-add-cities" name="form_add_country" method="post" action="">
                        <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="input-modal-add-citiescode" class="col-form-label">City Code:</label>                                
                            <input  id="input-modal-add-citiescode" class="form-control upper" name="city_code" type="text" size="45" maxlength="30"  placeholder="City Code." required/>
                        </div>
                        <div class="form-group">
                            <label for="input-modal-add-citiesname" class="col-form-label">City Name:</label>                                
                            <input  id="input-modal-add-citiesname" class="form-control upper" name="city_name" type="text" size="45" maxlength="30"  placeholder="City Name." required/>
                        </div>
                        <div class="form-group">
                            <label for="input-modal-add-citiespostalcode" class="col-form-label">Postal Code:</label>                                
                            <input  id="input-modal-add-citiespostalcode" class="form-control solonro" name="postalcode" type="text" size="45" maxlength="30"  placeholder="Postal Code." required/>
                        </div>
                        <div id="div-modal-add-select-enabled" class="form-group">
                            <label for="modal-add-enabled-select" class="col-form-label">Enabled</label> <br> 
                            <select id="modal-add-enabled-select" name="city_enabled" class="form-control input-sm">
                                <option value="0">No</option> 
                                <option value="1" selected>Yes</option>                                         
                            </select>                                
                        </div>
                        <div id="div-modal-add-select-visible" class="form-group">
                            <label for="modal-add-visible-select" class="col-form-label">Visible</label> <br> 
                            <select id="modal-add-visible-select" name="city_visible" class="form-control input-sm">
                                <option value="0">No</option> 
                                <option value="1" selected>Yes</option>                                    
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-modal-add-close" type="button" class="btn btn-danger btn-close" data-dismiss="modal">Close</button>
                    <button id="btt-modal-add-cities" type="button" class="btn btn-primary">Add</button>                                            
                </div>
            </div>
        </div>
    </div>        
    <!--------------------Fin Modal add cities by countries------------------------------------------>


    <!--------------------Msg Modal------------------------------------------>             
    <div class="modal fade" id="modal-msg" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modall-he">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h2 class="modal-title" id="exampleModalLabel">Inventory KW</h2>
                </div>
                <div id="error-msg" class="modal-body">
                    {{ $error_msg ?? ''}}
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-id-err-msg" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                      
                </div>
            </div>
        </div>
    </div>                   
    <!--------------------Fin Msg Modal------------------------------------------>       


    <!--------------------Modal confirm------------------------------------------>        
    <div class="modal fade" id="modal-confirm-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modall-he">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h2 class="modal-title" id="exampleModalLabel">Cities by Countries</h2>
                </div>
                <div id="div-confirm-data-text" class="modal-body">
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-modal-confirm-close" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button id="btt-modal-confirm" type="button" class="btn btn-success">Ok</button>                      
                </div>
            </div>
        </div>
    </div>                               
    <!--------------------Fin Modal confirm------------------------------------------>
</div><!--div class="content"-->

@endsection

@section('scripts')
    <script src="{{ asset('js/jq/citiesbycountries_jq.js') }}" defer></script>                         
@show
