@extends('layouts.app')

@section('content')
<div id="content">

    @include('layouts.navbar')
    
    <div class="container-fluid p-md-4">   

        <div id="card-ppal" class="card mb-3">		        	
            <div class="card-header" id="cardd-hea">
                <h1>Managers by Providers </h1>
            </div> 

            <div name= "card-body-ppal" class="card-body">
                <div name="row1-card-body-ppal" class="row">
                    <div name="col1-row1-card-body-ppal" class="col-5">                                                                              
                        <div class="form-group">
                            <form id="form-research" method="post" action="{{ route('providers.index') }}">
                                <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                                <select id="select-providers" name="idproviders" class="form-control input-sm">                                                    
                                    <option hidden value="-1" selected="selected">  Provider </option>
                                    @foreach($data_result as $item)                                                                                                               
                                        <option value="{{$item->id}}"> {{$item->name}} </option>                                                                                                
                                    @endforeach                                                    
                                </select>
                            </form>
                        </div>
                    </div>

                    <div name="col2-row1-card-body-ppal" class="col-5">
                    </div>
                    
                    <div  class="col-1"> 
                        <button id = "btn-add" type="button" class="btn btn-info" data-toggle="tooltip" title="Add managers by providers">
                            <span class="fa fa-plus" aria-hidden="true"></span>                                    
                        </button>  
                    </div>
                </div>
            </div> <!--card-body-->
        </div>


        
        <!-----------------------------------Mostrar datos-------------------------------------->
        <div id="card-managersbyproviders" class="card mb-3">        
            <div id="cardd-hea" class="card-header">                                    
                <h3>Managers by Providers</h3> 
            </div>
            
            <div id="data" class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div id="div_table_managersbyproviders" class="table-responsive">                          
                        @if(!is_null($data_result))
                            <table id="tablemanagersbyproviders" class="table table-striped" cellspacing="0" width="100%">
                                <thead class="thead-dark">
                                    <tr>            
                                        <th>Id</th>     
                                        <th>Code</th>                                                             
                                        <th>Name</th>                                                                                                                                               
                                        <th>LastName</th>
                                        <th>Address</th>                                                
                                        <th>Phone 1</th>                                                
                                        <th>Phone 2</th>                                                
                                        <th>Phone 3</th>                                                                                                
                                        <th>Note</th>                                                                                                
                                        <th>Enable</th>
                                        <th>Visible</th>
                                        <th></th>                                                                                                
                                    </tr>
                                </thead>
                                <tfoot class="thead-dark">
                                    <tr>
                                        <th>Id</th>     
                                        <th>Code</th>                                                             
                                        <th>Name</th>                                                                                            
                                        <th>LastName</th>
                                        <th>Address</th>                                                                                                                                                                                                   
                                        <th>Phone 1</th>                                                
                                        <th>Phone 2</th>                                                
                                        <th>Phone 3</th>                                                                                                                                         
                                        <th>Note</th>
                                        <th>Enable</th>
                                        <th>Visible</th>
                                        <th></th>                                                                                                
                                    </tr>
                                </tfoot>
                            </table>
                            @endif                          
                        </div>
                    </div>         
                </div>    
            </div>
        </div>
        <!-----------------------------------Mostrar datos-------------------------------------->
        
    </div><!--<div class="container-fluid">-->


    <!--------------------Modal Edit data------------------------------------------>        
    
    <div class="modal fade" id="modal-edit-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header  modall-he">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="exampleModalLabel">Managers by Providers</h2>
                </div>
                <div id="edit-data" class="modal-body">                      
                    <form id="form-modal-edit-data">                       
                        <input  id="input-modal-edit-data-idmanagersbyproviders" type="hidden" />
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="input-modal-edit-data-provider" class="col-form-label">Provider:</label>                                
                                    <input  id="input-modal-edit-data-provider" class="form-control" name="idprovider" type="text" size="45" maxlength="25"  disabled/>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="input-modal-edit-data-code" class="col-form-label">Code:</label>                                
                                    <input  id="input-modal-edit-data-code" class="form-control upper" type="text" size="45" maxlength="25"  placeholder="Code."/>
                                </div>
                            </div>
                        </div>
                        <div class="row">                                
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="input-modal-edit-data-name" class="col-form-label">Name:</label>                                
                                    <input  id="input-modal-edit-data-name" class="form-control upper" type="text" size="45" maxlength="30"  placeholder="Name."/>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="input-modal-edit-data-lastname" class="col-form-label">Lastname:</label>                                
                                    <input  id="input-modal-edit-data-lastname" class="form-control upper" type="text" size="45" maxlength="25"  placeholder="Lastname."/>
                                </div>
                            </div>
                        </div>                                              
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">                                        
                                    <label for="input-modal-edit-data-address" class="col-form-label">Address:</label>                                
                                    <input  id="input-modal-edit-data-address" class="form-control" type="text" size="45" maxlength="30"  placeholder="Address."/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="input-modal-edit-data-note" class="col-form-label">Note:</label>                                
                                    <input  id="input-modal-edit-data-note" class="form-control" type="text" size="45" maxlength="30"  placeholder="Note."/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="input-modal-edit-data-phone1" class="col-form-label">Phone 1:</label>                                
                                    <input  id="input-modal-edit-data-phone1" class="form-control" name="providers_phone1" type="text" size="45" maxlength="30"  placeholder="Phone 1." required/>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="input-modal-edit-data-phone2" class="col-form-label">Phone 2:</label>                                
                                    <input  id="input-modal-edit-data-phone2" class="form-control" name="providers_phone2" type="text" size="45" maxlength="30"  placeholder="Phone 2." required/>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="input-modal-edit-data-phone3" class="col-form-label">Phone 3:</label>                                
                                    <input  id="input-modal-edit-data-phone3" class="form-control" name="providers_phone3" type="text" size="45" maxlength="30"  placeholder="Phone 3." required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div id="div-modal-edit-select-enabled" class="form-group">
                                    <label for="modal-edit-enabled-select" class="col-form-label">Enabled</label> <br> 
                                    <select id="modal-edit-enabled-select" class="form-control input-sm">
                                        <option value="0">No</option> 
                                        <option value="1" selected>Yes</option>                                         
                                    </select>                                
                                </div>
                            </div>
                            <div class="col-6">
                                <div id="div-modal-edit-select-visible" class="form-group">
                                    <label for="modal-edit-visible-select" class="col-form-label">Visible</label> <br> 
                                    <select id="modal-edit-visible-select" class="form-control input-sm">
                                        <option value="0">No</option> 
                                        <option value="1" selected>Yes</option>                                    
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-modal-edit-close" type="button" class="btn btn-danger btn-close" data-dismiss="modal">Close</button>
                    <button id="btt-modal-edit-save" type="button" class="btn btn-success">Save changes</button>
                    <button id="btt-modal-delete" type="button" class="btn btn-warning" style="display: none;">Delete</button>                      
                </div>
            </div>
        </div>
    </div>        
                                
    <!--------------------Fin Modal Edit------------------------------------------>
            
                    
    <!--------------------Modal add ------------------------------------------>        
    
    <div class="modal fade" id="modal-add" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header  modall-he">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="exampleModalLabel">Managers Providers</h2>     
                </div>
                <div id="edit-data" class="modal-body">                      
                    <form id="form-modal-add-managersbyproviders" name="form_add_managersbyproviders" method="post" action="">
                        <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="input-modal-add-provider" class="col-form-label">Provider:</label>                                
                                    <input  id="input-modal-add-provider" class="form-control" name="idprovider" type="text" size="45" maxlength="25"  disabled/>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="input-modal-add-code" class="col-form-label"> Code:</label>                                
                                    <input  id="input-modal-add-code" class="form-control upper" name="managersbyproviders_code" type="text" size="45" maxlength="25"  placeholder="Code." required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">                                
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="input-modal-add-name" class="col-form-label">Name:</label>                                
                                    <input  id="input-modal-add-name" class="form-control upper" name="managersbyproviders_name" type="text" size="45" maxlength="30"  placeholder="Name." required/>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="input-modal-add-lastname" class="col-form-label"> Lastname:</label>                                
                                    <input  id="input-modal-add-lastname" class="form-control upper" name="managersbyproviders_lastname" type="text" size="45" maxlength="25"  placeholder="Lastname." required/>
                                </div>
                            </div>
                        </div>                           
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="input-modal-add-address" class="col-form-label">Address:</label>                                
                                    <input  id="input-modal-add-address" class="form-control" name="managersbyproviders_address" type="text" size="45" maxlength="30"  placeholder="Address." required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">                                        
                                    <label for="input-modal-add-note" class="col-form-label">Note:</label>                                
                                    <input  id="input-modal-add-note" class="form-control" name="managersbyproviders_note" type="text" size="45" maxlength="30"  placeholder="Note." required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="input-modal-add-phone1" class="col-form-label">Phone 1:</label>                                
                                    <input  id="input-modal-add-phone1" class="form-control" name="managersbyproviders_phone1" type="text" size="45" maxlength="30"  placeholder="Phone 1." required/>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="input-modal-add-phone2" class="col-form-label">Phone 2:</label>                                
                                    <input  id="input-modal-add-phone2" class="form-control" name="managersbyproviders_phone2" type="text" size="45" maxlength="30"  placeholder="Phone 2." required/>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="input-modal-add-phone3" class="col-form-label">Phone 3:</label>                                
                                    <input  id="input-modal-add-phone3" class="form-control" name="managersbyproviders_phone3" type="text" size="45" maxlength="30"  placeholder="Phone 3." required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div id="div-modal-add-select-enabled" class="form-group">
                                    <label for="modal-add-enabled-select" class="col-form-label">Enabled</label> <br> 
                                    <select id="modal-add-enabled-select" name="managersbyproviders_enabled" class="form-control input-sm">
                                        <option value="0">No</option> 
                                        <option value="1" selected>Yes</option>                                         
                                    </select>                                
                                </div>
                            </div>
                            <div class="col-6">
                                <div id="div-modal-add-select-visible" class="form-group">
                                    <label for="modal-add-visible-select" class="col-form-label">Visible</label> <br> 
                                    <select id="modal-add-visible-select" name="managersbyproviders_visible" class="form-control input-sm">
                                        <option value="0">No</option> 
                                        <option value="1" selected>Yes</option>                                    
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-modal-add-close" type="button" class="btn btn-danger btn-close" data-dismiss="modal">Close</button>
                    <button id="btt-modal-add" type="button" class="btn btn-success">Add</button>                                            
                </div>
            </div>
        </div>
    </div>        
    
    <!--------------------Fin Modal add cities by countries------------------------------------------>


    <!--------------------Msg Modal------------------------------------------>             
    <div class="modal fade" id="modal-msg" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header  modall-he">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="exampleModalLabel">Inventory KW</h2>
                </div>
                <div id="error-msg" class="modal-body">
                   {{$error_msg}}
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-id-err-msg" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                      
                </div>
            </div>
        </div>
    </div>                   
    <!--------------------Fin Msg Modal------------------------------------------>       


    <!--------------------Modal confirm------------------------------------------>        
    <div class="modal fade" id="modal-confirm-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header  modall-he">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="exampleModalLabel">Managers by Providers</h2>
                </div>
                <div id="div-confirm-data-text" class="modal-body">                                                                                            
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-modal-confirm-close" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button id="btt-modal-confirm" type="button" class="btn btn-success">Ok</button>                      
                </div>
            </div>
        </div>
    </div>                               
    <!--------------------Fin Modal confirm------------------------------------------>

</div> <!--div class="wrapper"--> 

@endsection

@section('scripts')
    <script src="{{ asset('js/jq/managersbyproviders_jq.js') }}" defer></script>
@show