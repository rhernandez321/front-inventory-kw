<nav id="sidebar" class="active">
	<h1><a href="{{ url('/home') }}" class="logo">KW</a></h1>
    <ul class="list-unstyled components mb-5" id="myUL">          
        <li>{{--PurchaseOrder--}}
            <a href="#purchaseorderMenu"
            class="active caret"
            >
                <spam class="fas fa-truck"></spam>
                PurchaseOrder
            </a>
            <ul class="list-unstyled nested" id="purchaseorderMenu">
                <li>
                    <a href="{{ route('purchaseorder.index') }}">
                    New Order
                    </a>
                </li>
                <li>
                    <a href="{{ route('citiesbycountries') }}">
                        History
                    </a>
                </li>
            </ul>
        </li>{{--Fin purchaseorder--}}
                    
        <li>{{--Products--}}
                        <a href="{{ route('masterproducts.index') }}" 
                        class="active"
                        >
                        <span class="fa fa-sticky-note"></span>
                            Products
                        </a>
        </li>{{--Fin Products--}}
                    
        <li>{{--Places--}}
            <a href="#placeMenu"
            class="active caret"
            >
                <spam class="fas fa-globe"></spam>
                Places
            </a>
            <ul class="list-unstyled nested" id="placeMenu">
                <li>
                    <a href="{{ route('countries.index') }}">
                    Countries
                    </a>
                </li>
                <li>
                    <a href="{{ route('citiesbycountries') }}">
                            Cities
                    </a>
                </li>     
            </ul>
        </li>{{--Fin Places--}}
                    
        <li>{{--Warehouse--}}
            <a href="#warehouseMenu"
            class="active caret"
            >
                <spam class="fas fa-building"></spam>
                Warehouse
            </a>
            <ul class="list-unstyled nested" id="warehouseMenu">
                <li>
                    <a href="{{ route('wharehouse.index') }}">
                    Warehouse
                    </a>
                </li>
                <li>        
                    <a href="{{ route('locationbywharehouses.index') }}">
                        Locations
                    </a>
                </li>
                <li>        
                    <a href="{{ route('sections.index') }}">
                        Sections
                    </a>
                </li>                
            </ul>
        </li>{{--Fin Warehouse--}}

        <li>{{--Providers--}}
            <a href="#providersMenu"
            class="active caret"
            >
                <spam class="fas fa-tag"></spam>
                Providers
            </a>
            <ul class="list-unstyled nested" id="providersMenu">
                <li>
                    <a href="{{ route('providers.index') }}">
                    Providers
                    </a>
                </li>
                <li>            
                    <a href="{{ route('managersbyproviders.index') }}">
                    Managers Providers
                    </a>
                </li>
            </ul>
        </li>{{--Fin Providers--}}
                    
        <li>{{--assemblies--}}
            <a href="#assembliesMenu"
            class="active caret"
            >
                <spam class="fas fa-tag"></spam>
                Assemblies
            </a>
            <ul class="list-unstyled nested" id="assembliesMenu">
                <li>
                    <a href="{{ route('assemblies.index') }}">
                    Assemblies
                    </a>
                </li>                                        
            </ul>
        </li>{{--Fin assemblies--}}
                    
        <li>{{--Config--}}
            <a href="#configMenu"
            class="active caret"
            ><spam class="fas fa-cog"></spam>                    
                Config
            </a>
            <ul class="list-unstyled nested" id="configMenu">
                <li>
                    <a href="{{ route('companies.index') }}">
                    Companies
                    </a>
                </li>
                <li>
                    <a href="{{ route('colors.index') }}">
                    Colors
                    </a>
                </li>
                <li>
                    <a href="{{ route('moneys.index') }}">
                    Moneys
                    </a>
                </li>
                <li>
                    <a href="{{ route('alloys.index') }}">
                    Alloys
                    </a>
                </li>
                <li>
                    <a href="{{ route('categories.index') }}">
                    Categories
                    </a>
                </li>
                <li>
                    <a href="{{ route('subcategories.index') }}">
                        Subcategories
                    </a>
                </li>
                <li>
                    <a href="{{ route('movementstypes.index') }}">
                        Movements Type
                    </a>
                </li>
                <li>
                    <a href="{{ route('reviews.index') }}">
                        Reviews
                    </a>
                </li>
                <li>
                    <a href="{{ route('status.index') }}">
                        Status
                    </a>
                </li>
                <li>
                    <a href="{{ route('unittypes.index') }}">
                        Unit Types
                    </a>
                </li>
                <li>
                    <a href="{{ route('presentations.index') }}">
                        Presentations
                    </a>
                </li>              
            </ul>
        </li>{{--Fin Config--}}
    </ul>


    



</nav>