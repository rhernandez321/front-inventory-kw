<!-- Sidebar  -->
<div>
    
        <nav id="sidebar">
                <div class="sidebar-header">                    
                       <h3>Inventory KW</h3>
                       <strong>KW</strong>                    
                </div>
            
                <ul class="list-unstyled components">
                    <li class="active">
                        <a  href="#homeMenu"  data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <i class="fas fa-home"></i>
                            Homegggg
                        </a>

                        <ul class="collapse list-unstyled" id="homeMenu">
                            <li>
                                <a href="#">Home 1</a>
                            </li>
                            <li>
                                <a href="#">Home 2</a>
                            </li>
                            <li>
                                <a href="#">Home 3</a>
                            </li>
                        </ul>
                    </li>
                
                    <li><!--Places-->
                        <a href="#placeMenu"
                        data-toggle="collapse" 
                        aria-expanded="false" 
                        class="dropdown-toggle"
                        >
                            <i class="fas fa-globe"></i>
                            Places
                        </a>

                        <ul class="collapse list-unstyled" id="placeMenu">
                            <li>
                                <a href="{{ route('countries.index') }}">
                                Countries
                                </a>
                            </li>
                            <li>
                    
                                <a href="{{ route('citiesbycountries.index') }}">
                                        Cities
                                </a>
                            </li>
                                                
                        </ul>
                    </li><!-- Fin Places-->
                    
                    <li><!--Config-->
                        <a  href="#configMenu"
                            data-toggle="collapse" 
                            aria-expanded="false" 
                            class="dropdown-toggle"
                        >
                            <i class="fas fa-globe"></i>
                            Config
                        </a>
                        
                        <ul class="collapse list-unstyled" id="configMenu">
                            <li>
                                <a href="{{route('countries.index')}}">
                                            Colors
                                </a>   
                            </li>                                                                          
                        </ul>
                    </li><!--Fin Config-->

                    <li>                
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <i class="fas fa-copy"></i>
                            Pages
                        </a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li>
                                <a href="#">Page 1</a>
                            </li>
                            <li>
                                <a href="#">Page 2</a>
                            </li>
                            <li>
                                <a href="#">Page 3</a>
                            </li>
                        </ul>
                    </li>
                </ul>

        </nav>

 </div>   
 