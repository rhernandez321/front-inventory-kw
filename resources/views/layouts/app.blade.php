<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!--<title>{{ config('app.name', 'Laravel') }}</title>-->
        <title>Inventory KW</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('js/style.js') }}" defer></script>
        <script src="{{ asset('js/funciones_js.js') }}" defer></script>
        <script src="{{ asset('js/validate_boostrap.js') }}" defer></script>

        <script src="{{ asset('js/jq/funciones_jq.js') }}" defer></script>

        <!--script src="{{ asset('js/style_jq.js') }}" defer></script-->

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">-->
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
       
        
        



        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>    
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/">
        <link href="{{ asset('/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

        <!--DatTable-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.22/datatables.min.css"/>
        <!--script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.22/datatables.min.js" defer></script-->     
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js" defer></script>     

        <!--libreras datetimepickers-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css" integrity="sha256-FAOaXTpl90/K8cXmSdsskbQN3nKYulhCpPbcFzGTWKI=" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha256-bqVeqGdJ7h/lYPq6xrPv/YGzMEb6dNxlfiTUHSgRCp8=" crossorigin="anonymous" defer></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.ru.min.js" integrity="sha256-iGDUwn2IPSzlnLlVeCe3M4ZIxQxjUoDYdEO6oBZw/Go=" crossorigin="anonymous" defer></script>
                
        <!-- Font Awesome JS -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

        <!--Botones pdf, copy, excel-->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer></script>     
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer></script>     
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js" defer></script>     
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js" defer></script>     
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" defer></script>     
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" defer></script>     
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" defer></script>     
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js" defer></script>     
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js" defer></script>

        <style type="text/css">/* Chart.js */
        @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style>
    </head>

    <body>
        <!-- jQuery CDN - Slim version (=without AJAX) -->
        <!-- Popper.JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <!-- Bootstrap JS -->
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>


        
        <div class=" wrapper d-flex align-items-stretch" >
        @guest
            
        @else
            
            @include('layouts.sidebar')

                <main style="width:100%; background: #edffef;">
                        @yield('content')               
                </main>

         @endguest
        </div>
       
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>

         <!--VARIABLES GLOBALES-->
         <script>
 
            function countriesRoute(){
            return  "{{ route('countries_research') }}";
            }

            function countriesAction(){
                return "{{ route('countries.action') }}";
            }

            function citiesByCountriesAction(){
                return "{{ route('citiesbycountries.action') }}";
            }

            function citiesByCountriesResearch(){
                return "{{ route('citiesbycountries_research') }}";
            }

            function colorsAction(){
                return "{{ route('colors.action') }}";
            }

            function colorsResearch(){
                return "{{ route('colors_research') }}";
            }

            function moneyAction(){
                return "{{ route('moneys.action') }}";
            }

            function moneyResearch(){
                return "{{ route('moneys_research') }}";
            }

            function alloysAction(){
                return "{{ route('alloys.action') }}";
            }

            function alloysResearch(){
                return "{{ route('alloys_research') }}";
            }

            function categoriesResearch(){
                return "{{ route('categories_research') }}";
            }

            function categoriesAction(){
                return "{{ route('categories.action') }}";
            }

            function subcategoriesResearch(){
                return "{{ route('subcategories_research') }}";        
            }

            function subcategoriesAction(){
                return "{{ route('subcategories.action') }}";        
            }

            function movementsTypesExist(){
                return "{{ route('movementstypes_exist') }}";        
            }

            function movementsTypesResearch(){
                return "{{ route('movementstypes_research') }}";        
            }

            function movementsTypesAction(){
                return "{{ route('movementstypes.action') }}";        
            }

            function reviewsAction(){
                return "{{ route('reviews.action') }}";        
            }

            function reviewsResearch(){
                return "{{ route('reviews_research') }}";        
            }

            function statusAction(){
                return "{{ route('status.action') }}";        
            }

            function statusResearch(){
                return "{{ route('status_research') }}";        
            }

            function unittypesAction(){
                return "{{ route('unittypes.action') }}";        
            }

            function unittypesResearch(){
                return "{{ route('unittypes_research') }}";        
            }

            function presentationsAction(){
                return "{{ route('presentations.action') }}";        
            }

            function presentationsResearch(){
                return "{{ route('presentations_research') }}";        
            }

            function wharehouseAction(){
                return "{{ route('wharehouse.action') }}";        
            }
                
            function wharehouseResearch(){
                return "{{ route('wharehouse_research') }}";        
            }

            function wharehouseAction(){
                return "{{ route('wharehouse.action') }}";        
            }
                
            function locationbywharehousesResearch(){
                return "{{ route('locationbywharehouses_research') }}";        
            }

            function locationbywharehousesAction(){
                return "{{ route('locationbywharehouses.action') }}";        
            }

            function sectionsResearch(){
                return "{{ route('sections_research') }}";        
            }

            function sectionsAction(){
                return "{{ route('sections.action') }}";        
            }

            function sectionsbyLocationsbyWharehousesandCompany(){
                return "{{ route('sections_locationsbywharehousesandcompany') }}";        
            }

            function providersResearch(){
                return "{{ route('providers_research') }}";        
            }

            function providersAction(){
                return "{{ route('providers.action') }}";        
            }

            function providersCitiesbyCountries(){
                return "{{ route('providers_citiesbycountries') }}";        
            }

            function managersbyprovidersResearch(){
                return "{{ route('managersbyproviders_research') }}";        
            }

            function managersbyprovidersAction(){
                return "{{ route('managersbyproviders.action') }}";        
            }

            function companiesResearch(){
                return "{{ route('companies_research') }}";        
            }

            function companiesAction(){
                return "{{ route('companies.action') }}";        
            }

            function companiesCitiesbyCountries(){
                return "{{ route('companies_citiesbycountries') }}";        
            }

            function masterproductsAction(){
                return "{{ route('masterproducts.action') }}";        
            }

            function masterproducts_locations(){
                return "{{ route('masterproducts_locations') }}";        
            }

            function masterproductsSubcategories(){        
                return "{{ route('masterproducts_subcategories') }}";        
            }

            function masterproductsSearch(){
                return "{{ route('masterproducts_search') }}";        
            }

            function masterproductsSections(){
                return "{{ route('masterproducts_sections') }}";        
            }

            function assembliesResearch(){
                return "{{ route('assemblies_research') }}";        
            }

            function purcharseorderProducts(){
                return "{{ route('purcharseorderproducts') }}";        
            }

            function purcharseorderProviders(){
                return "{{ route('purcharseorderproviders') }}";        
            }

            function purcharseorderAction(){
                return "{{ route('purcharseorder.action') }}";        
            }

            function purcharseorderActionDetails(){
                return "{{ route('purcharseorder.actiondetails') }}";        
            }

        </script>

    </body>
</html>