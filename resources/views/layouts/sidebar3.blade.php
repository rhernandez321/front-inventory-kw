        <!-- Sidebar  -->    
        <nav id="sidebar" class="col-12 d-none d-md-block bg-light sidebar">
        
            <div id="sidebarheader" class="sidebar-header">
                <h3>Inventory KW</h3>
                <strong>KW</strong>
            </div>

            <ul class="list-unstyled components">

                <li class="active">
                    <a  href="#homeMenu"  data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="fas fa-home"></i>
                        Home
                    </a>

                    <ul class="collapse list-unstyled" id="homeMenu">
                        <li>
                            <a href="#">Home 1</a>
                        </li>
                        <li>
                            <a href="#">Home 2</a>
                        </li>
                        <li>
                            <a href="#">Home 3</a>
                        </li>
                    </ul>
                </li>
                
                <li>{{--PurchaseOrder--}}
                    <a href="#purchaseorderMenu"
                    data-toggle="collapse" 
                    aria-expanded="false" 
                    class="dropdown-toggle"
                    >
                        <i class="fas fa-truck"></i>
                        PurchaseOrder
                    </a>

                    <ul class="collapse list-unstyled" id="purchaseorderMenu">
                        <li>
                            <a href="{{ route('purchaseorder.index') }}">
                            New Order
                            </a>
                        </li>
                        <li>
                
                            <a href="{{ route('citiesbycountries') }}">
                                History
                            </a>
                        </li>
                                            
                    </ul>
                </li>{{--Fin purchaseorder--}}
                
                <li>{{--Products--}}
                    <a href="{{ route('masterproducts.index') }}">
                        Products
                    </a>
                    
                </li>{{--Fin Products--}}
                
                <li>{{--Places--}}
                    <a href="#placeMenu"
                    data-toggle="collapse" 
                    aria-expanded="false" 
                    class="dropdown-toggle"
                    >
                        <i class="fas fa-globe"></i>
                        Places
                    </a>

                    <ul class="collapse list-unstyled" id="placeMenu">
                        <li>
                            <a href="{{ route('countries.index') }}">
                            Countries
                            </a>
                        </li>
                        <li>
                
                            <a href="{{ route('citiesbycountries') }}">
                                    Cities
                            </a>
                        </li>
                                            
                    </ul>
                </li>{{--Fin Places--}}
                
                <li>{{--Warehouse--}}
                    <a href="#warehouseMenu"
                    data-toggle="collapse" 
                    aria-expanded="false" 
                    class="dropdown-toggle"
                    >
                        <i class="fas fa-building"></i>
                        Warehouse
                    </a>

                    <ul class="collapse list-unstyled" id="warehouseMenu">
                        <li>
                            <a href="{{ route('wharehouse.index') }}">
                            Warehouse
                            </a>
                        </li>

                        <li>        
                            <a href="{{ route('locationbywharehouses.index') }}">
                                Locations
                            </a>
                        </li>

                        <li>        
                            <a href="{{ route('sections.index') }}">
                                Sections
                            </a>
                        </li>
                                            
                    </ul>
                </li>{{--Fin Warehouse--}}

                <li>{{--Providers--}}
                    <a href="#providersMenu"
                    data-toggle="collapse" 
                    aria-expanded="false" 
                    class="dropdown-toggle"
                    >
                        <i class="fas fa-tag"></i>
                        Providers
                    </a>

                    <ul class="collapse list-unstyled" id="providersMenu">
                        <li>
                            <a href="{{ route('providers.index') }}">
                            Providers
                            </a>
                        </li>

                        <li>            
                            <a href="{{ route('managersbyproviders.index') }}">
                            Managers Providers
                            </a>
                        </li>
                                            
                    </ul>
                </li>{{--Fin Providers--}}
                
                <li>{{--assemblies--}}
                    <a href="#assembliesMenu"
                    data-toggle="collapse" 
                    aria-expanded="false" 
                    class="dropdown-toggle"
                    >
                        <i class="fas fa-tag"></i>
                        Assemblies
                    </a>

                    <ul class="collapse list-unstyled" id="assembliesMenu">
                        <li>
                            <a href="{{ route('assemblies.index') }}">
                            Assemblies
                            </a>
                        </li>                                        
                    </ul>
                </li>{{--Fin assemblies--}}
                
                <li>{{--Config--}}
                    <a href="#configMenu"
                    data-toggle="collapse" 
                    aria-expanded="false" 
                    class="dropdown-toggle"
                    >
                        <i class="fas fa-cog"></i>                    
                        Config
                    </a>

                    <ul class="collapse list-unstyled" id="configMenu">
                        <li>
                            <a href="{{ route('companies.index') }}">
                            Companies
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('colors.index') }}">
                            Colors
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('moneys.index') }}">
                            Moneys
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('alloys.index') }}">
                            Alloys
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('categories.index') }}">
                            Categories
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('subcategories.index') }}">
                                Subcategories
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('movementstypes.index') }}">
                                Movements Type
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('reviews.index') }}">
                                Reviews
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('status.index') }}">
                                Status
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('unittypes.index') }}">
                                Unit Types
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('presentations.index') }}">
                                Presentations
                            </a>
                        </li>
                                            
                    </ul>

                </li>{{--Fin Config--}}

            </ul>

        </nav>
    