@extends('layouts.app')

@section('content')
<div id="content">

    @include('layouts.navbar')

    <div class="container-fluid p-md-4">  

        <div id="card-ppal" class="card mb-3">		        	
            <div class="card-header" id="cardd-hea">
                  <h1>Assemblies </h1>
            </div> 

            <div name= "card-body-ppal" class="card-body">
                <div name="row0-card-body-ppal" class="row">                                    
                    <div name="col1-row0-card-body-ppal" class="col-5">                      
                        <div class="form-group">
                            <form id="form-master-search" method="post" action="">
                                <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                                <div class="input-group mb-3">
                                    <input  id="input_search_assemblies" class="form-control" type="search" size="30" name="find_products"  placeholder="Name / Code." />
                                    <button id="btn-search" class="btn btn-default" type="button">
                                        <span class="fa fa-search  icon"></span>
                                    </button>
                                </div>
                            </form>
                        </div>                                                
                    </div>   

                    <div name="col2-row0-card-body-ppal" class="col-5">              
                    </div>         

                    <div name="col3-row0-card-body-ppal" class="col-1">                                                                      
                        <form  method="GET" action="{{ route('assemblies.create') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">       
                            <button id = "btn-add" type="submit" class="btn btn-info" data-toggle="tooltip" title="Add products">
                                    <span class="fa fa-plus" aria-hidden="true"></span>                                    
                            </button>        
                        </form>
                    </div>                                                             
                </div>
            </div> <!--card-body-->
        </div>


        
        <!-----------------------------------Mostrar datos-------------------------------------->
        <div id="card-master-products" class="card mb-3">        
            <div id="cardd-hea" class="card-header">                                    
                <h3>Assemblies</h3> 
            </div>
             
            <div id="data" class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div id="div_table_assemblies" class="table-responsive">                          
                        @if(!is_null($data_result))                             
                            <table id="tableassemblies" class="table table-striped" cellspacing="0" width="100%">
                                <thead class="thead-dark">
                                    <tr>            
                                        <th>Id</th>     
                                        <th>Code</th>         
                                        <th>Name</th>
                                        <th>Description</th> 
                                        <th>Cost</th>     
                                        <th>Price</th>                                                        
                                        <th>Color</th> 
                                        <th>Review</th>                                                                                                
                                        <th>Id_Color</th>
                                        <th>Id_Review</th>                                             
                                        <th>Enabled</th>     
                                        <th>Visible</th>   
                                        <th></th>                                                                                                
                                    </tr>
                                </thead>                         
                            </table>       
                            @endif                          
                        </div>
                    </div>      
                </div> 
            </div>
        </div> 
        <!-----------------------------------Mostrar datos-------------------------------------->        
                
    </div><!--<div class="container-fluid">-->


    <!--------------------Msg Modal------------------------------------------>             
    <div class="modal fade" id="modal-msg" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modall-he">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title" id="exampleModalLabel">Inventory KW</h2>
                </div>
                <div id="error-msg" class="modal-body">
                   {{$error_msg}}
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-id-err-msg" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                      
                </div>
            </div>
          </div>
     </div>                   
    <!--------------------Fin Msg Modal------------------------------------------>       

</div> <!--div class="content"--> 

@endsection

@section('scripts')
    <script src="{{ asset('js/jq/assemblies_index_jq.js') }}" defer></script>
@show