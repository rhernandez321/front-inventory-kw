@extends('layouts.app')

@section('content')
<div id="content">

    @include('layouts.navbar')

    <div class="container-fluid p-md-4">  

        <div class="content-wrapper">

            <section class="content-header card-header">
                <h1><i class="fa fa-edit"></i>Create Assemblies</h1>
            </section>

            <section class="content">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-12">
                            
                                <form id="form-assemblies" method="POST" action="" class="needs-validation" novalidate>
                                    
                                    <input name="_token" type="hidden"  value="{{ csrf_token() }}">

                                    <div class="col-md-12">
                                        <div class="row ">                                           

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                <label for="inputAssembliesCode" class="col-2 col-form-label">Code</label>
                                                    <div class="">
                                                        <input type="text" class="form-control uppder" id="inputAssembliesCode" placeholder="Code" maxlength="30" required>
                                                        <div class="invalid-feedback">Please fill out this field.</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                <label for="inputAssembliesName" class="col-2 col-form-label">Name</label>
                                                    <div class="">
                                                        <input type="text" class="form-control uppder" id="inputAssembliesName" placeholder="Name" maxlength="50" required>
                                                        <div class="invalid-feedback">Please fill out this field.</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                <label for="selectAssembliesReview" class="col-2 col-form-label">Review</label>
                                                    <div class="col-10">
                                                        <select id="selectAssembliesReview" class="form-control input-sm" required>
                                                            <option hidden value="-1">Reviews</option> 
                                                            
                                                        </select>
                                                        <div class="invalid-feedback">Please fill out this field.</div>
                                                    </div>
                                                </div>
                                            </div> 

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                <label for="selectAssembliesColor" class="col-2 col-form-label">Color</label>
                                                    <div class="col-10">
                                                        <select id="selectAssembliesColor" class="form-control input-sm" required>
                                                            <option hidden value="-1">Colors</option> 
                                                            
                                                        </select>
                                                        <div class="invalid-feedback">Please fill out this field.</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <img src="" class="ml-3" id="previewImg" width="100%" height="100%">  
                                                </div>
                                            </div>                                   

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row ">
                                        

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                <label for="inputAssembliesCost" class="col-2 col-form-label">Cost</label>
                                                    <div class="col-10">
                                                        <input type="number" class="form-control" id="inputAssembliesCost" placeholder="Cost" required>
                                                    </div>
                                                    <div class="invalid-feedback">Please fill out this field.</div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                <label for="inputAssembliesPrice" class="col-2 col-form-label">Price</label>
                                                    <div class="col-10">
                                                        <input type="number" class="form-control" id="inputAssembliesPrice" placeholder="Price" required>
                                                    </div>
                                                    <div class="invalid-feedback">Please fill out this field.</div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="selectAssembliesPartType" class="col-2 col-form-label">Part</label>
                                                    <div class="col-10">
                                                        <select id="selectAssembliesPartType" class="form-control input-sm" required>
                                                            <option hidden value="-1">Part Type</option> 
                                                        </select>
                                                        <div class="invalid-feedback">Please fill out this field.</div>
                                                    </div>
                                                </div>  
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="customFileLang" class="col-2 col-form-label">Image:</label>                                                                                                           
                                                    <div class="custom-file">                                                        
                                                        <input id="customFileLang" type="file" class="custom-file-input" lang="es">
                                                        <label class="custom-file-label" for="customFileLang">Select file</label>
                                                    </div>
                                                </div>
                                            </div>   
                                                                           
                                        </div>
                                    </div>

                                     <div class="col-md-12">
                                        <button id = "btn-add" type="button" class="btn btn-info btn-lg btn-block btn-send" data-toggle="tooltip" title="Add products">                                            
                                            Add
                                        </button>    
                                    </div>  
                                </form> <!--form master-products-->  
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div><!--<div class="container-fluid">-->

        

     <!--------------------Msg Modal------------------------------------------>             
     <div class="modal fade" id="modal-msg" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
              <div class="modal-header modall-he">
                      <h2 class="modal-title" id="exampleModalLabel">Inventory KW</h2>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div id="error-msg" class="modal-body">
                     {{$error_msg}}
                  </div>
                  <div class="modal-footer modall-fo">
                      <button id="btt-id-err-msg" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>                      
                  </div>
              </div>
          </div>
     </div>                   
    <!--------------------Fin Msg Modal------------------------------------------>       

</div> <!--div class="content"--> 

@endsection

@section('scripts')
    <script src="{{ asset('js/jq/assemblies_create_jq.js') }}" defer></script>
@show