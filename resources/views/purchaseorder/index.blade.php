@extends('layouts.app')

@section('content')
<div id="content" class="">

        @include('layouts.navbar')


    <div class="container-fluid">    
        <div id="card-ppal" class="card mb-3">		        	
			
            <div class="card-header">
                  <h1>Assemblies </h1>
            </div> 
            <!------------------------------------>
            <div name= "card-body-ppal" class="card-body">
                
                <div name="row0-card-body-ppal" class="row">                                    
                    <!--BLOQUE DE BUSQUEDA-->
                    <div name="col1-row0-card-body-ppal" class="col-4">                      
                        <div class="form-group">
                            <form id="form-master-search" method="post" action="">
                                    <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                      
                                    <div class="input-group mb-3">
                                        <input  id="input_search_assemblies" class="form-control" type="search" size="30" name="find_products"  placeholder="Name / Code." />                        
                                        
                                        <button id="btn-search" class="btn btn-default" type="button">
                                            <span class="fa fa-search  icon"></span>
                                        </button>
                                    </div>
                            </form>
                        </div>                                                
                    </div>   
                    <!--FIN BLOQUE DE BUSQUEDA-->

                    <div name="col2-row0-card-body-ppal" class="col-6">
                                          
                    </div>         

                    <!--BUTTON ADD-->
                    <div name="col3-row0-card-body-ppal" class="col-1">                                                                      
                        <form  method="GET" action="{{ route('assemblies.create') }}">
                            
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                
                            <button id = "btn-add" type="submit" class="btn btn-info" data-toggle="tooltip" title="Add products">
                                    <span class="fa fa-plus" aria-hidden="true"></span>                                    
                            </button>        
                            
                        </form>
                    </div> 
                    
                    <!--FIN BUTTON ADD-->
                                                            
                </div> <!--row0-->
                                
            </div> <!--<div id= "card-body-ppal" class="card-body">-->
            <!------------------------------------>
        </div><!--<div id="card-ppal" class="card bg-info mb-3">-->


        
        <!-----------------------------------Mostrar datos------------------------------------->
        <div id="card-master-products" class="card bg-info mb-3">        
            <div class="card-header">                                    
                  <div class="row">
                        <div  class="col-11"> 
                        <h5>Assemblies</h5> 
                        </div>                                
                  </div>
                  
            </div>
             
            <div id="data" class="card-body">
                <div class="row">
                        <div class="col-12">
                            <div id="div_table_purchaseorder" class="table-responsive">                          
                            @if(!is_null($data_result))
                                                                                       
                                    <table id="tablepurchaseorder" class="display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                       
                                                <th>Id</th>     
                                                <th>IdProvider</th>
                                                <th>IdUser</th>
                                                <th>IdUserCanceled</th>
                                                <th>IdMoneyType</th>
                                                <th>Code</th>         
                                                <th>Provider</th>
                                                <th>Cost</th>     
                                                <th>Cost of Shipping</th>     
                                                <th>tax</th>
                                                <th>Canceled</th>     
                                                <th>Note</th> 
                                                <th>Canceled</th>                                                     
                                                <th>Date Canceled</th>                                                                                                     
                                                <th></th>                                                                                                
                                            </tr>
                                        </thead>
                                                                        
                                    </table>
                                                
                                @endif                          
                            </div>
                        </div><!--<div class="col-12">-->               
                </div> <!--<div class="row">-->       
            </div><!--<div id="data" class="card-body">-->
        </div><!--<div id="card-wharehouse" class="card bg-info mb-3"> -->    
        <!-----------------------------------Mostrar datos------------------------------------->        
                
    </div><!--<div class="container-fluid">-->

    /////////////////////////////////////////////
    <div class="content-wrapper" style="min-height: 765px;">
        <!-- Content Header (Page header) -->
		  <section class="content-header">
		        <h1><i class="fa fa-edit"></i> Agregar nueva compra</h1>
		
		  </section>
		<!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Nueva Compra</h3>
                </div>
                <div class="box-body">
                      <div class="row">
                            <!-- *********************** Purchase ************************** -->
                            <div class="col-md-12 col-sm-12">
                                <form method="post" name="new_purchase" id="new_purchase">
                                    <div class="box box-info">
                                        <div class="box-header box-header-background-light with-border">
                                            <h3 class="box-title  ">Detalles de la compra</h3>
                                        </div>

                                        <div class="box-background">
                                            <div class="box-body">
                                                <div class="row">

                                                    <div class="col-md-5">

                                                        <label>Proveedor</label>
										                <div class="input-group">
                                                            <select class="form-control select2 select2-hidden-accessible" name="supplier_id" id="supplier_id" required="" tabindex="-1" aria-hidden="true">
											                        <option value="">Selecciona Proveedor</option>
											
                                                            </select>
                                                            <span class="select2 select2-container select2-container--default" dir="ltr" style="width: 479px;">
                                                                <span class="selection">
                                                                    <span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-supplier_id-container">
                                                                        <span class="select2-selection__rendered" id="select2-supplier_id-container" title="Selecciona Proveedor">Selecciona Proveedor</span>
                                                                        <span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>
                                                                    </span>
                                                                </span>
                                                                        <span class="dropdown-wrapper" aria-hidden="true"></span>
                                                            </span>
							                				<span class="input-group-btn">
											                	<button class="btn btn-default" type="button" data-toggle="modal" data-target="#proveedor_modal"><i class="fa fa-plus"></i> Nuevo</button>
											                </span>
										                </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Fecha</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control datepicker" name="purchase_date" value="31/01/2021" readonly="">
											                <span class="input-group-btn ">
												                <button class="btn btn-default " type="button"><i class="fa fa-calendar "></i></button>
											                </span>
                                           
                                                        </div>
                                                    </div>
									                <div class="col-md-2">

                                                        <label>Compra Nº</label>
                                                        <input type="text" class="form-control" name="order_number" id="order_number" required="" value="1223461">
                                                    </div>
									
									                <div class="col-md-2">

                                                        <label>Agregar productos</label>
                                                        <button type="button" class="btn btn-block btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i> Buscar productos</button>
                                                    </div>
                                                </div>

                                            </div><!-- /.box-body -->
                                        </div>
                                    </div>
                                    <!-- /.box -->
                            
                                </form>
                            </div>
                            <!--/.col end -->
					  </div>
					  <div id="resultados_ajax" class="col-md-12" style="margin-top:4px"></div><!-- Carga los datos ajax -->
					  <div id="resultados" class="col-md-12" style="margin-top:4px"> 
                      	<div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>CODIGO</th>
                                        <th class="text-center">CANT.</th>
                                        <th>DESCRIPCION</th>
                                        <th><span class="pull-right">PRECIO UNIT.</span></th>
                                        <th><span class="pull-right">PRECIO TOTAL</span></th>
                                        <th></th>
                                    </tr>

                                    <tr>
                                        <td colspan="4"><span class="pull-right">NETO $</span></td>
                                        <td><span class="pull-right">0.00</span></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="text-right">
                                            <select name="taxes" id="taxes" onchange="tax_value(this.value)">
                                            <option value="10.00">IVA 10.00 %</option><option value="0.00" selected="">IVA 0.00 %</option>			
                                            </select>
                                        </td>
                                        <td><span class="pull-right">0.00</span></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><span class="pull-right">TOTAL $</span></td>
                                        <td><span class="pull-right">0.00</span></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                      </div><!-- Carga los datos ajax -->
					
					  <div class="box-footer">
						<button type="submit" class="btn btn-success pull-right "><i class="fa fa-floppy-o"></i> Guardar datos</button>
                      </div>
					
                </div><!-- /.box-body -->
            
            </div><!-- /.box -->	
     
        </section><!-- /.content -->
	</div>
    /////////////////////////////////////////////////

        

     <!--------------------Msg Modal------------------------------------------>             
     <div class="modal fade" id="modal-msg" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Inventory KW</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div id="error-msg" class="modal-body">
                     {{$error_msg}}
                  </div>
                  <div class="modal-footer">
                      <button id="btt-id-err-msg" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>                      
                  </div>
              </div>
          </div>
     </div>                   
    <!--------------------Fin Msg Modal------------------------------------------>       


</div> <!--div class="wrapper"--> 
@endsection

@section('scripts')
    <script src="{{ asset('js/jq/assemblies_index_jq.js') }}" defer></script>
@show