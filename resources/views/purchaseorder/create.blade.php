@extends('layouts.app')

@section('content')
<div id="content">

    @include('layouts.navbar')
   
    <div class="container-fluid p-md-4">

        <!----------------------------index order---------------------------------------------------->
        <div class="content-wrapper">
        
            <section class="content-header card-header">
                <h1><i class="fa fa-edit"></i>New Purchase Order</h1>
            </section>

            <section class="content">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <form method="post" name="new_purchase" id="new_purchase">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input  id="input-factormoney" type="hidden" value= "{{ $money->factor }}"/>
                                        <input  id="input-idmoney" type="hidden" value= "{{ $money->id }}"/>
                                        <div class="box box-info">
                                            <div class="box-background">
                                                <div class="box-body">                                                
                                                    <div id="div-msg-canceled" class="alert alert-warning" style="display:none" >
                                                        Purchare Order Canceled
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="select-option-providers" class="col-form-label">Provider</label>                                
                                                                <select id="select-option-providers" class="form-control input-sm">                                                    
                                                                    <option hidden value="-1" selected="selected">  Providers </option>                                                                                                                
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <div class="form-group">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label for="input-purchaseorder-date" class="col-form-label">Date</label>                                
                                                                <input  id="input-purchaseorder-date" class="form-control datepicker" type="text" size="45" maxlength="30" readonly/>
                                                            </div>                                                        
                                                        </div>
                                                        <div class="col-md-2">                                                        
                                                            <div class="form-group">
                                                                <label for="input-nro-purchaseorder" class="col-form-label">Number</label>                                
                                                                <input  id="input-nro-purchaseorder" class="form-control solonro" type="text" size="45" maxlength="30" readonly/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">                                                        
                                                            <div class="form-group">
                                                                <label for="input-cost-of-shipping" class="col-form-label">Cost Of shipping</label>                                
                                                                <input  id="input-cost-of-shipping" class="form-control" type="number" step="0.01" size="45" maxlength="30"  placeholder="0.00"/>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">                                                        
                                                            <div id="div-new-po" class="form-group">
                                                                <label for="btn-new-po" class="col-form-label">&nbsp;</label>                                
                                                                <button id="btn-new-po" type="button" class="btn btn-block btn-info" data-toggle="modal" data-target="#"> New Order </button>
                                                            </div>
                                                            <div id="div-addproducts" class="form-group" style="display:none">
                                                                <label for="searchproducts_all" class="col-form-label">&nbsp;</label>                                
                                                                <button id="searchproducts_all" type="button" class="btn btn-block btn-info" data-toggle="modal" data-target="#"><i class="fa fa-search"></i> Add products</button>
                                                            </div>
                                                        </div>                                                    
                                                    </div>
                                                </div><!-- /.box-body -->
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                        <!--div id="resultados_ajax" class="col-md-12" style="margin-top:4px"></div--><!-- Carga los datos ajax -->
                        <div id="resultados" class="col-md-12" style="margin-top:4px"> 
                            <div class="table-responsive">
                                <table id="tablepurcharseorderlist" class="table">
                                        <tbody>
                                            <tr style="background-color:#73ace8;">
                                                <th>CODIGO</th>
                                                <th class="text-center">CANT.</th>
                                                <th>DESCRIPCION</th>
                                                <th><span class="float-right">PRECIO UNIT.</span></th>
                                                <th><span class="float-right">PRECIO TOTAL</span></th>
                                                <th></th>
                                            </tr>
                                            <tr id="rowneto">
                                                <td colspan="4">
                                                    <span class="float-right">NETO {{ $money->simbolo }}</span>                                           
                                                </td>
                                                <td><span id="spanneto" class="pull-right">0.00</span></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="text-right">
                                                    <select name="taxes" id="select-option-taxes">
                                                    <option value="10.00">IVA 10.00 %</option><option value="0.00" selected="">IVA 0.00 %</option>			
                                                    </select>
                                                </td>
                                                <td><span id="spantax" class="float-right">0.00</span></td>
                                                <td></td>
                                            </tr>
                                            <tr style="background-color: #a7d1ea">
                                                <td colspan="4"><span class="float-right">TOTAL {{ $money->simbolo }}</span></td>
                                                <td><span id="spantotal" class="float-right">0.00</span></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                </table>
                            </div>
                        </div><!-- Carga los datos ajax -->                       
                        <div class="box-footer modal-footer float-right">
                                <!--button type="submit" class="btn btn-success float-right "><i class="fa fa-floppy"></i> Guardar datos</button-->
                                <div id="div-btn-update-po" style="display:none">
                                    <button id="btn-cancel-po" type="button" class="btn btn-danger btn-close">
                                        <span class="fa fa-close"></span>
                                        Cancel
                                    </button>
                                    <button id="btn-update-po" type="button" class="btn btn-success btn-close">
                                        <span class="fa fa-save"></span>
                                        Update
                                    </button>  
                                </div>
                                <div id="div-btn-confirm-po">
                                    <button id="btn-confirm-po" type="button" class="btn btn-success">
                                        <span class="fa fa-save"></span>
                                        Confirm
                                    </button>  
                                </div>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->	
            </section><!-- /.content -->
        </div>
        <!----------------------------fin index order---------------------------------------------------->

        <!--------------------Modal Search Products ------------------------------------------>        
        <div class="modal fade bs-example-modal-lg" id="modal-search-products" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">

                    <div class="modal-header modall-he">                                            
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>                      
                        </button>
                        <h1 class="modal-title" id="exampleModalLabel">Products</h1>
                    </div>

                    <div id="edit-data" class="modal-body"> 
                        <!--<div class="form-group">
                            <form id="form-research" method="post" class="form-inline" action="">
                                <!--input name="_method" type="hidden" value="GET"-->
                                <!--<input name="_token" type="hidden"  value="{{ csrf_token() }}">
                                <input  id="modal-input-find-products" class="form-control upper" type="search" size="30" placeholder="Name / Code." value="{{ $_data ?? ''}}" /> 
                                &nbsp;&nbsp;
                                <button id="modal-btn-search-products" class="btn btn-default my-2 my-sm-0" type="button">
                                    <span class="fa fa-search  icon"></span>
                                    Search
                                </button>
                            </form>
                        </div>-->
                        <div id="divproducts" class="table-responsive" >
                                <table id="tableproducts" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Id</th>                             
                                            <th>Code</th>
                                            <th>Product</th>
                                            <th>Provider</th>                                                                
                                            <th>Qtv</th>
                                            <th>Cost</th>                                                                
                                            <th></th>                                         
                                        </tr>
                                    </thead>  
                                </table>
                        </div>
                    </div>

                    <div class="modal-footer modall-fo">
                        <button id="btt-modal-add-close" type="button" class="btn btn-danger btn-close" data-dismiss="modal">Close</button>                                     
                    </div>
                    
                </div>
            </div>
        </div>        
        <!--------------------Fin Modal Search Products------------------------------------------>

        <!--------------------Modal confirm------------------------------------------>        
        <div class="modal fade" id="modal-confirm-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header modall-he">                    
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h1 class="modal-title" id="exampleModalLabel">Purcharse Order</h1>    
                    </div>
                    <div id="div-confirm-data-text" class="modal-body">
                    </div>
                    <div class="modal-footer modall-fo">
                        <button id="btt-modal-confirm-close" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button id="btt-modal-confirm" type="button" class="btn btn-success">Ok</button>                      
                    </div>
                </div>
            </div>
        </div>                               
        <!--------------------Fin Modal confirm------------------------------------------>
                        
    </div><!--<div class="container-fluid">-->

     <!--------------------Msg Modal------------------------------------------>             
    <div class="modal fade" id="modal-msg" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header modall-he">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <h1 class="modal-title" id="exampleModalLabel">Inventory KW</h1>
                </div>
                <div id="error-msg" class="modal-body">
                   {{$error_msg}}
                </div>
                <div class="modal-footer modall-fo">
                    <button id="btt-id-err-msg" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                      
                </div>
            </div>
        </div>
    </div>                   
    <!--------------------Fin Msg Modal------------------------------------------>       

</div> <!--div class="content"--> 

@endsection

@section('scripts')
    <script src="{{ asset('js/jq/purchaseorder_create_jq.js') }}" defer></script>
@show