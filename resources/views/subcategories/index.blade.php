@extends('layouts.app')

@section('content')
<div id="content" class="">

        @include('layouts.navbar')
    
    <div class="container-fluid p-md-4">    
        <div id="card-ppal" class="card mb-3">		        	
			
            <div class="card-header" id="cardd-hea">
                  <h1>Subcategories </h1>
            </div> 
            <!------------------------------------>
            <div name= "card-body-ppal" class="card-body">
                <div name="row1-card-body-ppal" class="row">
                    <div name="col1-row1-card-body-ppal" class="col-5">                                                                              
                        <div class="form-group">
                            <form id="form-research-categories" method="post" action="">
                                
                                <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                                                                                                    
                                    <select id="select-categories" name="idcategories" class="form-control input-sm">                                                    
                                        <option hidden value="-1" selected> Categories </option>
                                        
                                        @foreach($categories as $item)                                                                                                               
                                             <option value="{{$item->id}}"> {{$item->name}} </option>                                                                                                
                                        @endforeach                                                    
                                    </select>
                                                                                             
                            </form>
                        </div>                        
                    </div><!--<div id="col1-row1-card-body-ppal" class="col-11">-->

                    <div  class="col-6"> 
                    </div>

                    <div  class="col-1"> 
                            <button id = "btn-add-subcategories" type="button" class="btn btn-info" data-toggle="tooltip" title="Add subcategories">
                                <span class="fa fa-plus" aria-hidden="true"></span>                                    
                            </button>                                                
                    </div>
                                        
                </div>     
            </div> <!--card-body-->

        </div>


        
        <!-----------------------------------Mostrar datos-------------------------------------->
        <div id="card-subcategories" class="card mb-3">        
            
            <div id="cardd-hea" class="card-header"> 
                <h3>subcategories</h3>
            </div>
            
            <div id="subcategories-data" class="card-body">
                <div class="row">
                        <div class="col-12">
                            <div id="div_table_subcategories" class="table-responsive">                          
                            @if(!is_null($subcategories))                                                   
                                    
                                    <table id="tablesubcategories" class="table table-striped" cellspacing="0" width="100%">
                                        <thead  class="thead-dark">
                                            <tr>            
                                                <th>Id</th>
                                                <th>Code</th>
                                                <th>Name</th>                                                
                                                <th>Description</th> 
                                                <th>Enable</th>
                                                <th>Visible</th>
                                                <th></th>                                                
                                            </tr>
                                        </thead>
                                
                                        <tfoot  class="thead-dark">
                                            <tr>
                                                <th>Id</th>
                                                <th>Code</th>
                                                <th>Name</th>                                                
                                                <th>Description</th> 
                                                <th>Enable</th>
                                                <th>Visible</th>
                                                <th></th>                                                
                                            </tr>
                                        </tfoot>
                                    </table>
                                                
                                @endif                          
                            </div>
                        </div>            
                </div>     
            </div>
        </div>  
        <!-----------------------------------Mostrar datos-------------------------------------->
        
    </div><!--<div class="container-fluid">-->


    <!--------------------Modal Edit data------------------------------------------>        
    
    <div class="modal fade" id="modal-edit-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                  <div class="modal-header modall-he">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <h2 class="modal-title" id="exampleModalLabel">subcategories</h2>
                      
                  </div>
                  <div id="edit-data" class="modal-body">                      
                      
                       <form id="form-modal-edit-data">
                            <input  id="input-modal-edit-data-idsubcategories" type="hidden" />

                            <div class="form-group">
                                <label for="input-modal-edit-data-code" class="col-form-label">Subcategories Code:</label>                                
                                <input  id="input-modal-edit-data-code" class="form-control upper" type="text" size="45" maxlength="30"  placeholder="Subcategories Code."/>
                            </div>

                            <div class="form-group">
                                <label for="input-modal-edit-data-name" class="col-form-label">Subcategories Name:</label>                                
                                <input  id="input-modal-edit-data-name" class="form-control upper" type="text" size="45" maxlength="30"  placeholder="Subcategories Name."/>
                            </div>

                            <div class="form-group">
                                <label for="input-modal-edit-data-description" class="col-form-label">Subcategories Description:</label>                                
                                <input  id="input-modal-edit-data-description" class="form-control" type="text" size="45" maxlength="30"  placeholder="Subcategories Description."/>
                            </div>

                            <div id="div-modal-edit-select-enabled" class="form-group">
                                <label for="modal-edit-enabled-select" class="col-form-label">Enabled</label> <br> 
                                <select id="modal-edit-enabled-select" class="form-control input-sm">
                                    <option value="0">No</option> 
                                    <option value="1" selected>Yes</option>                                         
                                </select>                                
                            </div>

                            <div id="div-modal-edit-select-visible" class="form-group">
                                <label for="modal-edit-visible-select" class="col-form-label">Visible</label> <br> 
                                <select id="modal-edit-visible-select" class="form-control input-sm">
                                    <option value="0">No</option> 
                                    <option value="1" selected>Yes</option>                                    
                                </select>
                            </div>
                            
                       </form>
                  </div>
                  <div class="modal-footer modall-fo">
                      <button id="btt-modal-edit-close" type="button" class="btn btn-danger btn-close" data-dismiss="modal">Close</button>
                      <button id="btt-modal-edit-save" type="button" class="btn btn-success">Save changes</button>
                      <button id="btt-modal-delete" type="button" class="btn btn-warning" style="display: none;">Delete</button>                      
                  </div>
              </div>
          </div>
    </div>        
                                
    <!--------------------Fin Modal Edit------------------------------------------>
            
                    
    <!--------------------Modal add cities by countries------------------------------------------>        
    
    <div class="modal fade" id="modal-add-subcategories" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                  <div class="modal-header modall-he">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                      
                      <h2 class="modal-title" id="exampleModalLabel">subcategories</h2>
                      
                  </div>
                  <div id="edit-data" class="modal-body">                      
                      
                       <form id="form-modal-add-subcategories" name="form_add_csubcategories" method="post" action="">
                            <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label for="input-modal-add-subcategories-code" class="col-form-label">Subcategories Code:</label>                                
                                <input  id="input-modal-add-subcategories-code" class="form-control upper" name="subcategories-code" type="text" size="45" maxlength="30"  placeholder="Subcategories Code." required/>
                            </div>

                            <div class="form-group">
                                <label for="input-modal-add-subcategories-name" class="col-form-label">Subcategories Name:</label>                                
                                <input  id="input-modal-add-subcategories-name" class="form-control upper" name="subcategories-name" type="text" size="45" maxlength="30"  placeholder="Subcategories Name." required/>
                            </div>

                            <div class="form-group">
                                <label for="input-modal-add-subcategories-description" class="col-form-label">Description:</label>                                
                                <input  id="input-modal-add-subcategories-description" class="form-control" class="" name="subcategories-description" type="text" size="45" maxlength="30"  placeholder="Subcategories Description." required/>
                            </div>

                            <div id="div-modal-add-select-enabled" class="form-group">
                                <label for="modal-add-enabled-select" class="col-form-label">Enabled</label> <br> 
                                <select id="modal-add-enabled-select" name="subcategories-enabled" class="form-control input-sm">
                                    <option value="0">No</option> 
                                    <option value="1" selected>Yes</option>                                         
                                </select>                                
                            </div>

                            <div id="div-modal-add-select-visible" class="form-group">
                                <label for="modal-add-visible-select" class="col-form-label">Visible</label> <br> 
                                <select id="modal-add-visible-select" name="subcategories-visible" class="form-control input-sm">
                                    <option value="0">No</option> 
                                    <option value="1" selected>Yes</option>                                    
                                </select>
                            </div>
                            
                       </form>
                  </div>
                  <div class="modal-footer modall-fo">
                      <button id="btt-modal-add-close" type="button" class="btn btn-danger btn-close" data-dismiss="modal">Close</button>
                      <button id="btt-modal-add-subcategories" type="button" class="btn btn-success">Add</button>                                            
                  </div>
              </div>
          </div>
     </div>        
    
    <!--------------------Fin Modal add cities by countries------------------------------------------>


     <!--------------------Msg Modal------------------------------------------>             
     <div class="modal fade" id="modal-msg" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                  <div class="modal-header modall-he">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                      <h2 class="modal-title" id="exampleModalLabel">Inventory KW</h2>
                  </div>
                  <div id="error-msg" class="modal-body">
                     {{$error_msg}}
                  </div>
                  <div class="modal-footer modall-fo">
                      <button id="btt-id-err-msg" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                      
                  </div>
              </div>
          </div>
     </div>                   
    <!--------------------Fin Msg Modal------------------------------------------>       


    <!--------------------Modal confirm------------------------------------------>        
    <div class="modal fade" id="modal-confirm-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header modall-he">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        <h2 class="modal-title" id="exampleModalLabel">Subcategories</h2>
                        
                    </div>
                    <div id="div-confirm-data-text" class="modal-body">                      
                                                                                                
                    </div>
                    <div class="modal-footer modall-fo">
                        <button id="btt-modal-confirm-close" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button id="btt-modal-confirm" type="button" class="btn btn-success">Ok</button>                      
                    </div>
                </div>
            </div>
    </div>                               

</div> <!--div class="wrapper"--> 
@endsection

@section('scripts')
    <script src="{{ asset('js/jq/subcategories_jq.js') }}" defer></script>                         
@show
