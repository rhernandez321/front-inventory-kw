@extends('layouts.app')

@section('content')
<div id="content">

    @include('layouts.navbar')

    <div class="container-fluid p-md-4">    

        <div class="content-wrapper">

            <section class="content-header card-header">
                <h1><i class="fa fa-edit"></i>Create Products</h1>
            </section>

            <section class="content">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-12">

                                <form id="form-master-products" method="POST" action="" class="needs-validation" novalidate>
                                
                                    <input name="_token" type="hidden"  value="{{ csrf_token() }}">

                                    <!--input  id="form-mp-input-moneytypes" name="input-fmp-moneytypes" type="hidden" value = "" disabled/>
                                    <input  id="form-mp-input-moneyfactor" name="fmp-price" type="hidden" value = "" disabled/--> 
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="form-mp-input-nrofactura" class="col-form-label">Nro. Bill :</label> 
                                                    <input  id="form-mp-input-nrofactura" class="form-control solonro" name="fmp-nrofactura" type="text" placeholder="Nro. Bill" maxlength="30" required/>
                                                    <div class="invalid-feedback">Please fill out this field.</div> 
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <div class="form-group input-group date" data-provide="datepicker">
                                                    <label for="form-mp-input-datepurchase" class="col-form-label">Date purchase:</label> 
                                                    <div class="input-group">
                                                        <input id="form-mp-input-datepurchase" type="text" class="form-control" required/>
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-calendar-alt"></i>
                                                            </span>
                                                        </div>
                                                        <div class="invalid-feedback">Please fill out this field.</div>                                                        
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                            ​    <div class="text-center">
                                                    <img src="" id="previewImg" width="100%" height="100%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="row">
                                            
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="form-mp-input-name" class="col-form-label">Name:</label>                                
                                                    <input  id="form-mp-input-name" class="form-control upper" name="fmp-name" type="text" placeholder="Name." size="45" maxlength="35" required/>
                                                    <div class="invalid-feedback">Please fill out this field.</div>
                                                </div>
                                            </div>


                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="form-mp-select-category" class="col-form-label">Category:</label>                                
                                                    <select id="form-mp-select-category" name="fmp-category" class="form-control input-sm" required>                                                                                                                        
                                                        <option disabled selected value>  </option>
                                                        @foreach($categories as $item)                                                                                                               
                                                            <option value="{{$item->id}}"> {{$item->name}} </option>                                                                                                
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">Please fill out this field.</div>    
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="form-mp-select-subcategory" class="col-form-label">Subcategory:</label>                                
                                                    <select id="form-mp-select-subcategory" name="fmp-Subcategory" class="form-control input-sm" required>                                                                    
                                                    </select>
                                                    <div class="invalid-feedback">Please fill out this field.</div>     
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="form-mp-input-code" class="col-form-label">Code:</label>                                
                                                    <input  id="form-mp-input-code" class="form-control upper" name="fmp-code" type="text" placeholder="Code" size="45" maxlength="35" required/>
                                                    <div class="invalid-feedback">Please fill out this field.</div>                                                        
                                                </div>
                                            </div>

                                            <div class="col-md-8">
                                                <label for="form-mp-input-description" class="col-form-label">Description:</label>                                
                                                <input  id="form-mp-input-description" class="form-control" type="text"/>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="form-mp-select-presentation" class="col-form-label">Presentation:</label>                                
                                                    <select id="form-mp-select-presentation" name="fmp-presentation" class="form-control input-sm">                                                    
                                                        <option disabled selected value>  </option>
                                                        @foreach($presentations as $item)                                                                                                               
                                                            <option value="{{$item->id}}"> {{$item->name}} </option>                                                                                                
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="form-mp-select-unit" class="col-form-label">Unit:</label>                                
                                                    <select id="form-mp-select-unit" name="fmp-unit" class="form-control input-sm" required>
                                                        <option disabled selected value>  </option>
                                                        @foreach($unitTypes as $item)                                                                                                               
                                                            <option value="{{$item->id}}"> {{$item->name}} </option>                                                                                                
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="form-mp-select-color" class="col-form-label">Color:</label>                                
                                                    <select id="form-mp-select-color" name="fmp-color" class="form-control input-sm">                                                    
                                                        <option disabled selected value>  </option>
                                                        @foreach($colors as $item)                                                                                                               
                                                            <option value="{{$item->id}}"> {{$item->name}} </option>                                                                                                
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="customFileLang" class="col-form-label">Image:</label>                                                                                                           
                                                    <div class="custom-file">                                                        
                                                        <input id="customFileLang" type="file" class="custom-file-input" lang="es">
                                                        <label class="custom-file-label" for="customFileLang">Select file</label>
                                                    </div>  
                                                </div>
                                            </div>  

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">
                                        
                                            <div class="col-md-3">
                                                <div class="form-group">                                           
                                                    <label for="form-mp-select-status" class="col-form-label">Status:</label>                                
                                                    <select id="form-mp-select-status" name="fmp-status" class="form-control input-sm" required>
                                                        <option disabled selected value>  </option>
                                                        @foreach($status as $item)                                                                                                               
                                                            <option value="{{$item->id}}"> {{$item->name}} </option>                                                                                                
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">Please fill out this field.</div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="form-mp-input-qtv" class="col-form-label">Qtv:</label>                                
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">123</span>
                                                        </div>
                                                        <input  id="form-mp-input-qtv" class="form-control" name="fmp-qtv" type="number" step="0.01" placeholder="Qtv." size="45" maxlength="15" value="0" required/>
                                                    </div>    
                                                    <div class="invalid-feedback">Please fill out this field.</div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="form-mp-input-cost" class="col-form-label">Cost:</label>                                
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">{{$moneys->simbolo}}</span>
                                                        </div>
                                                        <input  id="form-mp-input-cost" class="form-control" name="fmp-cost" type="number" step="0.01" placeholder="Cost." size="45" maxlength="15" value="0.00" required/>
                                                    </div>
                                                    <div class="invalid-feedback">Please fill out this field.</div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="form-mp-input-price" class="col-form-label">Price:</label>                                
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">                                                                
                                                            <span class="input-group-text">{{$moneys->simbolo}}</span>
                                                        </div>
                                                        <input  id="form-mp-input-price" class="form-control" name="fmp-price" type="number" step="0.01" placeholder="Price." size="45" maxlength="15" value="0.00" required/>
                                                    </div>
                                                    <div class="invalid-feedback">Please fill out this field.</div>
                                                </div>
                                            </div>
                                        
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="form-mp-select-provider" class="col-form-label">Provider:</label>
                                                    <select id="form-mp-select-provider" name="fmp-provider" class="form-control input-sm" required>
                                                        <option disabled selected value>  </option>
                                                        @foreach($providers as $item)                                                                                                               
                                                            <option value="{{$item->id}}"> {{$item->name}} </option>                                                                                                
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">Please fill out this field.</div>
                                                </div>
                                            </div>  

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="form-mp-select-review" class="col-form-label">Review:</label>                                
                                                    <select id="form-mp-select-review" name="fmp-enabled" class="form-control input-sm">                                                                                                                       
                                                        <option disabled selected value>  </option>
                                                        @foreach($reviews as $item)                                                                                                               
                                                            <option value="{{$item->id}}"> {{$item->name}} </option> 
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="form-mp-select-wharehouse" class="col-form-label">Wharehouse:</label>                                
                                                    <select id="form-mp-select-wharehouse" name="fmp-wharehouse" class="form-control input-sm" required>
                                                        <option disabled selected value></option>
                                                        @foreach($wharehouses as $item)                                                                                                               
                                                            <option value="{{$item->id}}"> {{$item->name}}</option>                                                                                                
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">Please fill out this field.</div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="form-mp-select-locations" class="col-form-label">Location:</label>                                
                                                        <select id="form-mp-select-locations" name="fmp-location" class="form-control input-sm" required> 
                                                        </select>
                                                    <div class="invalid-feedback">Please fill out this field.</div>
                                                </div>
                                            </div>                                          

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="form-mp-select-sections" class="col-form-label">Section:</label>                                
                                                    <select id="form-mp-select-sections" name="fmp-section" class="form-control input-sm" required>
                                                    </select>
                                                    <div class="invalid-feedback">Please fill out this field.</div>   
                                                </div>
                                            </div>  

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="form-mp-select-alloys" class="col-form-label">Alloys:</label>                                
                                                    <select id="form-mp-select-alloys" name="fmp-enabled" class="form-control input-sm">                                                                                                                       
                                                        <option disabled selected value>  </option>
                                                        @foreach($alloys as $item)                                                                                                               
                                                            <option value="{{$item->id}}"> {{$item->name}} </option> 
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="form-mp-select-enabled" class="col-form-label">Enabled:</label>                                
                                                    <select id="form-mp-select-enabled" name="fmp-enabled" class="form-control input-sm" required>
                                                        <option value="0">No</option> 
                                                        <option value="1" selected>Yes</option>                                    
                                                    </select>
                                                    <div class="invalid-feedback">Please fill out this field.</div>
                                                </div>
                                            </div>

                                                                                    

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <button id = "btn-add" type="button" class="btn btn-info btn-lg btn-block btn-send" data-toggle="tooltip" title="Add products">                                            
                                            Add
                                        </button>   
                                    </div>  

                                </form> <!--form master-products-->  
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div> 
                
    </div><!--<div class="container-fluid">-->

        

     <!--------------------Msg Modal------------------------------------------>             
     <div class="modal fade" id="modal-msg" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                  <div class="modal-header modall-he">
                      <h2 class="modal-title" id="exampleModalLabel">Inventory KW</h2>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div id="error-msg" class="modal-body">
                     {{$error_msg}}
                  </div>
                  <div class="modal-footer modall-fo">
                      <button id="btt-id-err-msg" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>                      
                  </div>
              </div>
          </div>
     </div>                   
    <!--------------------Fin Msg Modal------------------------------------------>       

</div> 
@endsection

@section('scripts')
    <script src="{{ asset('js/jq/masterproducts_jq.js') }}" defer></script>
@show