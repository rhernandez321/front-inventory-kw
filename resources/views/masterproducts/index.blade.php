@extends('layouts.app')

@section('content')
<div id="content">

    @include('layouts.navbar')
            
    <div class="container-fluid  p-md-4">

        <div id="card-ppal" class="card bg- mb-3">
            <div class="card-header" id="cardd-hea">
                <h1>Master Products</h1>
            </div>
            <div class="card-body">
                <div class="row">

                    <div class="col-5">
                        <div class="form-group">
                            <form id="form-master-search" method="post" action="">
                                <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                                <div class="input-group mb-3">
                                    <input  id="input_find_products" class="form-control" type="search" size="30" name="find_products"  placeholder="Name / Code." />                        
                                    <button id="btn-search" class="btn btn-default" type="button">
                                        <span class="fa fa-search  icon"></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-5">
                    </div>
                    <div class="col-2">
                        <div class="col-1">                                                                      
                            <form  method="GET" action="{{ route('masterproducts.create') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">                                  
                                <button id = "btn-add" type="submit" class="btn btn-info" data-toggle="tooltip" title="Add products">
                                    <span class="fa fa-plus" aria-hidden="true"></span>                                    
                                </button>        
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

         <!-----------------------------------Mostrar datos-------------------------------------->
        
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div id="data" class="table-responsive">
                            <div id="div_table_masterproducts">
                            @if(!is_null($data_result))
                                <table id="tablemasterproducts" class="table table-striped">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Id</th>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Provider</th>
                                            <th>Color</th>
                                            <th>Presentation</th>
                                            <th>Unit</th>
                                            <th>Category</th>
                                            <th>Subcategory</th>
                                            <th>Review</th>
                                            <th>Qtv</th>
                                            <th>Cost</th>
                                            <th>Price</th>                                                
                                            <th>Alloy</th>
                                            <th>Status</th>
                                            <th>Enabled</th>
                                            <th>Cost</th>
                                            <th>Price</th>                                                
                                            <th>Alloy</th>
                                            <th>Status</th>
                                            <th>Enabled</th>
                                        </tr>
                                    </thead>
                                </table>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/jq/masterproducts_index_jq.js') }}" defer></script>
@show