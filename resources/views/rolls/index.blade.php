
<!DOCTYPE html>
<html>
  <head>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>    
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

        <!--DatTable-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.22/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.22/datatables.min.js"></script>     

        <script src="/assets/js/funciones_js.js" type="text/javascript"></script>
        <script src="/assets/js/funciones_jq.js" type="text/javascript"></script>
                
  </head>

  <title>Inventory KW</title>

<body >
    <div class="container-fluid">    
        <div id="card-ppal" class="card bg-info mb-3">		        	
			
            <div class="card-header">
                  <h1>Rolls </h1>
            </div> 
            <!------------------------------------>
            <div name= "card-body-ppal" class="card-body">
                <div name="row1-card-body-ppal" class="row">
                    <div name="col1-row1-card-body-ppal" class="col-11">                                                                              
                        <div class="form-group">
                            <form id="form-research" method="post" action="{{ route('rolls.index') }}">
                                <!--input name="_method" type="hidden" value="GET"-->
                                <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                                
                                <div name="row1-form-research" class="row">
                                    <div name="col1-row1-form-research" class="col-5"> 
                                        <div class="form-group">
                                            <label for="input_find_rolls" class="col-form-label">Global Research:</label>                                           
                                            <input  id="input_find_rolls" class="form-control" type="text" size="30" name="find_status"  placeholder="Name / Description." value="{{$_data}}" />
                                            <button id="btn-find-rolls" type="button" class="btn-outline-info ">Research</button>                                                                                               
                                        </div>
                                    </div>
                                                                       
                                </div>                                                     
                            </form>
                        </div>
                    </div><!--<div id="col1-row1-card-body-ppal" class="col-11">-->
                                        
                </div><!--<div id="row1-card-body-ppal" class="row">-->                
            </div> <!--<div id= "card-body-ppal" class="card-body">-->
            <!------------------------------------>
        </div><!--<div id="card-ppal" class="card bg-info mb-3">-->


        
        <!-----------------------------------Mostrar datos------------------------------------->
        <div id="card-reviews" class="card bg-info mb-3">        
            
            <div class="card-header">                                    
                  <div class="row">
                        <div  class="col-11"> 
                            <h5>Rolls</h5>
                        </div>
                        <div  class="col-1"> 
                            <button id = "btn-add" type="button" class="btn btn-info" data-toggle="tooltip" title="Add rolls">
                                <span class="fa fa-plus" aria-hidden="true"></span>                                    
                            </button>                                                
                        </div>
                                
                  </div>
                  
            </div>
            <div id="data" class="card-body">
                <div class="row">
                        <div class="col-12">
                            <div id="div_table_rolls" class="table-responsive">                          
                            @if(!is_null($data_result))
                                                                                       
                                    <table id="tablerolls" class="display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>            
                                                <th>Id</th>                                                     
                                                <th>Name</th>                                                                                                
                                                <th>Description</th>                                                
                                                <th>Enable</th>
                                                <th>Visible</th>
                                                <th></th>                                                                                                
                                            </tr>
                                        </thead>
                                
                                        <tfoot>
                                            <tr>
                                                <th>Id</th>                                                     
                                                <th>Name</th>
                                                <th>Description</th>                                                
                                                <th>Enable</th>
                                                <th>Visible</th>
                                                <th></th>                                                                                                
                                            </tr>
                                        </tfoot>
                                    </table>
                                                
                                @endif                          
                            </div>
                        </div><!--<div class="col-12">-->               
                </div> <!--<div class="row">-->       
            </div><!--<div id="data" class="card-body">-->
        </div><!--<div id="card-rolls" class="card bg-info mb-3"> -->    
        <!-----------------------------------Mostrar datos------------------------------------->

        

        
        
    </div><!--<div class="container-fluid">-->


    <!--------------------Modal Edit data------------------------------------------>        
    
    <div class="modal fade" id="modal-edit-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                  <div class="modal-header" style="flex-direction:column;">
                      <h5 class="modal-title" id="exampleModalLabel">Inventory KW</h5>
                      <h6 class="modal-title" id="exampleModalLabel">Rolss</h6>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div id="edit-data" class="modal-body">                      
                      
                       <form id="form-modal-edit-data">
                            <input  id="input-modal-edit-data-idrolls" type="hidden" />                                
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="input-modal-edit-data-name" class="col-form-label">Rolls Name:</label>                                
                                        <input  id="input-modal-edit-data-name" class="form-control" type="text" size="45" maxlength="30"  placeholder="Rolls Name."/>
                                    </div>
                                </div>                                                                
                            </div>
                                                    
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="input-modal-edit-data-description" class="col-form-label">Description:</label>                                
                                        <input  id="input-modal-edit-data-description" class="form-control" type="text" size="45" maxlength="30"  placeholder="Description."/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div id="div-modal-edit-select-enabled" class="form-group">
                                        <label for="modal-edit-enabled-select" class="col-form-label">Enabled</label> <br> 
                                        <select id="modal-edit-enabled-select" class="form-control input-sm">
                                            <option value="0">No</option> 
                                            <option value="1" selected>Yes</option>                                         
                                        </select>                                
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div id="div-modal-edit-select-visible" class="form-group">
                                        <label for="modal-edit-visible-select" class="col-form-label">Visible</label> <br> 
                                        <select id="modal-edit-visible-select" class="form-control input-sm">
                                            <option value="0">No</option> 
                                            <option value="1" selected>Yes</option>                                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                                                                                    
                       </form>
                  </div>
                  <div class="modal-footer">
                      <button id="btt-modal-edit-close" type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Close</button>
                      <button id="btt-modal-edit-save" type="button" class="btn btn-primary">Save changes</button>
                      <button id="btt-modal-delete" type="button" class="btn btn-danger" style="display: none;">Delete</button>                      
                  </div>
              </div>
          </div>
    </div>        
                                
    <!--------------------Fin Modal Edit------------------------------------------>
            
                    
    <!--------------------Modal add ------------------------------------------>        
    
    <div class="modal fade" id="modal-add-rolls" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                  <div class="modal-header" style="flex-direction:column;">
                      <h5 class="modal-title" id="exampleModalLabel">Inventory KW</h5>
                      
                      <h6 class="modal-title" id="exampleModalLabel">Rolls</h6>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div id="edit-data" class="modal-body">                      
                      
                       <form id="form-modal-add-rolls" name="form_add_rolls" method="post" action="">
                            <input name="_token" type="hidden"  value="{{ csrf_token() }}">
                            
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="input-modal-add-name" class="col-form-label">Rolls Name:</label>                                
                                        <input  id="input-modal-add-name" class="form-control" name="rolls_name" type="text" size="45" maxlength="30"  placeholder="Rolls Name." required/>
                                    </div>
                                </div>
                            </div>  
                                                                                        
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="input-modal-add-description" class="col-form-label">Description:</label>                                
                                        <input  id="input-modal-add-description" class="form-control" name="reviews_description" type="text" size="45" maxlength="30"  placeholder="Description." required/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div id="div-modal-add-select-enabled" class="form-group">
                                        <label for="modal-add-enabled-select" class="col-form-label">Enabled</label> <br> 
                                        <select id="modal-add-enabled-select" name="rolls_enabled" class="form-control input-sm">
                                            <option value="0">No</option> 
                                            <option value="1" selected>Yes</option>                                         
                                        </select>                                
                                    </div>
                                </div>
                        
                                <div class="col-6">
                                    <div id="div-modal-add-select-visible" class="form-group">
                                        <label for="modal-add-visible-select" class="col-form-label">Visible</label> <br> 
                                        <select id="modal-add-visible-select" name="rolls_visible" class="form-control input-sm">
                                            <option value="0">No</option> 
                                            <option value="1" selected>Yes</option>                                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                       </form>
                  </div>
                  <div class="modal-footer">
                      <button id="btt-modal-add-close" type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Close</button>
                      <button id="btt-modal-add" type="button" class="btn btn-primary">Add</button>                                            
                  </div>
              </div>
          </div>
     </div>        
    
    <!--------------------Fin Modal add cities by countries------------------------------------------>


     <!--------------------Msg Modal------------------------------------------>             
     <div class="modal fade" id="modal-msg" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Inventory KW</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div id="error-msg" class="modal-body">
                     {{$error_msg}}
                  </div>
                  <div class="modal-footer">
                      <button id="btt-id-err-msg" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>                      
                  </div>
              </div>
          </div>
     </div>                   
    <!--------------------Fin Msg Modal------------------------------------------>       


    <!--------------------Modal confirm------------------------------------------>        
    <div class="modal fade" id="modal-confirm-data" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header" style="flex-direction:column;">
                        <h5 class="modal-title" id="exampleModalLabel">Inventory KW</h5>
                        <h6 class="modal-title" id="exampleModalLabel">Reviews</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="div-confirm-data-text" class="modal-body">                      
                                                                                                
                    </div>
                    <div class="modal-footer">
                        <button id="btt-modal-confirm-close" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button id="btt-modal-confirm" type="button" class="btn btn-success">Ok</button>                      
                    </div>
                </div>
            </div>
    </div>                               
    <!--------------------Fin Modal confirm------------------------------------------>

</body>

<!--BLOQUE DE CODIGO javascript/jquery-->
<script type="text/javascript" language="JavaScript">


//Bloque JQuery
$(function(){
      
    
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });


    //Leer la variable de php $err_msg y verificar si tiene o no valor
     
    if(!(@json($error_msg) == '')){                     
            $("#modal-msg").modal("show");
           
     }

     /** Realizar la primera recarga de data */
    researchData('');
        
    /**Se ejecuta cuando se recibe una respuesta sactifactoria del ajax que actualiza */    
    function successUpdate(datos) {          
       if(datos['_ok'] == '0'){
            closeModal('.btn-close'); //Indicamos la clase de cerrar modal
            refreshDataTable()
        }
        showModalMessage(datos['_msg']); //(funciones_js.js)
                
    }

   
    /**
    Función que realiza la carga el datatable de la data */
    function researchData($datafind){
        
        //Botones update y delete
        var btn_html = "<button type='button' class='edit btn btn-outline-warning' data-toggle='modaledit' data-target='#modaledit'><span class='fa fa-wrench' aria-hidden='true'></span></button>"
        btn_html = btn_html + "&nbsp;&nbsp;<button type='button' class='delete btn btn-outline-danger' data-toggle='modaledit' data-target='#modaldelete'><span class='fa fa-trash' aria-hidden='true'></span></button>"
                
        var datatable = $('#tablerolls').DataTable({
         
           // processing: true,
           // serverSide: true,
            //serverMethod: 'post',
            responsive: true,
            
            ajax: {url: "{{route('rolls_research')}}",         
                    type: "post",
                    //dataSrc: "_data",
                    data: {"iddata": $datafind},                    
                    dataSrc: function (data) {
                        console.log(data);
                        if (data['_ok'] == '0')
                        {                            
                            console.log(data["_data"]);
                            return data['_data'];
                        }else{
                            console.log(data["_msg"]);
                            showModalMessage(data['_msg']);                            
                            data['_data'] = [];
                            return data['_data']
                        }                           
                    } 
            },
            
            columns: [                        
                        {data:"id"},                      
                        {data:"name"},
                        {data:"description"},                        
                        {
                            bSortable: false,
                            data:"enabled",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }
                        },
                        {   bSortable: false,
                            data:"visible",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }
                        },
                        {  
                            bSortable: false,
                            data: null, 
                            "defaultContent": btn_html                           
                        }
                        
                        
                       /*Concatena campos 
                       { 
                            "data": null,
                            "className": "button",
                            "defaultContent": '<button name="vcvcvc" id="' + {data: "name"} + '" value="id" OnClick="EditRow(this);"  class="btn btn-primary" data-toggle="modal" data-target="#myModal">Editar</button>'
                        }*/                                            
                        
            ],     
            columnDefs: [
                          { "visible": false, "targets": [0] },
                          
                        ],         
            select:true
        });

      //  editar("#tablecountries",datatable);
    }//Fin researchcountries
         

    /**
    Capturar la acción clip de los botones de la tabla countries
     */
    $('#tablerolls').on('click', "[data-toggle='modaledit']", function(){
           
           table = $('#tablerolls').DataTable();  
           var row = $(this).parents('tr')[0]; 
           var data = table.row( row ).data();
                     
           console.log( data);           
           $('#input-modal-edit-data-idrolls').val(data.id);          
           if($(this).hasClass("edit")){                            
                $('#input-modal-edit-data-name').val(data.name);
                $('#input-modal-edit-data-name').prop('disabled', false);                
                $('#input-modal-edit-data-description').val(data.description);
                $('#input-modal-edit-data-description').prop('disabled', false);
                $('#modal-edit-enabled-select').val(data.enabled);
                $('#modal-edit-visible-select').val(data.visible);

                $('#div-modal-edit-select-visible').show();
                $('#div-modal-edit-select-enabled').show();
                $('#btt-modal-edit-save').show();                            
                $('#btt-modal-delete').hide();
                                
                //$('#modal-edit-data').modal({backdrop: 'static', keyboard: false})
            
                $("#modal-edit-data").modal("show");                                 
           }            
           else{                                
               // $('#input-modal-edit-data-idalloy').val(data.id);               
                $('#input-modal-edit-data-name').prop('disabled', true);
                $('#input-modal-edit-data-name').val(data.name);        
                $('#input-modal-edit-data-description').prop('disabled', true);
                $('#input-modal-edit-data-description').val(data.description);         
                $('#div-modal-edit-select-visible').hide();
                $('#div-modal-edit-select-enabled').hide();
                $('#btt-modal-edit-save').hide();                            
                $('#btt-modal-delete').show();                            
                
                $("#modal-edit-data").modal("show");                   
                        
           }
                     
   });
  
   
    /**
    Mostrar modal add
     */
    $('#btn-add').on({click:                
        function (event)
        {            
            event.preventDefault();  
           
            $('#input-modal-add-code').val('');
            $('#input-modal-add-simbol').val('');
            $('#input-modal-add-name').val('');
            $('#input-modal-add-factor').val('');
            $('#input-modal-add-description').val('');
                        
            $("#modal-add-rolls").modal('show');
                                                                                                                                       
        }//function (event)
    }); // 
    
    /**
    Función que ejecuta la acción de refresch del datatable
     */
    function refreshDataTable()
    {                 
            if ( $.fn.dataTable.isDataTable( '#tablerolls' ) ) {
               
                $('#tablerolls').DataTable().searching = false;
                $('#tablerolls').DataTable().paging = false;
                $('#tablerolls').DataTable().destroy();
            }       
            
            researchData($('#input_find_rolls').val());                                        
    }     

    
    /**Ejecutar la acción de agregar */
    $('#btt-modal-add').on({click:
        function (event)
        {     
            event.preventDefault(); 
                                  
            if($('#input-modal-add-name').val() === ''){
                $("#error-msg").text("Insert roll name.");
                $("#modal-msg").modal("show");
            }            
            else{
                //$("form").submit();  
                //$("#form-modal-add-countrie").submit();  
            
                $('#div-confirm-data-text').html('Add roll?');
                $('#btt-modal-confirm').addClass('add');            
                $("#modal-confirm-data").modal('show');
            
            }
             
        }
    }); //Fin $('#id-input-send-add-country').on({click:
     
    $('#btt-modal-confirm').on({click:    
            function (event)
            {   
                
                event.preventDefault();  
                                           
                $("#modal-confirm-data").modal('hide');
    
                if($(this).hasClass("edit")){                     
                    $('#btt-modal-confirm').removeClass('edit');
                    updateData('edit');
                }
                else
                if($(this).hasClass("delete")){                   
                    $('#btt-modal-confirm').removeClass('delete'); 
                    updateData('delete');
                }
                else
                if($(this).hasClass("add")){
                    $('#btt-modal-confirm').removeClass('add'); 
                    //$("#form-modal-add-countrie").submit(); 
                    addData();
                }
                            
            }
     });//$('#b


    /**
    Función que ejecuta la acción de búsqueda
     */
    $('#btn-find-rolls').on({click:
         function (event)
         {
            event.preventDefault(); 
            
            refreshDataTable();            
                                               
           // researchcountries($('#input_find_categories').val());
         }
    })

    /**
     * Function que ejecuta el ajax de actualización de la data (update, delete)
     *  */ 
    function updateData(action){
                    

            var data = new Object();
                        
            var Obj_Params = new Object();
                      
            //var idbtn = $(this).prop('id');
            
            data.action      = action;                                                          
            data.id          = $('#input-modal-edit-data-idrolls').val();    
            data.name        = $('#input-modal-edit-data-name').val();            
            data.description = $('#input-modal-edit-data-description').val();            
            data.enabled     = $('#modal-edit-enabled-select').val();
            data.visible     = $('#modal-edit-visible-select').val();  
            
            //Obj_Params.msg = 'Operación exitosa';
            if(!(data.action == '')){
               EjecutarAjaxNew(data, '{{ route("rolls.action") }}', 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
            }
    }//Fin updateDataCountry

    /**
     * Función que permite agrgar nueva money
     */
    function addData(){
                    

                    var data = new Object();
                                
                    var Obj_Params = new Object();
                              
                    //var idbtn = $(this).prop('id');
        
                    data.action      = 'add';                                                                                                                      
                    data.name        = $('#input-modal-add-name').val();                    
                    data.description = $('#input-modal-add-description').val();
                    data.enabled     = $('#modal-add-enabled-select').val();
                    data.visible     = $('#modal-add-visible-select').val();  

        
                    //Obj_Params.msg = 'Operación exitosa';
                    if(!(data.action == '')){
                       EjecutarAjaxNew(data, '{{ route("rolls.action") }}', 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
                    }
            }//Fin updateDataCountry



})//Fin $(function(){
</script>


</html>

