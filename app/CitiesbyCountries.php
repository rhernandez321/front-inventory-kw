<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CitiesbyCountries extends Model
{
    //
    protected $table='citiesbycountries';
    
    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'name', 'postalcode', 'enabled', 'visible', 'id_country' , 'id_user');

    //protected $hidden = ['created_at'];
    public $timestamps = false;
}
