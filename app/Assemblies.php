<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assemblies extends Model
{
    //
    protected $table='assamblies';

    protected $primaryKey = 'id';
    protected $fillable =  ['code', 
                            'name', 
                            'description', 
                            'precio', 
                            'costo', 
                            'id_color',
                            'id_review', 
                            'enabled', 
                            'visible', 
                            'id_user',
                            'idcompany'
                            ];

    public $timestamps = false;
}
