<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterProdcuts extends Model
{
    //
    protected $table='master_products';

    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'id_company', 'idprov', 'idcol', 'idwh', 'idcat', 'idscat', 'idrv', 'name',
                                 'description', 'qtv', 'cost', 'price', 'factormoney',  'enabled', 'visible',
                                 'id_user', 'id_moneytype', 'id_status', 'id_alloy', 'id_unit', 'id_typemovement',
                                 'idlocation', 'ruta_img');
    public $timestamps = false;

}
