<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    //
    protected $table='companies';

    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'name', 'description', 'address', 'id_country', 'id_city', 'enabled', 'visible', 'id_user');

    public $timestamps = false;
}
