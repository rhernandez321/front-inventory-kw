<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reviews extends Model
{
    //
    protected $table='reviews';

    protected $primaryKey = 'id';
    protected $fillable =  array('name', 'description', 'enabled', 'visible', 'id_company', 'id_user');

    public $timestamps = false;
}
