<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rolls extends Model
{
    //
    protected $table='rolls';

    protected $primaryKey = 'id';
    protected $fillable =  array('name', 'description', 'enabled', 'visible', 'id_user');
    public $timestamps = false;

}
