<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    //
    protected $table='purcharse_orders';

        protected $primaryKey = 'id';
        protected $fillable =  [
                                'refcode',
                                'idcompany',
                                'idprovider',
                                'providername',
                                'provideraddress',
                                'totalcost',
                                'costofshipping',
                                'tax',
                                'totalgral',
                                'idmoneytype',
                                'factormoney',
                                'canceled',
                                'datecanceled',
                                'idusercanceled',
                                'confirmed',
                                'dateconfirmed',
                                'iduserconfirmed',
                                'iduser',                            
                            ];

        public $timestamps = false;

}
