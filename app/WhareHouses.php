<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhareHouses extends Model
{
    //
    protected $table='wharehouses';

    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'name', 'description', 'enabled', 'visible', 'id_user');

    public $timestamps = false;


    
    
}
