<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alloys extends Model
{
    //
    protected $table='alloys';

    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'name', 'description', 'enabled', 'visible', 'id_user');

    public $timestamps = false;
}
