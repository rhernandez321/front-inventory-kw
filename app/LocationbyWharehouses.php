<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationbyWharehouses extends Model
{
    //
    protected $table='locationbywharehouses';

    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'name', 'description', 'id_company',
                                 'id_wharehouse', 'enabled', 'visible', 'id_user');

    public $timestamps = false;


   

}
