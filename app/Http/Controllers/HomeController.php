<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /*public function indexCountries()
    {         
        return view('countries.index');
    }*/

   /* public function indexCitiesbycountries()
    {   
        $countries = [];
        
        return view('citiesbycountries.index', compact('countries'));        
    
    }*/
}
