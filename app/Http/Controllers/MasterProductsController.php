<?php

namespace App\Http\Controllers;

use App\LocationbyWharehouses;
use App\Categories;
use App\SubCategories;
use App\Status;
use App\Presentations;
use App\UnitTypes;
use App\Colors;
use App\Moneys;
use App\WhareHouses;
use App\Providers;
use App\Reviews;
use App\Alloys;
use App\Movementstypes;


use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//////////////////////////

use View;
use Redirect;
use Exception;

use Response;

class MasterProductsController extends Controller
{
    private $m_path = "masterproducts";
    private $m_iduser = 1;
    private $m_idcompany = 1;
   
    
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //
        $error_msg = "";
        $_data = "";
        $data_result = [];
        $categories = [];
        $status = [];
        $presentations = [];
        $unitTypes = [];
        $colors = [];
        $moneys = [];
        $wharehouses = [] ;
        $providers = [] ;
     
                
        /*return view($this->m_path . '.index', compact('data_result', 'categories', 'status', 'presentations',
                    'unitTypes', 'colors', 'moneys', 'wharehouses', 'providers', 'error_msg'));   
                    */
        return view($this->m_path . '.index', compact('data_result', 'error_msg'));             
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $error_msg = "";
        
        $data_result = [];
        
        //Buscar las categories habilitadas        
        $categories =  Categories::where('enabled', '=', 1)
                                   ->where('id_company', $this->m_idcompany)
                                   ->select('id', 'code', 'name')
                                   ->get();
        
        //Buscar las status habilitadas        
        $status = Status::where('enabled', '=', 1)                         
                          ->select('id', 'name')
                          ->get();
        //Buscar las presentations habilitadas        
        $presentations = Presentations::where('enabled', '=', 1)  
                                    ->select('id', 'code', 'name')                       
                                    ->get();
        //Obtener todos los UnitTypes        
        $unitTypes = UnitTypes::where('enabled', '=', 1)                         
                            ->get();

        //Obener todos los colors        
        $colors = Colors::where('enabled', '=', 1)                         
                          ->select('id', 'code', 'name')
                          ->get();

        //Obener símbolo del money type base        
        $moneys = Moneys::where('mdefault', '=', 1)
                          //->where('enabled', '=', 1)
                          ->where('id_company', $this->m_idcompany)
                          ->select('simbolo')
                          ->first();
        //dd($moneys);                  
               
        //Obtener los wharehouses
        $wharehouses = WhareHouses::where('enabled', '=', 1)
                                    ->where('id_company', $this->m_idcompany)
                                    ->select('id', 'code', 'name')
                                    ->get();
        //Obtener los providers
        $providers = Providers::all();
        //Obtener los tipos reviews by company
        $reviews = Reviews::where('enabled', '=', 1)
                                ->where('id_company', $this->m_idcompany)
                                ->select('id', 'name')
                                ->get();
                                
        //Obtener los alloys existentes y enabled
        $alloys = Alloys::where('enabled', '=', 1)            
                          ->select('id', 'code', 'name')              
                          ->get();

        //return view($this->m_path.'.create');
        return view($this->m_path . '.create', compact('data_result', 'categories', 'status', 'presentations',
                    'unitTypes', 'colors', 'moneys', 'wharehouses', 'providers', 'reviews', 'alloys', 'error_msg'));   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    

    //Realizar consulta y obtener sections by locations and wharehouse
    private function findSectionsByLocations($_idlocation, $_enabled, &$_error_msg){
            try{
                
                $data_result = DB::select('SELECT S.id, S.code, S.name 
                                        FROM sections AS S
                                        WHERE (S.id_location = :_idlocation) AND ((S.enabled = :_enabled) OR (:_enabled_aux = "all"))
                                        ORDER BY S.name',
                                        ['_idlocation'=>$_idlocation, '_enabled'=>$_enabled,  '_enabled_aux' => $_enabled]);         
                                                                                                                                        
                if(is_null($data_result) || empty($data_result)){               
                    $data_result = [];
                } 
                            
                return $data_result;                                                                         
            }   
            catch(Exception $e){            
                $_error_msg = $e->getMessage(); 
            
                return [];
            }
            catch(\Illuminate\database\QueryException $e){
                $_error_msg = $e->getMessage();                               
                return [];
            }  

    }//findSectionsByLocations


  //Realizar consulta y obtener locations by wharehouse
  private function findLocationsByWharehouse($_idwharehouse, $_enabled, &$_error_msg){
            try{
                
                $data_result = DB::select('SELECT L.id, L.code, L.name 
                                        FROM locationbywharehouses AS L
                                        WHERE (L.id_wharehouse = :_idwharehouse) AND ((L.enabled = :_enabled) OR (:_enabled_aux = "all"))
                                        ORDER BY L.name',
                                        ['_idwharehouse'=>$_idwharehouse, '_enabled'=>$_enabled,  '_enabled_aux' => $_enabled]);                                
                                                                              
                if(is_null($data_result) || empty($data_result)){               
                    $data_result = [];
                } 
                            
                return $data_result;                                                                         
            }   
            catch(Exception $e){            
                $_error_msg = $e->getMessage(); 
               
                return [];
            }
            catch(\Illuminate\database\QueryException $e){
                $_error_msg = $e->getMessage();                               
                return [];
            }  

    }//findLocationsByWharehouse

    /**
    * Función que permite realizar la búsqueda de todos los productos existentes.
    * segú id company y enabled
    */    
    private function findAllProductsByCompanyAndEnabled($_idcompany, $_data, $_enabled,  &$_error_msg)   
    {          
        try{
            
            $data_result = DB::select('SELECT MP.id, MP.code, MP.name, MP.description, P.name AS provider_name, MP.qtv, 
                                              MP.cost, MP.price, M.name AS money_name, M.factor, C.name AS color, 
                                              PR.name AS presentation, UT.name AS unit, CAT.name AS category,
                                              SUBCAT.name AS subcategory, ST.name AS status_name, 
                                              WH.name AS wharehouses, L.name AS location_name, S.name AS section,
                                              A.name AS alloy, R.name AS review, MP.enabled
                                        FROM master_products AS MP
                                        LEFT JOIN providers AS P ON P.id = MP.idprov
                                        LEFT JOIN moneys AS M ON M.id = MP.id_moneytype
                                        LEFT JOIN colors AS C ON C.id = MP.idcol
                                        LEFT JOIN wharehouses AS WH ON WH.id = MP.idwh
                                        LEFT JOIN presentations AS PR ON PR.id = MP.idpre
                                        LEFT JOIN unittypes AS UT ON UT.id = MP.id_unit
                                        LEFT JOIN categorys AS CAT ON CAT.id = MP.idcat
                                        LEFT JOIN subcategorys AS SUBCAT ON SUBCAT.id = MP.idscat
                                        LEFT JOIN status AS ST ON  ST.id = MP.id_status
                                        LEFT JOIN locationbywharehouses AS L ON  L.id = MP.id_location
                                        LEFT JOIN sections AS S ON  S.id = MP.id_section
                                        LEFT JOIN alloys AS A ON  A.id = MP.id_alloy
                                        LEFT JOIN reviews AS R ON  R.id = MP.idrv
                                        WHERE ((MP.id_company = :_idcompany) AND ((MP.code LIKE :_code) OR (MP.name LIKE :_name) OR (MP.description LIKE :_description))) AND (MP.enabled = :_enabled) OR (:_enabled_aux = "all")',
                                        ['_idcompany'=>$_idcompany, ':_enabled' => $_enabled, ':_enabled_aux' => $_enabled, '_code'=>'%'.$_data.'%', '_name'=>'%'.$_data.'%', '_description'=>'%'.$_data.'%']);
                                                                                                          
            
            if(is_null($data_result) || empty($data_result)){               
               $data_result = [];
            } 
                               
            return $data_result;                                                                         
        }   
        catch(Exception $e){            
            $_error_msg = $e->getMessage();              
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                                                                    
            return [];
        }  
                        
    }//findAllProductsByCompanyAndEnabled
    
    
    //**Agregar nuevo producto */
    private function addProduct(Request $request){
        
        try{
             DB::beginTransaction();
             
                        
             
             //Obtener el id del tipo de movimiento Entrada
             $movementstypes = DB::table('movementstypes')
                               ->where('code', 'IN')
                               ->select('id', 'code') 
                               ->first();

             $idmovements     = $movementstypes->id;
             $movementstype = $movementstypes->code;             

             //Obtener el id y el moneyfactor de moneytype default
             $moneys = Moneys::where('mdefault', '=', 1)
                          //->where('enabled', '=', 1)
                          ->where('id_company', $this->m_idcompany)
                          ->select('id', 'factor')
                          ->first();             
             $idmoneytype = $moneys->id;             
             $moneyfactor = $moneys->factor;

             $idprod = DB::table('master_products')->insertGetId([
                                 'id_company'    => $request->idcompany, 
                                 'code'          => $request->code,
                                 'idprov'        => $request->idprovider,
                                 'idcol'         => $request->idcolor,
                                 'idpre'         => $request->idpresentation,
                                 'idwh'          => $request->idwharehouse, 
                                 'idcat'         => $request->idcategory, 
                                 'idscat'        => $request->idsubcategory, 
                                 'idrv'          => $request->idreview, 
                                 'name'          => $request->name, 
                                 'description'   => $request->description,
                                 'qtv'           => $request->qtv, 
                                 'cost'          => $request->cost, 
                                 'price'         => $request->price, 
                                 'factormoney'   => $moneyfactor, 
                                 'enabled'       => $request->enabled, 
                                 'visible'       => $request->visible, 
                                 'id_user'       => $request->iduser, 
                                 'id_moneytype'  => $idmoneytype, 
                                 'id_status'     => $request->idstatus,
                                 'id_alloy'      => $request->idalloy, 
                                 'id_unit'       => $request->idunit, 
                                 'id_location'   => $request->idlocation, 
                                 'id_section'    => $request->idsection, 
                                 'ruta_img'      => $request->rutaimg
            ]);
             
            //Actualizar histórico de movimiento                
            DB::table('movements_histories')->insert([
                      'name'                       => $request->name,
                      'code'                       => $request->code,
                      'id_product'                 => $idprod,
                      'nro_bill'                   => $request->nrobill,
                      'datepurchase'               => $request->datebill,
                      'id_origin_wharehouse'       => null,
                      'id_destination_wharehouse'  => $request->idwharehouse,  
                      'id_assembly'                => null, 
                      'id_provider'                => $request->idprovider, 
                      'id_color'                   => $request->idcolor,
                      'id_presentation'            => $request->idpresentation, 
                      'id_category'                => $request->idcategory,
                      'id_subcategory'             => $request->idsubcategory,
                      'id_review'                  => $request->idreview, 
                      'id_status'                  => $request->idstatus, 
                      'id_alloy'                   => $request->idalloy,
                      'id_location'                => $request->idlocation, 
                      'id_movementtype'            => $idmovements, 
                      'movementstypescode'          => $movementstype, 
                      'id_unittype'                => $request->idunit,
                      'id_moneytype'               => $idmoneytype,
                      'qtv'                        => $request->qtv,
                      'cost'                       => $request->cost, 
                      'price'                      => $request->price, 
                      'moneyfactor'                => $moneyfactor, 
                      'id_user'                    => $request->iduser,
                      'id_company'                 => $request->idcompany
            ]);

                        //Asegurarse de que se fije la transacción
             DB::commit();
             $data_response = array('_ok'=>0, '_data'=> '', '_msg'=>'Successfull Operation');
                        
        }
        catch(exception $e){
            $_error_msg = $e->getMessage();
            
            DB::rollback();
            $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$_error_msg);
        } 
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();
            
            DB::rollback();
            $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$_error_msg);
         }

        return $data_response;             
        
    }//addProduct
  

    //Fin bloque consultas sql

    //INIcio de funciones llamadas desde view con las rutas
    public function products_search(Request $request){        
        if($request->ajax())
        {            
            //$idwharehouse    = request()->get('idwharehouse');
            //$idcompany = request()->get('idcompany');
            
            try{

                $error_msg = "";
                
                $data_result = $this->findAllProductsByCompanyAndEnabled($this->m_idcompany, $request->data, $request->enabled,  $error_msg);
                   
                if ($error_msg === ""){     
                                        
                    $data_response = array('_ok'=> 0,'_data'=> $data_result, '_msg'=>'Successfull Operation');                                 
                
                }
                else{                                  
                    $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
                }
            }
            catch(exception $e){
                $error_msg = $e->getMessage();
                                
                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            } 
            
            return response()->json($data_response, 200);             
        }
    }//Fin product_search

   //Función que realiza la llamada a la consulta de buscar subcategories 
   public function subcategories(Request $request){
       
    if($request->ajax())
    {            
        //$idwharehouse    = request()->get('idwharehouse');
        //$idcompany = request()->get('idcompany');
        
        try{

            $error_msg = "";
            
            //$data_result = $this->findSubCategoriesByCategories($request->idcategory, $request->enabled,  $error_msg);
            $data_result = SubCategories::GetSubCategoriesByCategoryAndStatus($request->idcategory, '1');
            
             
            if ($error_msg === ""){     
                                    
                $data_response = array('_ok'=> 0,'_data'=> $data_result, '_msg'=>'Successfull Operation');                                 
                //dd($data_response);
            }
            else{                                  
                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            }
        }
        catch(exception $e){
            $error_msg = $e->getMessage();
                        
            $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
        } 
        
        return response()->json($data_response, 200);             
    }
   }//Fin subcategories

   
   //Función que realiza la llamada a la consulta de buscar locations 
   public function locations(Request $request){
       
    if($request->ajax())
    {            
        //$idwharehouse    = request()->get('idwharehouse');
        //$idcompany = request()->get('idcompany');
        
        try{

            $error_msg = "";
            
            //Esto retorna un a colección. Según lectura, es más inefeciente que hacer la consulta directamente en sql.
            //$data_result = LocationbyWharehouses::GetLocationsByWharehouseAndStatus($request->idwharehouse, '1');
           //dd($data_result);

            //Buscar locations by wharehouse con sentencia sql.
            $data_result = $this->findLocationsByWharehouse($request->idwharehouse, '1', $error_msg);
           
            if ($error_msg === ""){     
                                    
                $data_response = array('_ok'=> 0,'_data'=> $data_result, '_msg'=>'Successfull Operation');                                 
            
            }
            else{                                  
                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            }
        }
        catch(exception $e){
            $error_msg = $e->getMessage();
                        
            $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
        } 
        
        return response()->json($data_response, 200);             
    }
   }//Fin locations
 

   //Función que realiza la llamada a la consulta de buscar section
   public function sections(Request $request){
       
    if($request->ajax())
    {            
        
        try{

            $error_msg = "";
                   
            //Buscar locations by wharehouse con sentencia sql.
            
            $data_result = $this->findSectionsByLocations($request->idlocation, '1', $error_msg);
           
            if ($error_msg === ""){     
                                    
                $data_response = array('_ok'=> 0,'_data'=> $data_result, '_msg'=>'Successfull Operation');                                 
            
            }
            else{                                  
                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            }
        }
        catch(exception $e){
            $error_msg = $e->getMessage();
                        
            $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
        } 
        
        return response()->json($data_response, 200);             
    }
   }//Fin Sections

   
    /**
     *Función que permite ejecutar la acción de agregar, eliminar o actualizar los productos
    */
    function action(Request $request)
    {             
            if($request->ajax())
            {   
                try{                    
                        $data = [];
                        /*if((($request->action == 'add') || ($request->action == 'edit')) && (!($this->validate_data($request, $data)))){                                                
                            return response()->json($data);
                        }   
                        else*/
                        if($request->action == 'add'){

                            //addProduct($requets)
                            $data = $this->addProduct($request);
                        }
                        else
                        if($request->action == 'edit')
                        {   
                           /* $data = array(                                                       
                                'code'		  =>	$request->code,                        
                                'name'  	  =>	$request->name,                                                             
                                'description' =>	$request->description,                                
                                'enabled'	  =>	$request->enabled,
                                'visible'	  =>	$request->visible
                            );
                                                    
                            DB::table('locationbywharehouses')
                                ->where('id', $request->id)
                                ->update($data);
                            */
                        }
                        else
                        if($request->action == 'delete')
                        {
                            /*DB::table('locationbywharehouses')
                                ->where('id', $request->id)
                                ->delete();*/
                        }
                                                                                
                        //$data = array('_ok'=>0, '_data'=>'', '_msg'=>"Successfull Operation");
                    
                }
                catch(Exception $e){                     
                        $error_msg =  $e->getMessage();                                 
                    
                        $data = array('_ok'=>-1, '_data'=>'', '_msg'=>$error_msg);
                    
                }
                catch(\Illuminate\database\QueryException $e){                           
                    $error_msg = $e->getMessage();                                 
                    $data = array("ok" => -1, '_data'=>'', '_msg'=>$error_msg);   
                    //return response()->json($data);                 
                }
            
            return response()->json($data);
                
           }
    }//action(Request $request)




}//Fin class
