<?php

namespace App\Http\Controllers;

use App\PurchaseOrder;
use App\Moneys;
use App\Providers;
use App\PurcharseOrderDetail;

use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PurchaseOrderController extends Controller
{
    private $m_path = "purchaseorder";
    private $m_idcompany = 1;
    private $m_iduser = 1;
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $error_msg = "";        
        $data_result = [];

        //Obtener el id, simbolo y el moneyfactor de moneytype default
        $money = Moneys::where('mdefault', '=', 1)
        //->where('enabled', '=', 1)
        ->where('id_company', $this->m_idcompany)
        ->select('id', 'factor', 'simbolo', 'name')
        ->first();             
                
        return view($this->m_path . '.create', compact('data_result', 'money', 'error_msg'));   
    }

    /**
     * Obtener la dirección del proveedor
     */
    function providerAddress($id){
        $provider = Providers::where('id', $id)
        ->select('address')
        ->first();        
        return $provider->address;     
    }

    /**
    * Buscar providers habilitados
    * $_enabled: indica el estatus a buscar 1: enabled / 0:disable
    */    
    private function findprovidersbyEnabled($_enabled, &$_error_msg)   
    {          
        try{
                                       

            $data_result = DB::select('SELECT P.id, P.name, P.enabled 
                                 FROM providers AS P
                                 WHERE ((P.enabled = :_enabled))
                                 ORDER BY P.name',
                                 ['_enabled'=>$_enabled]);

            
            if(is_null($data_result) || empty($data_result)){               
               $data_result = [];
            } 
                               
            return $data_result;                                                                         
        }   
        catch(Exception $e){            
            $_error_msg = $e->getMessage();              
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                                                                    
            return [];
        }  
                        
    }//findprovidersbycountryandcity


    /**
     * Permite buscar los productos por company según el status
     * $_idcompany: Id company
     * $_data: Producto a buscar
     * $_enabled: Status a buscar
     * $_error_msg: I/O que retorna el mensaje de error, en caso de que ocurra.
     */
    private function productsByCompanyAndEnabled($_idcompany, $_data, $_enabled,  &$_error_msg)   
    {            
        try{
            
            $data_result = DB::select('SELECT MP.id, MP.code, MP.name, P.name AS provider_name,  
                                              MP.cost, M.simbolo AS moneytype, M.factor
                                        FROM master_products AS MP
                                        INNER JOIN providers AS P ON P.id = MP.idprov
                                        INNER JOIN moneys AS M ON M.id = MP.id_moneytype                                                                               
                                        WHERE ((MP.id_company = :_idcompany) AND ((MP.code LIKE :_code) OR (MP.name LIKE :_name))) AND (MP.enabled = :_enabled)',
                                        ['_idcompany'=>$_idcompany, ':_enabled' => $_enabled, '_code'=>'%'.$_data.'%', '_name'=>'%'.$_data.'%']);
                                                                                                          
            
            if(is_null($data_result) || empty($data_result)){               
               $data_result = [];
            } 
                               
            return $data_result;                                                                         
        }   
        catch(Exception $e){            
            $_error_msg = $e->getMessage();              
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                                                                    
            return [];
        }  
                        
    }

    /**
     * Recibe petición ajax para buscar productos
     */
    public function searchProducts(Request $request){            
        
        if($request->ajax())
        {            
            $find  = request()->get('data');
            $enabled = 1; //Buscar sólo los habilitados
            
            try{

                $error_msg = "";
                
                //$data_result = $this->findalloybydata($_find, $_enabled, $error_msg);
                $data_result = $this->productsByCompanyAndEnabled($this->m_idcompany, $find, $enabled, $error_msg);
                   
                if ($error_msg === ""){     
                                        
                    $data_response = array('_ok'=> 0,'_data'=> $data_result, '_msg'=>'Successfull Operation');                                 
                
                }
                else{                                  
                    $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
                }
            }
            catch(exception $e){
                $error_msg = $e->getMessage();
                
                //$data_response =  array('_ok'=> -1,'_data'=> $error_msg);
                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            } 
            
            return response()->json($data_response, 200);             
        }
        
    }//public function categories_research(Request $request){    

    /**
     * Recibe petición ajax para buscar providers
     */
    public function searchProviders(Request $request){            
        
        if($request->ajax())
        {
            try{

                $error_msg = "";
                                
                $data_result = $this->findprovidersbyEnabled($request->enabled, $error_msg);
                   
                if ($error_msg === ""){     
                                        
                    $data_response = array('_ok'=> 0,'_data'=> $data_result, '_msg'=>'Successfull Operation');                                 
                
                }
                else{                                  
                    $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
                }
            }
            catch(exception $e){
                $error_msg = $e->getMessage();
                
                //$data_response =  array('_ok'=> -1,'_data'=> $error_msg);
                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            } 
            
            return response()->json($data_response, 200);             
        }
        
    }//searchProviders(Request $request)



    function action(Request $request)
    { 
                    
            if($request->ajax())
            {   //dd($request->ptax);
                try{                    
                        $data = [];
                      
                        if(is_null($request->totalcost) || empty(trim($request->totalcost))){
                            $request->totalcost = 0;
                        }

                        if(is_null($request->costofshipping) || empty(trim($request->costofshipping))){
                            $request->costofshipping = 0;
                        }
                         
                        $totalTax = (($request->ptax * $request->totalcost)/100);
                                        
                        if($request->action == 'add'){

                            $obj = new PurchaseOrder();
                                                        
                            $idpo = DB::table('purcharse_orders')->insertGetId([
                            //$obj->refcode         = $request->code;                        
                            'idcompany'       => $this->m_idcompany,
                            'idprovider'      => $request->idprovider,
                            'providername'    => $request->providername,
                            'provideraddress' => $providerAddress = $this->providerAddress($request->idprovider),
                            'totalcost'       => $request->totalcost,
                            'costofshipping'  => $request->costofshipping,
                            'tax'             => $totalTax, //$request->tax, 
                            'ptax'            => $request->ptax,
                            'totalgral'       => $request->totalcost + $request->costofshipping + $request->tax,
                            'idmoneytype'     => $request->idmoneytype,
                            'factormoney'     => $request->factormoney,
                            'canceled'        => 0,
                            'confirmed'       => 0,                       
                            'iduser'          => $this->m_iduser //Usuario logueado
                            ]);
                            
                            $data = array('_ok'=>0, '_data'=>$idpo, '_msg'=>'Successfull Operation');
                        }
                        else                        
                        {                               
                                if($request->action == 'edit'){
                                    
                                    $data = array(                                                       
                                        //'code'		  =>    $request->code,                        
                                                                                                               
                                        'idprovider'      => $request->idprovider,
                                        'providername'    => $request->providername,
                                        'provideraddress' => $providerAddress = $this->providerAddress($request->idprovider),
                                        'totalcost'       => $request->totalcost,
                                        'costofshipping'  => $request->costofshipping,
                                        'tax'             => $totalTax, 
                                        'ptax'            => $request->ptax,
                                        'totalgral'       => $request->totalcost + $request->costofshipping + $totalTax,
                                        //'idmoneytype'     => $request->idmoneytype,
                                        //'factormoney'     => $request->factormoney,
                                        
                                        //'iduser'          => $this->m_iduser //Usuario logueado
                                    );
                                   
                                }       
                                else
                                if($request->action == 'confirmed')
                                {
                                    //dd(($request->ptax * $request->totalcost)/100);
                                    $data = [                                                                                        
                                        'confirmed'       => $request->confirmed,
                                        'costofshipping'  => $request->costofshipping,
                                        'totalcost'       => $request->totalcost,                                        
                                        'tax'             => $totalTax,
                                        'ptax'            => $request->ptax,
                                        'totalgral'       => $request->totalcost + $request->costofshipping + $totalTax,
                                        'iduserconfirmed' =>	$this->m_iduser,
                                        'dateconfirmed'   =>	NOW()

                                        //tax = ((totalcost * :_ptax2) / 100)
                                    ];
                                }
                                else
                                if($request->action == 'canceled')
                                {                                    
                                    $data = [                                                                                        
                                        'canceled'       =>	$request->canceled,
                                        'idusercanceled' =>	$this->m_iduser,
                                        'datecanceled'   =>	NOW()
                                    ];
                                }

                                DB::table('purcharse_orders')
                                ->where('id', $request->id)
                                ->update($data);
                                
                                $data = array('_ok'=>0, '_data'=>'', '_msg'=>'Successfull Operation');
                            
                        }                        
                        
                }
                catch(Exception $e){                     
                        $error_msg =  $e->getMessage();                                 
                        
                        $data = array('_ok'=>-1, '_data'=>'', '_msg'=>$error_msg);
                    
                }
                catch(\Illuminate\database\QueryException $e){                           
                    $error_msg = $e->getMessage();                                 
                    $data = array("ok" => -1, '_data'=>'', '_msg'=>$error_msg);   
                    //return response()->json($data);                 
            }
            
            return response()->json($data);
                
            }
    }//action(Request $request)



    /**
     * Function para agregar, modificar o eliminar productos de un purcharse
     */
    /* Bloque que funciona
    function actiondetails(Request $request)
    { 
                                            
            if($request->ajax())
            {   
                try{                    
                        $data = [];
                        $idpodetail = '';
                        if($request->action == 'add'){

                            $idpodetail = DB::table('detailspurchaseorder')->insertGetId([                                
                                    'idpurcharseorder' => $request->idpurcharseorder,
                                    'idproduct'        => $request->idproduct,
                                    'productcode'      => $request->productcode,
                                    'productname'      => $request->productname,
                                    'unitcost'         => $request->unitcost,
                                    'qtv'              => $request->qtv,
                                    'totalcost'        => $request->totalcost
                            ]);
                        }
                        else
                        if($request->action == 'edit')
                        {   
                         
                            //dd('Id: ' . $request->id . " Qtv: " . $request->qtv . " Total: " . $request->totalcost);
                            $data = array(                                                       
                                //'code'		  =>    $request->code,                        
                                                                                                       
                                //'idpurchaseorder'   => $request->idpurchaseorder,                                                                       
                                //'idproduct'         => $request->idproduct,
                                //'productcode'       => $request->productcode,
                                //'producname'        => $productname,
                                //'unitcost'          => $request->unitcost,
                                'qtv'               => $request->qtv,
                                'unitcost'           => $request->unitcost,     
                                'totalcost'         => $request->totalcost     
                                                         
                            );
                                                                                 
                            DB::table('detailspurchaseorder')
                                ->where('id', $request->id)
                                ->update($data);

                            
                        }                        
                        else
                        if($request->action == 'delete')
                        {
                            DB::table('detailspurchaseorder')
                                ->where('id', $request->id)
                                ->delete();
                        };
                                                                                
                        $data = array('_ok'=>0, '_data'=>$idpodetail, '_msg'=>'Successfull Operation');
                    
                }
                catch(Exception $e){                     
                        $error_msg =  $e->getMessage();                                 
                        
                        $data = array('_ok'=>-1, '_data'=>'', '_msg'=>$error_msg);
                    
                }
                catch(\Illuminate\database\QueryException $e){                           
                    $error_msg = $e->getMessage();                                 
                    $data = array("ok" => -1, '_data'=>'', '_msg'=>$error_msg);   
                    //return response()->json($data);                 
            }
            
            return response()->json($data);
                
            }
    }//action_detail
    Fin Bloque que funciona */
    



    /**
     * Function para agregar, modificar o eliminar productos de un purcharse
     */
    
    function actiondetails(Request $request)
    { //dd($request->tax);

            if(is_null($request->costofshipping) || empty(trim($request->costofshipping))){
                $request->costofshipping = 0;
            }
                       
        
           // $ttalTax = ($request->totalcost * $request->ptax) / 100;
            
            if($request->ajax())
            {   
                try{                    
                        $data = [];
                        $idpodetail = '';
                        if($request->action == 'add'){

                            try{
                                DB::beginTransaction();
                                                           
                                $idpodetail = DB::table('detailspurchaseorder')->insertGetId([                                
                                    'idpurcharseorder' => $request->idpurcharseorder,
                                    'idproduct'        => $request->idproduct,
                                    'productcode'      => $request->productcode,
                                    'productname'      => $request->productname,
                                    'unitcost'         => $request->unitcost,
                                    'qtv'              => $request->qtv,
                                    'totalcost'        => $request->totalcost
                                ]);
                                                                
                                DB::update('UPDATE purcharse_orders SET totalcost = totalcost + :_totalcost, 
                                                   ptax = :_ptax, tax = ((totalcost * :_ptax2) / 100), costofshipping = :_costofshipping,
                                                   totalgral = totalcost + tax + costofshipping
                                            WHERE (id >= :_idpo)',
                                            ['_idpo'=>$request->idpurcharseorder, 
                                             '_totalcost'=>$request->totalcost,
                                             '_costofshipping'=> $request->costofshipping,
                                             '_ptax'=>$request->ptax,
                                             '_ptax2'=>$request->ptax,
                                             //'_totaltax'=>$ttalTax
                                             
                                            ]
                                );
                                
                                //Asegurarse de que se fije la transacción
                                DB::commit();
                                //$data_response = array('_ok'=>0, '_data'=> '', '_msg'=>'Successfull Operation');
                                $data = array('_ok'=>0, '_data'=>$idpodetail, '_msg'=>'Successfull Operation');
                                        
                            }
                            catch(exception $e){
                                $_error_msg = $e->getMessage();
                                
                                DB::rollback();
                                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$_error_msg);
                            } 
                            catch(\Illuminate\database\QueryException $e){
                                $_error_msg = $e->getMessage();
                                
                                DB::rollback();
                                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$_error_msg);
                            }


                        }
                        else
                        if($request->action == 'edit')
                        {   
                                                     
                            try{
                                DB::beginTransaction();
                                
                                DB::update('UPDATE detailspurchaseorder 
                                            SET qtv = :_qtv, 
                                                unitcost = :_unitcost, 
                                                totalcost = qtv * unitcost                                                
                                            WHERE (id = :_id)',
                                            ['_id'=>$request->id,
                                             '_qtv'=>$request->qtv,
                                             '_unitcost' => $request->unitcost
                                            ]
                                );
                                                           
                                DB::update('UPDATE purcharse_orders SET totalcost = totalcost + (:_unitcost * :_qtvunit), 
                                                   ptax = :_ptax, 
                                                   tax = ((totalcost * :_ptax2) / 100), 
                                                   costofshipping = :_costofshipping,
                                                   totalgral = totalcost + tax + costofshipping
                                            WHERE (id = :_idpo)',
                                            ['_idpo'=>$request->idpurcharseorder, 
                                             '_unitcost'=>$request->unitcost,
                                             '_costofshipping'=> $request->costofshipping,
                                             '_ptax'=>$request->ptax,
                                             '_ptax2'=>$request->ptax,
                                             '_qtvunit'=>$request->qtvunit
                                             
                                            ]
                                );
                                
                                //Asegurarse de que se fije la transacción
                                DB::commit();
                                
                                $data = array('_ok'=>0, '_data'=>'', '_msg'=>'Successfull Operation');
                                        
                            }
                            catch(exception $e){
                                $_error_msg = $e->getMessage();
                                
                                DB::rollback();
                                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$_error_msg);
                            } 
                            catch(\Illuminate\database\QueryException $e){
                                $_error_msg = $e->getMessage();
                                
                                DB::rollback();
                                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$_error_msg);
                            }
                            //8888888888888888888888888
                            
                        }                        
                        else
                        if($request->action == 'delete')
                        {
                           //dd("ssssss");
                            try{
                                DB::beginTransaction();
                                                           
                                DB::table('detailspurchaseorder')
                                ->where('id', $request->id)
                                ->delete();
                                                           
                                DB::update('UPDATE purcharse_orders SET totalcost = totalcost + (:_totalcost * :_qtv), 
                                                   ptax = :_ptax, 
                                                   tax = ((totalcost * :_ptax2) / 100), 
                                                   costofshipping = :_costofshipping,
                                                   totalgral = totalcost + tax + costofshipping
                                            WHERE (id = :_idpo)',
                                            ['_idpo'=>$request->idpurcharseorder, 
                                             '_totalcost'=>$request->totalcost,
                                             '_costofshipping'=> $request->costofshipping,
                                             '_ptax'=>$request->ptax,
                                             '_ptax2'=>$request->ptax,
                                             '_qtv'=>$request->qtv
                                             
                                            ]
                                );
                                
                                //Asegurarse de que se fije la transacción
                                DB::commit();
                                
                                $data = array('_ok'=>0, '_data'=>'', '_msg'=>'Successfull Operation');
                                        
                            }
                            catch(exception $e){
                                $_error_msg = $e->getMessage();
                                
                                DB::rollback();
                                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$_error_msg);
                            } 
                            catch(\Illuminate\database\QueryException $e){
                                $_error_msg = $e->getMessage();
                                
                                DB::rollback();
                                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$_error_msg);
                            }
                           
                        };
                                                                                
                        
                    
                }
                catch(Exception $e){                     
                        $error_msg =  $e->getMessage();                                 
                        
                        $data = array('_ok'=>-1, '_data'=>'', '_msg'=>$error_msg);
                    
                }
                catch(\Illuminate\database\QueryException $e){                           
                    $error_msg = $e->getMessage();                                 
                    $data = array("ok" => -1, '_data'=>'', '_msg'=>$error_msg);   
                    //return response()->json($data);                 
            }
            
            return response()->json($data);
                
            }
    }//action_detail


}//Fin class
