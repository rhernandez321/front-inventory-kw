<?php

namespace App\Http\Controllers;

use App\CitiesbyCountries;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//////////////////////////

use View;
use Redirect;
use Exception;

use Response;

class CitiesbyCountriesController extends Controller
{

    private $m_path = 'citiesbycountries';
    private $m_id_user = 1;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        //$_data = "";
        $error_msg = "";
        $countries = [];
        $citiesbycountries = [];
        
        $countries = $this->research_countries($error_msg); 
        
        return view($this->m_path . '.index', compact('countries', 'error_msg')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
    * Función que permite realizar la búsqueda de los citiesbycountries según el id den country, nombre o código.
    * 
    */    
    private function research_citiesbycountries($_id, &$_error_msg)    
    {        
      
        try{
                        
            $citiesbycountries = DB::select('SELECT CC.id, CC.code, CC.name, CC.postalcode, CC.enabled, CC.visible
                                    FROM citiesbycountries AS CC 
                                    INNER JOIN countries AS C ON C.id = CC.id_country
                                    WHERE (C.id = :_id)
                                    ORDER BY CC.name',
                                    ['_id'=>$_id]);
                        
            
            if(is_null($citiesbycountries) || empty($citiesbycountries)){               
               $citiesbycountries = [];
            } 
                        
            return $citiesbycountries;                                                                         
        }   
        catch(Exception $e){            
            $_error_msg = $e->getMessage();            
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                               
            return [];
        }  
                        
    }

    
    /**
    * Función que permite realizar la búsqueda todos los countries.
    * 
    */    
    private function research_countries(&$_error_msg)    
    {               
        try{            
            $countries = DB::select('SELECT C.id, C.countrycode, C.name
                                    FROM countries AS C                                     
                                    WHERE (C.enabled = 1)
                                    ORDER BY C.name');
            
            
            if(is_null($countries) || empty($countries)){               
               $countries = [];
            } 
                        
            return $countries;                                                                         
        }   
        catch(Exception $e){            
            $_error_msg = $e->getMessage();                
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                                          
            return [];
        }  
                        
    }
    


/**Función ajax que retorna los citiesbycountries solicitados según idcountry */
public function citiesbycountries_research(Request $request){    
    
    if($request->ajax())
    {
        
        $_find  = request()->get('idcountry');
        
        
        try{

            $error_msg = "";
            
            $citiesbycountries = $this->research_citiesbycountries($_find, $error_msg);
            
            if ($error_msg === ""){     
                
                return response()->json(array('_ok'=> 0,'_data'=> $citiesbycountries, '_msg'=>''), 200); 
            
            }
            else{                               
                return response()->json(array('_ok'=> -1,'_data'=>[], '_msg'=>$error_msg), 200); 
            }
        }
        catch(exception $e){
            $error_msg = $e->getMessage();               
            return response()->json(array('_ok'=> -1,'_data'=>[], '_msg'=>$error_msg), 200); 
        } 
    }
    
}



/////////////////////////////////////////////////
/**
 *Función que permite ejecutar la acción de eliminar o actualizar los registros de la tabla countries
 */
function action(Request $request)
    {
    	if($request->ajax())
    	{   
            try{

                    if($request->action == 'add'){
                        $cities = new CitiesbyCountries();
                                    
                        $cities->code         = $request->code;
                        $cities->name         = $request->name;
                        $cities->postalcode   = $request->postalcode;
                        $cities->enabled      = $request->enabled;
                        $cities->visible      = $request->visible;
                        $cities->id_country   = $request->idcountry;
                        $cities->id_user      = $this->m_id_user; //Usuario logeado
                        
                        $cities->save();
                    }
                    else
                    if($request->action == 'edit')
                    {   
                        $data = array(
                            'code'	    =>	$request->code,
                            'name'		=>	$request->name,
                            'postalcode'=>	$request->postalcode,
                            'enabled'	=>	$request->enabled,
                            'visible'	=>	$request->visible
                        );
                                                
                        DB::table('citiesbycountries')
                            ->where('id', $request->id)
                            ->update($data);
                        
                    }
                    else
                    if($request->action == 'delete')
                    {
                        DB::table('citiesbycountries')
                            ->where('id', $request->id)
                            ->delete();
                    }
                                                                               
                    $data = array('_ok' => 0, '_data'  => '', '_msg'=>'Successfull Operation');
                  
            }
            catch(Exception $e){                          
                    $error_msg =  $e->getMessage();                     
                  
                    $data = array('_ok' => -1, '_data'  => "Error: fail operation", '_msg'=>$error_msg);
                  
            }
            catch(\Illuminate\database\QueryException $e){                           
                  $error_msg = $e->getMessage();               
                  
                  $data = array('_ok' => -1, '_data'  => "Error: fail operation", '_msg'=>$error_msg);
                  
           }
           
           return response()->json($data);
            
    	}
    }//action(Request $request)


}//Fin class
