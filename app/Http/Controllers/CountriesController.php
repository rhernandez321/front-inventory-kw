<?php

namespace App\Http\Controllers;

////////////////////////
use App\Countries;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//////////////////////////

use View;
use Redirect;
use Exception;

use Response;




class CountriesController extends Controller
{
    private $m_path = 'countries';
    private $m_iduser = 1;

      /**
     * Función que permite realizar la búsqueda de countries según el id, nombre o código.
     * 
     */    
    private function research_data($_data, &$_error_msg)    
    {        
        try{
            
            //$countries = [];            
            
            $countries = DB::select('SELECT C.id, C.countrycode, C.name, C.enabled, C.visible
                                    FROM countries AS C 
                                    WHERE (C.id = :_id) OR (C.countrycode = :_code) OR (C.name LIKE :_name)
                                    ORDER BY C.name',
                                    ['_id'=>$_data, '_name'=>'%'. $_data .'%', '_code'=>$_data]);
                                    
            
            //dd($countries);
            if(is_null($countries) || empty($countries)){               
               $countries = [];
            } 
                        
            return $countries;                                                                         
        }   
        catch(Exception $e){            
            $_error_msg = $e->getMessage(); 
           
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                               
           
            return [];
        }  
                        
    }

    
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        $_data = "";
        $error_msg = "";
        
        return view($this->m_path . '.index', compact('_data', 'error_msg'));        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
   
    }

    /**
     * Store a newly created resource in storage.
     *Autor: Arístides Cortesía
     *Función que permite crear un nuevo país.
     *valida que los valores correspondientes no se encuentren vacíos o sean nulos.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //_ preposición utilizada para indicar que se trata de una variable propia de la función       
       
       $error_msg = "";
       $error_aux = "";
       $_data = "";
       $enabled = 0;
       $visible = 0;
        
       if(empty($request->country_code) or is_null($request->country_code)){
            $error_msg = "Add country code please.";
                                 
       }
       else
       if(empty($request->country_name) or is_null($request->country_name)){
            $error_msg = "Add country name please.";        
       }
       
       if(is_null($request->country_enabled)){
        $enabled = 0;        
       }
       else{
        $enabled = $request->country_enabled;
       }

       if(is_null($request->country_visible)){
         $visible = 0;        
       }
       else{
         $visible = $request->country_visible;
       }
       
       if(empty($error_msg)){
            try{
                                
                //Creamos una variable del tipo de la clase Countries 
                $countries = new Countries();
                                    
                $countries->countrycode  = $request->country_code;
                $countries->name         = $request->country_name;
                $countries->enabled      = $enabled;
                $countries->visible      = $visible;
                $countries->id_user      = $this->m_iduser; //Esto es sólo de prueba. Se debe colocar el parámetro que corresponde 
                
                $countries->save();

                $country_name = "";
                $country_code = "";
                $error_msg    = "";
                
                //return view($this->m_path .'.create',  compact('$error_msg', 'country_name', 'country_code')); 
                
                }
            catch(Exception $e){                          
                $error_msg =  $e->getMessage();           
                
            }
            catch(\Illuminate\database\QueryException $e){                                           
                $error_msg = $e->getMessage(); 
                
            }
       }//if(!empty($error_msg)
       
       
       $countries = $this->research_data('', $error_aux);

       if(!empty($error_aux)){
        $error_msg = $error_aux;
        
       }
       
       return view($this->m_path . '.index', compact('countries', '_data', 'error_msg')); 
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //        
        //return view($this->m_path.'.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Función que realiza la búsqueda del país
     * según el valor del input find_country"
     */
    public function findcountry()
    {              
        $_data  = request()->get('find_country');
                        
        return $this->find_data($_data, '.index', '');        
    }

/**
 *Función que permite ejecutar la acción de eliminar o actualizar los registros de la tabla countries
 */
    function action(Request $request)
    {
        
    	if($request->ajax())
    	{   
            try{
            
                    if($request->action == 'edit')
                    {
                        $data = array(
                            'countrycode'	=>	$request->countrycode,
                            'name'		=>	$request->name,
                            'enabled'		=>	$request->enabled,
                            'visible'		=>	$request->visible
                        );
                        
                        DB::table('countries')
                            ->where('id', $request->id)
                            ->update($data);
                        
                    }
                    else
                    if($request->action == 'delete')
                    {
                        DB::table('countries')
                            ->where('id', $request->id)
                            ->delete();
                    }
                                       
                    $data = array('_ok'=>0, '_data'=> '', '_msg'=>'Successfull Operation');
                    return response()->json($data);                    
            }
            catch(Exception $e){                          
                    $error_msg =  $e->getMessage(); 
                    $data = array('_ok' => -1, '_data'=>'Error: fail operation', '_msg'=>$error_msg);
            }
            catch(\Illuminate\database\QueryException $e){                           
                  $error_msg = $e->getMessage(); 
                  
                  $data = array('_ok' => -1, '_data'=>'Error: fail operation', '_msg'=>$error_msg);                                    
           }        
            
           return response()->json($data);
    	}
    }

/**Función ajax que realiza la búsqueda de los países */
public function countries_research(Request $request){    
    if($request->ajax())
    {
        
        $_find  = request()->get('find_country');
        
        try{

            $error_msg = "";
            $countries = $this->research_data($_find, $error_msg);
            
            if ($error_msg == ""){               
                
                return response()->json(array('_ok'=> 0, '_data'=> $countries, '_msg'=> ''), 200); 
            
            }
            else{     
                
                return response()->json(array('_ok'=> -1,'_data'=> $countries, '_msg'=> $error_msg), 200); 
            }
        }
        catch(exception $e){
            $error_msg = $e->getMessage();             
            return response()->json(array('_ok'=> -1, '_data'=> $countries,'_msg'=>$error_msg), 200); 
        } 
    }
    
}



}//Fin class
