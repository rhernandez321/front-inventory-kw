<?php

namespace App\Http\Controllers;

use App\Moneys;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//////////////////////////

use View;
use Redirect;
use Exception;

use Response;


class MoneysController extends Controller
{
    private $m_path = "moneys";
    private $m_iduser = 1;
    private $m_idcompany = 1;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $error_msg = "";
        $_data = "";
        $data_result = [];
                
       
        return view($this->m_path . '.index', compact('data_result', '_data', 'error_msg'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //Actualizar moneys
    private function updateMoneystype(Request $request){
        $error_msg = "";
        
        try{
             DB::beginTransaction();

             if(1 == $request->default){
                //si $request->default = 1, poner el resto a 0
                DB::UPDATE('UPDATE moneys SET mdefault = 0
                        WHERE (id_company = :_id_company)',
                        ['_id_company' => $request->id_company]);
             }

             DB::UPDATE('UPDATE moneys SET code = :_code, 
                                           name = :_name, 
                                           description = :_description,  
                                           simbolo = :_simbolo, 
                                           factor = :_factor, 
                                           enabled = :_enabled, 
                                           visible = :_visible, 
                                           mdefault = :_default                                             
                         WHERE (id = :_id)',
                         ['_id'=>$request->id, 
                          '_code'=>$request->code,
                          '_name'=>$request->name,
                          '_description'=>$request->description,
                          '_simbolo'=>$request->simbolo,
                          '_factor'=>$request->factor,
                          '_enabled'=>$request->enabled,
                          '_visible'=>$request->visible,
                          '_default'=>$request->default
                         ]
                        );
                                      

             DB::commit();
             $data_response = array('_ok'=>0, '_data'=> '', '_msg'=>'Successfull Operation');
        }
        catch(exception $e){
            $error_msg = $e->getMessage();
                        
            $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            
            DB::rollback(); 
                        
        }
        catch(\Illuminate\database\QueryException $e){
            $error_msg = $e->getMessage();
                        
            $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);

            DB::rollback();            
         }

         return $data_response;
    }//updateMoneystype


    //**Agregar nuevo producto */
    private function addMoneystype(Request $request){
        $error_msg = "";
        
        try{
             DB::beginTransaction();
             
             if(1 == $request->default){
                //si $request->default = 1, poner el resto a 0 
                DB::UPDATE('UPDATE moneys SET mdefault = 0
                        WHERE (id_company = :_id_company)',
                        ['_id_company' => $request->id_company]);
             }

             DB::INSERT('INSERT INTO moneys(code, name, description, simbolo, factor, enabled, visible, mdefault, id_company, id_user)
                          VALUE(:_code, :_name, :_description, :_simbolo, :_factor, :_enabled, :_visible, :_mdefault, :_idcompany, :_iduser)',
                          ['_code'=>$request->code,
                           '_name'=>$request->name,
                           '_description'=>$request->description,
                           '_simbolo'=>$request->simbolo,
                           '_factor'=>$request->factor, 
                           '_enabled'=>$request->enabled, 
                           '_visible'=>$request->visible, 
                           '_mdefault'=>$request->default,                            
                           '_idcompany'=>$request->id_company,
                           '_iduser'=>$request->id_user                           
                          ]
                        );
                        
                        
             //Asegurarse de que se fije la transacción
             DB::commit();
             $data_response = array('_ok'=>0, '_data'=> '', '_msg'=>'Successfull Operation');
             
        }
        catch(exception $e){
            $error_msg = $e->getMessage();
                        
            $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);

            DB::rollback();
        } 
        catch(\Illuminate\database\QueryException $e){
            $error_msg = $e->getMessage();
                        
            $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);

            DB::rollback();            
         }

        return $data_response;             
        
    }//addmoney
   

    /**
    * Función que permite realizar la búsqueda de la data según el valor del parámetro $_data.
    * 
    */    
    private function findmoneysbydata($_data, $_idcompany, &$_error_msg)   
    {           
        try{
            
            $data_result = DB::select('SELECT M.id, M.code, M.name, M.simbolo, M.factor, M.description, M.mdefault, M.enabled, M.visible 
                                 FROM moneys AS M
                                 WHERE (M.id_company = :_idcompany) AND ((M.id = :_id) OR (M.code LIKE :_code) OR (M.name LIKE :_name) OR (M.description LIKE :_descrip) OR (M.simbolo = :_simbol) OR (M.factor = :_factor))                                 
                                 ORDER BY M.name',
                                 ['_id'=>$_data, 
                                  '_code'=>'%'. $_data . '%',
                                  '_name'=>'%'. $_data .'%',
                                  '_descrip'=>'%' . $_data . '%',
                                  '_simbol'=>$_data,
                                  '_factor'=>$_data,
                                  '_idcompany'=>$_idcompany
                                  ]);

            if(is_null($data_result) || empty($data_result)){               
               $data_result = [];
            } 
                               
            return $data_result;                                                                         
        }   
        catch(Exception $e){            
            $_error_msg = $e->getMessage();              
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                                                                    
            return [];
        }  
                        
    }//function allcategories(&$_error_msg)    

    
    /**Función ajax que retorna los citiesbycountries solicitados según idcountry */
    public function moneys_research(Request $request){            
        
        if($request->ajax())
        {            
            $_find  = request()->get('iddata');
            
            
            try{

                $error_msg = "";
                
                //$data_result = $this->findalloybydata($_find, $_enabled, $error_msg);
                $data_result = $this->findmoneysbydata($_find, $this->m_idcompany, $error_msg);
                   
                if ($error_msg === ""){     
                                        
                    $data_response = array('_ok'=> 0,'_data'=> $data_result, '_msg'=>'Successfull Operation');                                 
                
                }
                else{                                  
                    $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
                }
            }
            catch(exception $e){
                $error_msg = $e->getMessage();
                
                //$data_response =  array('_ok'=> -1,'_data'=> $error_msg);
                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            } 
            
            return response()->json($data_response, 200);             
        }
        
    }//public function categories_research(Request $request){    

//Validar data recibida
 function validate_data($request, &$_data){
    
    if(empty($request->code) || is_null($request->code)){                        
        $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert money code.");       
        
        return false;
    }
    /*else 
    if(empty($request->simbolo) || is_null($request->simbolo)){                        
        $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert money code.");
       // dd($_data);
        return false;
    }*/
    else
    if(empty($request->name) || is_null($request->name)){                        
        $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert money name.");       
        
        return false;
    }
    else
    if(empty($request->factor) || is_null($request->factor)){                                
        $request->factor = 0;       
        
        return true;
    }
    else
    if(!(is_numeric($request->factor))){
        $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Enter a valid factor. ");       
        
        return false;
    }
    else
    if(empty($request->enabled) || is_null($request->enabled)){                        
        //$_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert money factor.");
        $request->enabled = 0;       
        
        return true;
    }
    if(empty($request->visible) || is_null($request->visible)){                                
        $request->visible = 0;       
        
        return true;
    }
    else
    if(empty($request->default) || is_null($request->default)){
        $request->moneydefault = 0;
       
        return true;
    }
    else{
        return true;
    }
 }

/////////////////////////////////////////////////
/**
 *Función que permite ejecutar la acción de eliminar o actualizar los registros de la tabla
 */
function action(Request $request)
    {
        $error_msg = "";
        
    	if($request->ajax())
    	{   
            try{    
                    $data = [];
                    if(!($this->validate_data($request, $dataresult))){                                                                                                
                        return response()->json($dataresult);
                    }   
                    else
                    if($request->action == 'add'){
                                               
                        $request->id_user      = $this->m_iduser; //Usuario logueado
                        $request->id_company   = $this->m_idcompany; //Compañía elegida
                        
                        $dataresult = $this->addMoneystype($request);
                        
                    }
                    else
                    if($request->action == 'edit')
                    {   
       
                        $request->id_company   = $this->m_idcompany; //Compañía elegida                        
                        $dataresult = $this->updateMoneystype($request);
                        
                    }
                    else
                    if($request->action == 'delete')
                    {
                        DB::table('moneys')
                            ->where('id', $request->id)
                            ->delete();
                        $dataresult = array('_ok'=>0, '_data'=>'', '_msg'=>"Successfull Operation");    
                    }
                                                                               
                    //$dataresult = array('_ok'=>0, '_data'=>'', '_msg'=>"Successfull Operation");
                  
            }
            catch(Exception $e){                     
                    $error_msg =  $e->getMessage();                                 
                   
                    $dataresult = array('_ok'=>-1, '_data'=>'', '_msg'=>$error_msg);
                  
            }
            catch(\Illuminate\database\QueryException $e){                           
                  $error_msg = $e->getMessage();                                 
                  $dataresult = array("ok" => -1, '_data'=>'', '_msg'=>$error_msg);   
                  //return response()->json($data);                 
           }
           
           return response()->json($dataresult);
            
    	}
    }//action(Request $request)



}//Fin class
