<?php

namespace App\Http\Controllers;

use App\SubCategories;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//////////////////////////

use View;
use Redirect;
use Exception;

use Response;


class SubCategoriesController extends Controller
{
    private $m_path = 'subcategories';
    private $m_iduser = 1;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $error_msg = "";
        $_data = "";
        $categories = [];
        $subcategories = [];
                
        $categories = $this->findcategories($error_msg);
        
        return view($this->m_path . '.index', compact('categories', 'subcategories', '_data', 'error_msg'));         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }


        /**
    * Función que permite realizar la búsqueda de las subcategories según el id de categories.
    * 
    */    
    private function findsubcategories($_id, &$_error_msg)    
    {        
      
        try{
                        
            $subcategories = DB::select('SELECT SubC.id, SubC.code, SubC.name, SubC.description, SubC.enabled, SubC.visible
                                    FROM subcategorys AS SubC 
                                    INNER JOIN categorys AS C ON C.id = SubC.id_category
                                    WHERE (C.id = :_id)
                                    ORDER BY SubC.name',
                                    ['_id'=>$_id]);
            
            
            if(is_null($subcategories) || empty($subcategories)){               
               $subcategories = [];
            } 
                        
            return $subcategories;                                                                         
        }   
        catch(Exception $e){            
            $_error_msg = $e->getMessage();            
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                               
            return [];
        }  
                        
    }

    
    /**
    * Función que permite realizar la búsqueda todos las categories.
    * 
    */    
    private function findcategories(&$_error_msg)    
    {               
        try{
                        
            $categories = DB::select('SELECT C.id, C.code, C.name
                                    FROM categorys AS C                                     
                                    WHERE (C.enabled = 1)
                                    ORDER BY C.name');
            
            
            if(is_null($categories) || empty($categories)){               
               $categories = [];
            } 
                                    
            return $categories;                                                                         
        }   
        catch(Exception $e){            
            $_error_msg = $e->getMessage();                
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                                          
            return [];
        }  
                        
    }


        
    /**Función ajax que retorna los citiesbycountries solicitados según idcountry */
    public function subcategories_research(Request $request){            
        
        if($request->ajax())
        {               
            $_find  = request()->get('idsubcategory');
                        
            try{

                $error_msg = "";
                $categories = $this->findsubcategories($_find, $error_msg);
                
                if ($error_msg === ""){     
                    
                    //return response()->json(array('_ok'=> 0,'_data'=> $categories), 200); 
                    $data_response = array('_ok'=> 0,'_data'=> $categories, '_msg'=>'Successfull Operation');              
                   // return response()->json($data_response, 200);       
                
                }
                else{               
                   
                    $data_response =  array('_ok'=> -1, '_data'=>'Error: Fail operation.', '_msg'=>$error_msg);
                }
            }
            catch(exception $e){
                $error_msg = $e->getMessage();
                                            
                $data_response =  array('_ok'=> -1, '_data'=>'Error: Fail operation.', '_msg'=>$error_msg);
            } 
            
            return response()->json($data_response, 200);             
        }
        
    }//public function categories_research(Request $request){    


    /////////////////////////////////////////////////
    /**
     *Función que permite ejecutar la acción de eliminar o actualizar los registros de la tabla categories
    */
    function action(Request $request)
        {
            
            if($request->ajax())
            {   
                try{

                        if($request->action == 'add'){
                            $subcategories = new SubCategories();
                                        
                            $subcategories->code         = $request->code;
                            $subcategories->name         = $request->name;
                            $subcategories->description  = $request->description;
                            $subcategories->enabled      = $request->enabled;
                            $subcategories->visible      = $request->visible;  
                            $subcategories->id_category  = $request->idcategory;               
                            $subcategories->id_user      = $this->m_iduser; //Usuario logueado
                            
                            $subcategories->save();
                        }
                        else
                        if($request->action == 'edit')
                        {   
                            $data = array(
                                'code'	      =>	$request->code,
                                'name'		  =>	$request->name,
                                'description' =>	$request->description,
                                'enabled'	  =>	$request->enabled,
                                'visible'	  =>	$request->visible
                            );
                                                    
                            DB::table('subcategorys')
                                ->where('id', $request->id)
                                ->update($data);
                            
                        }
                        else
                        if($request->action == 'delete')
                        {
                            DB::table('subcategorys')
                                ->where('id', $request->id)
                                ->delete();
                        }
                                                                                
                        $data = array('_ok' => 0, '_data'=>'', '_msg'=>'Successfull Operation');
                    
                }
                catch(Exception $e){                          
                        $error_msg =  $e->getMessage();                     
                        $data = array("_ok" => -1, '_data'=>'Error: fail operation.', '_msg'=>$error_msg);
                    
                }
                catch(\Illuminate\database\QueryException $e){                           
                    $error_msg = $e->getMessage();               
                    $data = array("_ok" => -1, '_data'=>'Error: fail operation.', '_msg'=>$error_msg); 
                    
            }
            
            return response()->json($data);
                
            }
        }//action(Request $request)


}//Fin class
