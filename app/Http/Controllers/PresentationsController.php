<?php

namespace App\Http\Controllers;

use App\Presentations;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//////////////////////////

use View;
use Redirect;
use Exception;

use Response;


class PresentationsController extends Controller
{
    private $m_path = "presentations";
    private $m_iduser = 1;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $error_msg = "";
        $_data = "";
        $data_result = [];
                       
        return view($this->m_path . '.index', compact('data_result', '_data', 'error_msg'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



/**
    * Función que permite realizar la búsqueda de la data según el valor del parámetro $_data.
    * 
    */    
    private function findpresentationsbydata($_data, &$_error_msg)   
    {           
        try{
            
            /*if((is_null($_enabled)) or (empty($_enabled)))
            {
                $_enabled = 0;
                $_enabledaux = 0;
            }
            else{
                $_enabledaux = -1;
            }*/


           /* $data_result = DB::select('SELECT A.id, A.code, A.name, A.description, A.enabled, A.visible 
                                 FROM alloys AS A
                                 WHERE ((A.id = :_id) OR (A.code = :_code) OR (A.name LIKE :_name) OR (A.description LIKE :_descrip))
                                 AND ((A.enabled = :_enabled) OR (:_enabledaux = 0))      
                                 ORDER BY A.name',
                                 ['_id'=>$_data, '_name'=>'%'. $_data .'%', '_code'=>$_data, '_descrip'=>'%' . $_data . '%', '_enabled'=>$_enabled, ':_enabledaux'=>$_enabledaux]);

             */                                   

            $data_result = DB::select('SELECT PR.id, PR.code, PR.name, PR.description, PR.enabled, PR.visible 
                                 FROM presentations AS PR
                                 WHERE ((PR.id = :_id) OR (PR.name LIKE :_name) OR (PR.description LIKE :_descrip))
                                 ORDER BY PR.name',
                                 ['_id'=>$_data, '_name'=>'%'. $_data .'%', '_descrip'=>'%' . $_data . '%']);

            
            if(is_null($data_result) || empty($data_result)){               
               $data_result = [];
            } 
                               
            return $data_result;                                                                         
        }   
        catch(Exception $e){            
            $_error_msg = $e->getMessage();              
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                                                                    
            return [];
        }  
                        
    }//function allcategories(&$_error_msg)    

    
    /**Funcion que responde a una petición ajax de solicitud de búqueda de data*/
    public function presentations_research(Request $request){            
        
        if($request->ajax())
        {            
            $_find  = request()->get('iddata');
            
            
            try{

                $error_msg = "";
                
                //$data_result = $this->findalloybydata($_find, $_enabled, $error_msg);
                $data_result = $this->findpresentationsbydata($_find, $error_msg);
                   
                if ($error_msg === ""){     
                                        
                    $data_response = array('_ok'=> 0,'_data'=> $data_result, '_msg'=>'Successfull Operation');                                 
                
                }
                else{                                  
                    $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
                }
            }
            catch(exception $e){
                $error_msg = $e->getMessage();
                
                //$data_response =  array('_ok'=> -1,'_data'=> $error_msg);
                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            } 
            
            return response()->json($data_response, 200);             
        }
        
    }//public function categories_research(Request $request){    

//Validar data recibida
 function validate_data($request, &$_data){
        
    if(empty($request->name) || is_null($request->name)){                        
        $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert money name.");       
        return false;
    }    
    else
    if(empty($request->enabled) || is_null($request->enabled)){                        
        //$_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert money factor.");
        $request->enabled = 0;       
        return true;
    }
    if(empty($request->visible) || is_null($request->visible)){                                
        $request->visible = 0;       
        return true;
    }
    else{
        return true;
    }
 }

/////////////////////////////////////////////////
/**
 *Función que permite ejecutar la acción de eliminar o actualizar los registros de la tabla
 */
function action(Request $request)
    {
        
    	if($request->ajax())
    	{   
            try{                    
                    $data = [];
                    if(!($this->validate_data($request, $data))){                                                
                        return response()->json($data);
                    }   
                    else
                    if($request->action == 'add'){

                        $obj = new Presentations();
                        
                        $obj->code         = $request->code;
                        $obj->name         = $request->name;                        
                        $obj->description  = $request->description;                        
                        $obj->enabled      = $request->enabled;
                        $obj->visible      = $request->visible;                       
                        $obj->id_user      = $this->m_iduser; //Usuario logueado
                                       
                        $obj->save();
                    }
                    else
                    if($request->action == 'edit')
                    {   //dd('111: ' . $request->description);
                        $data = array(                                                       
                            'code'		  =>	$request->code,                        
                            'name'		  =>	$request->name,                        
                            'description' =>	$request->description,
                            'enabled'	  =>	$request->enabled,
                            'visible'	  =>	$request->visible
                        );
                                                
                        DB::table('presentations')
                            ->where('id', $request->id)
                            ->update($data);
                        
                    }
                    else
                    if($request->action == 'delete')
                    {
                        DB::table('presentations')
                            ->where('id', $request->id)
                            ->delete();
                    }
                                                                               
                    $data = array('_ok'=>0, '_data'=>'', '_msg'=>"Successfull Operation");
                  
            }
            catch(Exception $e){                     
                    $error_msg =  $e->getMessage();                                 
                   
                    $data = array('_ok'=>-1, '_data'=>'', '_msg'=>$error_msg);
                  
            }
            catch(\Illuminate\database\QueryException $e){                           
                  $error_msg = $e->getMessage();                                 
                  $data = array("ok" => -1, '_data'=>'', '_msg'=>$error_msg);   
                  //return response()->json($data);                 
           }
           
           return response()->json($data);
            
    	}
    }//action(Request $request)



}//Fin class
