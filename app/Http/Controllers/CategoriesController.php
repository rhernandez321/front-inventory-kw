<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//////////////////////////

use View;
use Redirect;
use Exception;

use Response;


class CategoriesController extends Controller
{
    private $m_path = "categories";
    private $m_iduser = 1;
    private $m_idcompany = 1;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $error_msg = "";
        $_data = "";
        $categories = [];
                
       
        return view($this->m_path . '.index', compact('categories', '_data', 'error_msg'));         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    /**
    * Función que permite realizar la búsqueda todos los categories.
    * 
    */    
    private function findcategoriesbydata($_data, $_idcompany, $_error_msg)   
    {   
        
        try{
                                 
            $categories = DB::select('SELECT CG.id, CG.code, CG.name, CG.description, CG.enabled, CG.visible 
                                 FROM categorys AS CG
                                 WHERE (CG.id = :_id) OR ((CG.id_company = :_idcompany) AND ((CG.code = :_code) OR (CG.name LIKE :_name) OR (CG.description LIKE :_descrip)))
                                 ORDER BY CG.name',
                                 ['_idcompany' => $_idcompany, '_id'=>$_data, '_name'=>'%'. $_data .'%', '_code'=>$_data, '_descrip'=>'%'. $_data .'%']);

                                                
            if(is_null($categories) || empty($categories)){               
               $categories = [];
            } 
                               
            return $categories;                                                                         
        }   
        catch(Exception $e){            
            $_error_msg = $e->getMessage();              
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                                                                    
            return [];
        }  
                        
    }//function allcategories(&$_error_msg)    

    
    /**Función ajax que retorna los citiesbycountries solicitados según idcountry */
    public function categories_research(Request $request){            

        if($request->ajax())
        {            
            $_find  = request()->get('idcategory');
            
            try{

                $error_msg = "";
                $categories = $this->findcategoriesbydata($_find, $this->m_idcompany, $error_msg);
                   
                if ($error_msg === ""){     
                                        
                    $data_response = array('_ok'=> 0,'_data'=> $categories, '_msg'=>'Successfull Operation');                                 
                
                }
                else{                                  
                    $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
                }
            }
            catch(exception $e){
                $error_msg = $e->getMessage();
                
                //$data_response =  array('_ok'=> -1,'_data'=> $error_msg);
                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            } 
            
            return response()->json($data_response, 200);             
        }
        
    }//public function categories_research(Request $request){    


/////////////////////////////////////////////////
/**
 *Función que permite ejecutar la acción de eliminar o actualizar los registros de la tabla categories
 */
function action(Request $request)
    {
    	if($request->ajax())
    	{   
            try{

                    if($request->action == 'add'){
                        $categories = new Categories();
                                    
                        $categories->code        = $request->code;
                        $categories->name         = $request->name;
                        $categories->description  = $request->description;
                        $categories->enabled      = $request->enabled;
                        $categories->visible      = $request->visible;                       
                        $categories->id_user      = $this->m_iduser; //Usuario logueado
                        $categories->id_company   = $this->m_idcompany;//Debe ser la company elegida
                                                
                        $categories->save();
                    }
                    else
                    if($request->action == 'edit')
                    {   
                        $data = array(
                            'code'	      =>	$request->code,
                            'name'		  =>	$request->name,
                            'description' =>	$request->description,
                            'enabled'	  =>	$request->enabled,
                            'visible'	  =>	$request->visible
                        );
                                                
                        DB::table('categorys')
                            ->where('id', $request->id)
                            ->update($data);
                        
                    }
                    else
                    if($request->action == 'delete')
                    {
                        DB::table('categorys')
                            ->where('id', $request->id)
                            ->delete();
                    }
                                                                               
                    $data = array('_ok'=>0, '_data'=>'', '_msg'=>"Successfull Operation");
                  
            }
            catch(Exception $e){                          
                    $error_msg =  $e->getMessage();                     
                    $data = array('_ok'=>-1, '_data'=>'', '_msg'=>$error_msg);
                  
            }
            catch(\Illuminate\database\QueryException $e){                           
                  $error_msg = $e->getMessage();               
                  $data = array("_ok" => -1, '_data'=>'', '_msg'=>$error_msg);   
                  //return response()->json($data);                 
           }
           
           return response()->json($data);
            
    	}
    }//action(Request $request)



}//Fin class
