<?php

namespace App\Http\Controllers;

use App\Assemblies;

use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//////////////////////////

use View;
use Redirect;
use Exception;

use Response;


class AssembliesController extends Controller
{

    private $m_path = "assemblies";
    private $m_iduser = 1;
    private $m_idcompany = 1;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $error_msg = "";
        $_data = "";
        $data_result = [];
                
        return view($this->m_path . '.index', compact('data_result', '_data', 'error_msg'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $error_msg   = "";
        $data_result = [];
        $colors      = [];
        $reviews     = [];

        return view($this->m_path . '.create', compact('data_result', 'colors', 'reviews', 'error_msg'));   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
    /**
     * Buscar las country existentes.
     */
    private function findcolors(&$_error_msg)   
    {        
        try{            
           
            $data_result = DB::select('SELECT C.id, C.code, C.name
                                 FROM colors AS C
                                 WHERE (enabled = 1)                                 
                                 ORDER BY C.name'
                                 );

            
            if(is_null($data_result) || empty($data_result)){               
               $data_result = [];
            } 
                               
            return $data_result;                                                                         
        }
        catch(Exception $e){            
            $_error_msg = $e->getMessage();              
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                                                                    
            return [];
        }
    }//Fin findcountries

    /**
    * Función que permite realizar la búsqueda de la data según el valor del parámetro $_data.
    * 
    */    
    private function findAssembliesByCompanyAndData($_idcompany, $_data, &$_error_msg)   
    {          
        try{
            
                                              
            $data_result = DB::select('SELECT ASS.id, ASS.code, ASS.name, ASS.description, ASS.id_color, C.name AS color, ASS.id_review, R.name AS review, ASS.precio, ASS.costo, ASS.enabled, ASS.visible 
                                 FROM assemblies AS ASS
                                 LEFT JOIN colors AS C ON (C.id = ASS.id_color) 
                                 LEFT JOIN reviews AS R ON (R.id = ASS.id_review) 
                                 WHERE (ASS.id_company = :_idcompany) AND 
                                       ((ASS.code LIKE :_code) OR (ASS.name LIKE :_name) OR (ASS.description LIKE :_descrip)
                                        OR (C.name LIKE :_color) OR (R.name LIKE :_review) OR (ASS.precio LIKE :_precio) OR (ASS.costo LIKE :_costo))
                                 ',
                                 ['_idcompany' => $_idcompany, '_code'=>$_data, '_name'=>$_data,
                                  '_descrip'=>$_data, '_color'=>$_data, '_review'=>$_data, 
                                  '_precio'=>$_data, '_costo'=>$_data]
                                 );

            
            if(is_null($data_result) || empty($data_result)){               
               $data_result = [];
            } 
                               
            return $data_result;                                                                         
        }   
        catch(Exception $e){            
            $_error_msg = $e->getMessage();              
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                                                                    
            return [];
        }  
                        
    }//function allcategories(&$_error_msg)    

    
    /**Funcion que responde a una petición ajax de solicitud de búqueda de data*/
    function research(Request $request){            
        
        if($request->ajax())
        {            
            //$datafind    = request()->get('data');
            
            //$idcountry = request()->get('idcountry');
                        
            try{

                $error_msg = "";
                                
                $data_result = $this->findAssembliesByCompanyAndData($this->m_idcompany, $request->data, $error_msg);
                   
                if ($error_msg === ""){     
                                        
                    $data_response = array('_ok'=> 0,'_data'=> $data_result, '_msg'=>'Successfull Operation');                                 
                
                }
                else{                                  
                    $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
                }
            }
            catch(exception $e){
                $error_msg = $e->getMessage();
                
                //$data_response =  array('_ok'=> -1,'_data'=> $error_msg);
                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            } 
            
            return response()->json($data_response, 200);             
        }
        
    }//public function categories_research(Request $request){    

    //Validar data recibida
    function validate_data($request, &$_data){
        if(empty($request->id_color) || is_null($request->id_color)){                        
            $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert valid color.");       
            return false;
        }
        else       
        if(empty($request->id_review) || is_null($request->id_review)){                        
            $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert valid review.");       
            return false;
        }
        else
        if(empty($request->code) || is_null($request->code)){                        
            $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert code.");       
            return false;
        }
        else
        if(empty($request->name) || is_null($request->name)){                        
            $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert name.");       
            return false;
        }
        else
        if(empty($request->address) || is_null($request->address)){                        
            $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert address.");       
            return false;
        }    
        else
        if(empty($request->phone1) || is_null($request->phone1)){                        
            $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert phone 1.");       
            return false;
        }        
        else
        if(empty($request->enabled) || is_null($request->enabled)){                        
            //$_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert money factor.");
            $request->enabled = 0;       
            return true;
        }
        else
        if(empty($request->visible) || is_null($request->visible)){                                
            $request->visible = 0;       
            return true;
        }
        else
        if(empty($request->precio) || is_null($request->precio)){                                    
            $request->enabled = 0;       
            return true;
        }
        if(empty($request->costo) || is_null($request->costo)){                        
            //$_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert money factor.");
            $request->costo = 0;       
            return true;
        }
        else{
            return true;
        }
    }

    /////////////////////////////////////////////////
    /**
     *Función que permite ejecutar la acción de eliminar o actualizar los registros de la tabla
    */
    function action(Request $request)
    { 
            
            if($request->ajax())
            {   
                try{                    
                        $data = [];
                        if((($request->action == 'add') || ($request->action == 'edit')) && (!($this->validate_data($request, $data)))){                                                
                            return response()->json($data);
                        }   
                        else
                        if($request->action == 'add'){

                            $obj = new Providers();
                            
                            $obj->code         = $request->code;                        
                            $obj->name         = $request->name;                                                    
                            $obj->description  = $request->description;
                            $obj->precio       = $request->precio;
                            $obj->costo        = $request->costo;
                            $obj->id_color     = $request->idcolor;
                            $obj->id_review    = $request->idreview;                            
                            $obj->enabled      = $request->enabled;
                            $obj->visible      = $request->visible;                       
                            $obj->id_user      = $this->m_iduser; //Usuario logueado
                                        
                            $obj->save();
                        }
                        else
                        if($request->action == 'edit')
                        {   
                            $data = array(                                                                                      
                                'code'		  =>	$request->code,                        
                                'name'		  =>	$request->name,                        
                                'description' =>	$request->description,
                                'precio'      =>	$request->precio,
                                'costo'       =>	$request->costo,
                                'id_color'    =>	$request->idcolor,
                                'id_review'   =>	$request->idreview,
                                'enabled'	  =>	$request->enabled,
                                'visible'	  =>	$request->visible
                            );
                                                    
                            DB::table('assamblies')
                                ->where('id', $request->id)
                                ->update($data);
                            
                        }
                        else
                        if($request->action == 'delete')
                        {
                            DB::table('assamblies')
                                ->where('id', $request->id)
                                ->delete();
                        }
                                                                                
                        $data = array('_ok'=>0, '_data'=>'', '_msg'=>"Successfull Operation");
                    
                }
                catch(Exception $e){                     
                        $error_msg =  $e->getMessage();                                 
                    
                        $data = array('_ok'=>-1, '_data'=>'', '_msg'=>$error_msg);
                    
                }
                catch(\Illuminate\database\QueryException $e){                           
                    $error_msg = $e->getMessage();                                 
                    $data = array("ok" => -1, '_data'=>'', '_msg'=>$error_msg);   
                    //return response()->json($data);                 
            }
            
            return response()->json($data);
                
            }
    }//action(Request $request)


    /**
     * Función que realiza la llamada a la consulta de los reviews
     */
    function reviews_research(Request $request){
        ///////////////////
        if($request->ajax())
        {                        
                                    
            try{

                $error_msg = "";
                                
                $data_result = $this->findreviews($error_msg);
                   
                if ($error_msg === ""){     
                                        
                    $data_response = array('_ok'=> 0,'_data'=> $data_result, '_msg'=>'Successfull Operation');                                 
                
                }
                else{                                  
                    $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
                }
            }
            catch(exception $e){
                $error_msg = $e->getMessage();
                
                //$data_response =  array('_ok'=> -1,'_data'=> $error_msg);
                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            } 
            
            return response()->json($data_response, 200);             
        }                
        ////////////////////

    }//citiesbycountries





}//Fin class
