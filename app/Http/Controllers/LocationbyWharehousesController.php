<?php

namespace App\Http\Controllers;

use App\LocationbyWharehouses;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//////////////////////////

use View;
use Redirect;
use Exception;

use Response;

class LocationbyWharehousesController extends Controller
{

    private $m_path = "locationbywharehouses";
    private $m_iduser = 1;
    private $m_idcompany = 1;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $error_msg = "";
        $_data = "";
        $data_result = [];
        
        $data_result = $this->findwharehousebycompany($this->m_idcompany, $_error_msg);   
        
        return view($this->m_path . '.index', compact('data_result', '_data', 'error_msg'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *Función que realiza la consulta de las wharehouse by company
     */
    private function findwharehousebycompany($_idcompany, &$_error_msg)   
    {
        try{
            $data_result = DB::select('SELECT WH.id, WH.code, WH.name 
                                 FROM wharehouses AS WH
                                 WHERE ((id_company = :_idcompany) AND (WH.enabled = 1))
                                 ORDER BY WH.name',
                                 ['_idcompany'=>$_idcompany]);

            
            if(is_null($data_result) || empty($data_result)){               
               $data_result = [];
            } 
                               
            return $data_result;                                                                         
        }
        catch(Exception $e){            
            $_error_msg = $e->getMessage();              
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                                                                    
            return [];
        }
    }//Fin findcities

    /**
     * Buscar las company existentes.
     */
    private function findcompany(&$_error_msg)   
    {        
        try{            
            //dd('$data_result');            
            $data_result = DB::select('SELECT C.id, C.code, C.name  
                                       FROM companies AS C 
                                       WHERE (C.enabled = 1) ORDER BY C.name');

        
            if(is_null($data_result) || empty($data_result)){               
               $data_result = [];
            } 
                               
            return $data_result;                                                                         
        }
        catch(Exception $e){            
            $_error_msg = $e->getMessage();              
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                                                                    
            return [];
        }
    }//Fin findcountries

    /**
    * Función que permite realizar la búsqueda de la data según el valor del parámetro $_data.
    * 
    */    
    private function findlocationbywharehousebycompany($_idcompany, $_idwharehouse, &$_error_msg)   
    {          
        try{
            
                                     
            $data_result = DB::select('SELECT L.id, L.code, L.name, L.description, L.enabled, L.visible 
                                 FROM locationbywharehouses AS L
                                 WHERE ((L.id_company = :_idcompany) AND (L.id_wharehouse = :_idwharehouse))
                                 ORDER BY L.name',
                                 ['_idcompany'=>$_idcompany, '_idwharehouse'=>$_idwharehouse]);

            
            if(is_null($data_result) || empty($data_result)){               
               $data_result = [];
            } 
                               
            return $data_result;                                                                         
        }   
        catch(Exception $e){            
            $_error_msg = $e->getMessage();              
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                                                                    
            return [];
        }  
                        
    }//function allcategories(&$_error_msg)    

    
    /**Funcion que responde a una petición ajax de solicitud de búqueda de data*/
    function research(Request $request){            
        
        if($request->ajax())
        {            
            $idwharehouse    = request()->get('idwharehouse');
            //$idcompany = request()->get('idcompany');
            $idcompany = $this->m_idcompany;
                                                                        
            try{

                $error_msg = "";
                                
                $data_result = $this->findlocationbywharehousebycompany($idcompany, $idwharehouse, $error_msg);
                   
                if ($error_msg === ""){     
                                        
                    $data_response = array('_ok'=> 0,'_data'=> $data_result, '_msg'=>'Successfull Operation');                                 
                
                }
                else{                                  
                    $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
                }
            }
            catch(exception $e){
                $error_msg = $e->getMessage();
                
                //$data_response =  array('_ok'=> -1,'_data'=> $error_msg);
                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            } 
            
            return response()->json($data_response, 200);             
        }
        
    }//public function categories_research(Request $request){    

    //Validar data recibida
    function validate_data($request, &$_data){
        
        if(empty($request->code) || is_null($request->code)){                        
            $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert code.");       
            return false;
        }
        else
        if(empty($request->name) || is_null($request->name)){                        
            $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert name.");       
            return false;
        }        
        else
        /*if(empty($request->description) || is_null($request->description)){                        
            $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert description.");       
            return false;
        }                    
        else*/
        if(empty($request->enabled) || is_null($request->enabled)){                        
            //$_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert money factor.");
            $request->enabled = 0;       
            return true;
        }
        if(empty($request->visible) || is_null($request->visible)){                                
            $request->visible = 0;       
            return true;
        }
        else{
            return true;
        }
    }

    /////////////////////////////////////////////////
    /**
     *Función que permite ejecutar la acción de eliminar o actualizar los registros de la tabla
    */
    function action(Request $request)
    {             
            if($request->ajax())
            {   
                try{                    
                        $data = [];
                        if((($request->action == 'add') || ($request->action == 'edit')) && (!($this->validate_data($request, $data)))){                                                
                            return response()->json($data);
                        }   
                        else
                        if($request->action == 'add'){

                            $obj = new LocationbyWharehouses();
                            
                            $obj->code          = $request->code;                                                    
                            $obj->name          = $request->name;   
                            $obj->description   = $request->description;                                                    
                            //$obj->id_company    = $request->idcompany;
                            $obj->id_company    = $this->m_idcompany;                    
                            $obj->id_wharehouse = $request->idwharehouse;
                            $obj->enabled       = $request->enabled;
                            $obj->visible       = $request->visible;                       
                            $obj->id_user       = $this->m_iduser; //Usuario logueado
                                        
                            $obj->save();
                        }
                        else
                        if($request->action == 'edit')
                        {   
                            $data = array(                                                       
                                'code'		  =>	$request->code,                        
                                'name'  	  =>	$request->name,                                                             
                                'description' =>	$request->description,                                
                                'enabled'	  =>	$request->enabled,
                                'visible'	  =>	$request->visible
                            );
                                                    
                            DB::table('locationbywharehouses')
                                ->where('id', $request->id)
                                ->update($data);
                            
                        }
                        else
                        if($request->action == 'delete')
                        {
                            DB::table('locationbywharehouses')
                                ->where('id', $request->id)
                                ->delete();
                        }
                                                                                
                        $data = array('_ok'=>0, '_data'=>'', '_msg'=>"Successfull Operation");
                    
                }
                catch(Exception $e){                     
                        $error_msg =  $e->getMessage();                                 
                    
                        $data = array('_ok'=>-1, '_data'=>'', '_msg'=>$error_msg);
                    
                }
                catch(\Illuminate\database\QueryException $e){                           
                    $error_msg = $e->getMessage();                                 
                    $data = array("ok" => -1, '_data'=>'', '_msg'=>$error_msg);   
                    //return response()->json($data);                 
            }
            
            return response()->json($data);
                
            }
    }//action(Request $request)


    /**
     * Función que realiza la llamada a la consulta de los wharehouse by company
     */
    function wharehousebycompany(Request $request){
        ///////////////////
        if($request->ajax())
        {                        
            //$idcompany = $request->idcompany;
                        
            try{

                $error_msg = "";
                                
                $data_result = $this->findwharehousebycompany($request->idcompany, $error_msg);
                   
                if ($error_msg === ""){     
                                        
                    $data_response = array('_ok'=> 0,'_data'=> $data_result, '_msg'=>'Successfull Operation');                                 
                
                }
                else{                                  
                    $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
                }
            }
            catch(exception $e){
                $error_msg = $e->getMessage();
                
                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            } 
            
            return response()->json($data_response, 200);             
        }                
        ////////////////////

    }//citiesbycountries



}//Fin class

