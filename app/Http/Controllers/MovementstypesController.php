<?php

namespace App\Http\Controllers;

use App\Movementstypes;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//////////////////////////

use View;
use Redirect;
use Exception;

use Response;


class MovementstypesController extends Controller
{
    private $m_path = "movementstypes";
    private $m_iduser = 1;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $error_msg = "";
        $_data = "";
        $data_result = [];
        
        //$data_result = $this->findmovementstypes($error_msg);   
                     
        return view($this->m_path . '.index', compact('data_result', '_data', 'error_msg'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //Verificar la existencia de los movimientos básicos
    //Entrada/Salida
    function movementstypes_exist(){
        $error_msg = "";

        //$movemenstT = findmovementstypes('', $error_msg);
        if (DB::table('movementstypes')->where('code', 'IN')->doesntExist()) {
            $data_response = array('_ok'=> 0,'_data'=> 'IN', '_msg'=>'Successfull Operation');
        }
        else
        if (DB::table('movementstypes')->where('code', 'OUT')->doesntExist()) {
            $data_response = array('_ok'=> 0,'_data'=> 'OUT', '_msg'=>'Successfull Operation');
        }
        else
            $data_response = array('_ok'=> 0,'_data'=> 'OTHER', '_msg'=>'Successfull Operation');

        return response()->json($data_response, 200);             
      
    }//verifyExistMovementsTypes

    
    /**
     *Función que realiza la consulta de los movementstypes.
     */
    private function findmovementstypes($_data, &$_error_msg)   
    {
        try{
          
            $data_result = DB::select('SELECT MT.id, MT.code, MT.name, MT.description, MT.enabled, MT.visible
                                 FROM movementstypes AS MT                                 
                                 WHERE (MT.id = :_id) OR (MT.name LIKE :_name) OR (MT.description LIKE :_descrip)
                                 ORDER BY MT.name',
                                 ['_id'=>$_data, '_name'=>'%'. $_data .'%', '_descrip'=>'%'. $_data .'%']);  

            if(is_null($data_result) || empty($data_result)){               
               $data_result = [];
            } 
                            
            return $data_result;                                                                         
        }
        catch(Exception $e){            
            $_error_msg = $e->getMessage();              
            return [];
        }
        catch(\Illuminate\database\QueryException $e){
            $_error_msg = $e->getMessage();                                                                    
            return [];
        }
    }//Fin findcities

    
    /**Funcion que responde a una petición ajax de solicitud de búqueda de data*/
    function research(Request $request){            
        
        if($request->ajax())
        {            
            $data    = request()->get('datafind');

                                                
            try{

                $error_msg = "";
                                
                $data_result = $this->findmovementstypes($data, $error_msg);               
                //dd($movemenstT); 
                //$movemenstT =  array(['id'=> 0,'name'=> 'Out'], ['id'=> 1,'mt'=> 'Int']);
                //////////////////
              
               // dd($movemenstT[1][1]->name);
                ///////////////////////
                   
                if ($error_msg === ""){     
                                        
                    $data_response = array('_ok'=> 0,'_data'=> $data_result, '_msg'=>'Successfull Operation');                                 
                
                }
                else{                                  
                    $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
                }
            }
            catch(exception $e){
                $error_msg = $e->getMessage();
                
                //$data_response =  array('_ok'=> -1,'_data'=> $error_msg);
                $data_response = array('_ok'=> -1,'_data'=> 'Error: fail operation', '_msg'=>$error_msg);
            } 
            
            return response()->json($data_response, 200);             
        }
        
    }//public function categories_research(Request $request){    

    //Validar data recibida
    function validate_data($request, &$_data){
               
        if(empty($request->code) || is_null($request->code)){                        
            $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert code.");       
            return false;
        }                           
        else
        if(empty($request->name) || is_null($request->name)){                        
            $_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert name.");       
            return false;
        }                           
        else
        if(empty($request->enabled) || is_null($request->enabled)){                        
            //$_data = array('_ok'=>-1, '_data'=>'', '_msg'=>"Insert money factor.");
            $request->enabled = 0;       
            return true;
        }
        if(empty($request->visible) || is_null($request->visible)){                                
            $request->visible = 0;       
            return true;
        }
        else{
            return true;
        }
    }

    /////////////////////////////////////////////////
    /**
     *Función que permite ejecutar la acción de eliminar o actualizar los registros de la tabla
    */
    function action(Request $request)
    {             
            if($request->ajax())
            {   
                try{                    
                        $data = [];
                        if((($request->action == 'add') || ($request->action == 'edit')) && (!($this->validate_data($request, $data)))){                                                
                            return response()->json($data);
                        }   
                        else
                        if($request->action == 'add'){

                            $obj = new Movementstypes();
                            
                            $obj->code         = $request->code;                        
                            $obj->name         = $request->name;                        
                            $obj->description  = $request->description;                                                    
                            $obj->enabled      = $request->enabled;
                            $obj->visible      = $request->visible;                       
                            $obj->id_user      = $this->m_iduser; //Usuario logueado
                                        
                            $obj->save();
                        }
                        else
                        if($request->action == 'edit')
                        {   
                            $data = array(                                                                                       
                                'code'		  =>	$request->code,                        
                                'name'		  =>	$request->name,                        
                                'description' =>	$request->description,                                
                                'enabled'	  =>	$request->enabled,
                                'visible'	  =>	$request->visible
                            );
                                                    
                            DB::table('movementstypes')
                                ->where('id', $request->id)
                                ->update($data);
                            
                        }
                        else
                        if($request->action == 'delete')
                        {
                            DB::table('movementstypes')
                                ->where('id', $request->id)
                                ->delete();
                        }
                                                                                
                        $data = array('_ok'=>0, '_data'=>'', '_msg'=>"Successfull Operation");
                    
                }
                catch(Exception $e){                     
                        $error_msg =  $e->getMessage();                                 
                    
                        $data = array('_ok'=>-1, '_data'=>'', '_msg'=>$error_msg);
                    
                }
                catch(\Illuminate\database\QueryException $e){                           
                    $error_msg = $e->getMessage();                                 
                    $data = array("ok" => -1, '_data'=>'', '_msg'=>$error_msg);   
                    //return response()->json($data);                 
            }
            
            return response()->json($data);
                
            }
    }//action(Request $request)



}//Fin class
