<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    //    
    protected $table='countries';
    
    protected $primaryKey = 'id';
    protected $fillable =  array('countrycode', 'name', 'id_user', 'enabled', 'visible');

    //protected $hidden = ['created_at'];
    public $timestamps = false;


            
}
