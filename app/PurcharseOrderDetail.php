<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PurcharseOrderDetail extends Model
{
    //  
    protected $table='detailspurchaseorder';
    protected $primaryKey = 'id';
    
    protected $fillable =  [        
        'idproduct',
        'idpurcharseorder',
        'productcode',
        'producname',
        'unitcost',
        'qtv',
        'totalcost'
    ];

    public $timestamps = false;

}
