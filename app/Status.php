<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    //
    protected $table='status';

    protected $primaryKey = 'id';
    protected $fillable =  array('name', 'description', 'enabled', 'visible', 'id_user');

    public $timestamps = false;


    

}
