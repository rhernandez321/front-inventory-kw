<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategories extends Model
{
    //
    protected $table='subcategorys';

    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'name', 'description', 'enabled', 'visible', 'id_user', 'id_category');

    public $timestamps = false;


    //Obtener las subcategories by categories and status
    public function scopeGetSubCategoriesByCategoryAndStatus($query, $idcategory, $status){
        $categories = [];

        if( 'all' == $status){                                  
                $categories = Categories::all();                       
        }
        else{
                //$categories = $query->where('enabled', $status)->get();           
                $categories = $query->where('id_category', $idcategory)->where('enabled', $status)->get();           
        }

        return $categories;
    
        
    }


}//Fin class


