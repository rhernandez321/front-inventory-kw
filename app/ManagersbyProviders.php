<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManagersbyProviders extends Model
{
    //
    protected $table='managersbyproviders';

    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'name', 'note', 'address', 'id_provider',
                                 'phone1', 'phone2', 'phone3', 'enabled', 'visible', 'id_user');

    public $timestamps = false;
}
