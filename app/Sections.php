<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sections extends Model
{
    //

    protected $table='sections';

    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'name', 'description', 'id_location',
                                 'id_wharehouse', 'enabled', 'visible', 'id_user');

    public $timestamps = false;
}
