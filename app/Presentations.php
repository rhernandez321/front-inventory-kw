<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presentations extends Model
{
    //
    protected $table='presentations';

    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'name', 'description', 'enabled', 'visible', 'id_user');
    public $timestamps = false;


}
