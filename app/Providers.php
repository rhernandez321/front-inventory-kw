<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Providers extends Model
{
    //
    protected $table='providers';

    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'name', 'note', 'address', 'id_country',
                                 'id_city', 'phone1', 'phone2', 'phone3', 'enabled', 'visible', 'id_user');

    public $timestamps = false;



}
