<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movementstypes extends Model
{
    //
    protected $table='movementstypes';

    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'name', 'descriptions', 'movementstype', 'enabled', 'visible', 'id_user');

    public $timestamps = false;
}
