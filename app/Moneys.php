<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moneys extends Model
{
    //
    protected $table='moneys';

    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'name', 'description', 'simbolo', 'factor', 'enabled', 'visible', 'default', 'id_user');

    public $timestamps = false;


   
}
