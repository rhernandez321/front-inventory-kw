<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitTypes extends Model
{
    //
    protected $table='unittypes';

    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'name', 'description', 'factor', 'enabled', 'visible', 'id_user');

    public $timestamps = false;

    
}
