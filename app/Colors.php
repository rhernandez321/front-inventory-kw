<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colors extends Model
{
    //
    protected $table='colors';

    protected $primaryKey = 'id';
    protected $fillable =  array('code', 'name', 'description', 'enabled', 'visible', 'id_user');

    public $timestamps = false;


    
}
