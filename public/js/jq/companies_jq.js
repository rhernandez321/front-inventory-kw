$(function(){
      
    
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });


     /** Realizar la primera recarga de data */
    researchData('', '');


    /**Elegir el country y buscar cities*/
    $('#select-countries').on({change: 
         function (event)
         {                                
             citiesbycountry();
             $('#select-cities').change(); 
         }
    });//Fin $('#select_idioma').on({change: 

    /**Elegir city y buscar companies */
    $('#select-cities').on({change: 
         function (event)
         {                   
             researchData($('#select-countries').val(), $(this).val());                          
         }
    });//Fin $('#select_idioma').on({change: 
        
    /**Se ejecuta cuando se recibe una respuesta sactifactoria del ajax que actualiza */    
    function successUpdate(datos) {          
       if(datos['_ok'] == '0'){
            closeModal('.btn-close'); //Indicamos la clase de cerrar modal
            refreshDataTable()
        }
        showModalMessage(datos['_msg']); //(funciones_js.js)
                
    }

   
    /**
    Función que realiza la carga el datatable de la data */
    function researchData($idcountry, $idcity){
                
        //Botones update y delete
        var btn_html = "<button type='button' class='edit btn btn-outline-warning' data-toggle='modaledit' data-target='#modaledit'><span class='fa fa-wrench' aria-hidden='true'></span></button>"
        btn_html = btn_html + "&nbsp;&nbsp;<button type='button' class='delete btn btn-outline-danger' data-toggle='modaledit' data-target='#modaldelete'><span class='fa fa-trash' aria-hidden='true'></span></button>"
                
        var datatable = $('#tablecompanies').DataTable({
         
           // processing: true,
           // serverSide: true,
            //serverMethod: 'post',
            responsive: true,
            destroy: true,
            
            ajax: {url: companiesResearch(),         
                    type: "post",
                    //dataSrc: "_data",
                    data: {"idcountry":$idcountry,
                           "idcity":$idcity,
                          },                    
                    dataSrc: function (data) {
                        console.log(data);
                        if (data['_ok'] == '0')
                        {                            
                            console.log(data["_data"]);
                            return data['_data'];
                        }else{
                            console.log(data["_msg"]);
                            showModalMessage(data['_msg']);                            
                            data['_data'] = [];
                            return data['_data']
                        }                           
                    } 
            },
            
            columns: [
                        {data:"id"},
                        {data:"code"},
                        {data:"name"},                        
                        {data:"description"},                        
                        {data:"address"},                        
                        {
                            bSortable: false,
                            data:"enabled",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }
                        },
                        {   bSortable: false,
                            data:"visible",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }
                        },
                        {  
                            bSortable: false,
                            data: null, 
                            "defaultContent": btn_html                           
                        }
                        
                        
                       /*Concatena campos 
                       { 
                            "data": null,
                            "className": "button",
                            "defaultContent": '<button name="vcvcvc" id="' + {data: "name"} + '" value="id" OnClick="EditRow(this);"  class="btn btn-primary" data-toggle="modal" data-target="#myModal">Editar</button>'
                        }*/                                            
                        
            ],     
            columnDefs: [
                          { "visible": false, "targets": [0] },
                          
                        ],         
            select:true
        });

      //  editar("#tablecountries",datatable);
    }//Fin researchcountries
         

    /**
    Capturar la acción clip de los botones de la tabla countries
     */
    $('#tablecompanies').on('click', "[data-toggle='modaledit']", function(){
           
           table = $('#tablecompanies').DataTable();  
           var row = $(this).parents('tr')[0]; 
           var data = table.row( row ).data();
                     
           console.log( data);           
           
           $('#input-modal-edit-data-idcompany').val(data.id);        

           $('#input-modal-edit-data-city').val($('#select-cities option:selected').text());   
           $('#input-modal-edit-data-country').val($('#select-countries option:selected').text());                     
           
           if($(this).hasClass("edit")){                                       
                $('#input-modal-edit-data-code').val(data.code);   
                $('#input-modal-edit-data-code').prop('disabled', false);                                             
                $('#input-modal-edit-data-name').val(data.name);
                $('#input-modal-edit-data-name').prop('disabled', false);            
                $('#input-modal-edit-data-description').val(data.description);
                $('#input-modal-edit-data-description').prop('disabled', false);
                $('#input-modal-edit-data-address').val(data.address);
                $('#input-modal-edit-data-address').prop('disabled', false);
                $('#modal-edit-enabled-select').val(data.enabled);
                $('#modal-edit-visible-select').val(data.visible);

                $('#div-modal-edit-select-visible').show();
                $('#div-modal-edit-select-enabled').show();
                $('#btt-modal-edit-save').show();                            
                $('#btt-modal-delete').hide();
                                
                //$('#modal-edit-data').modal({backdrop: 'static', keyboard: false})
            
                $("#modal-edit-data").modal("show");                                 
           }            
           else{                                
               // $('#input-modal-edit-data-idalloy').val(data.id);               
                $('#input-modal-edit-data-code').prop('disabled', true);
                $('#input-modal-edit-data-code').val(data.code);                
                $('#input-modal-edit-data-name').prop('disabled', true);
                $('#input-modal-edit-data-name').val(data.name);                        
                $('#input-modal-edit-data-description').prop('disabled', true);
                $('#input-modal-edit-data-description').val(data.description);         
                $('#input-modal-edit-data-address').prop('disabled', true);
                $('#input-modal-edit-data-address').val(data.address);         
                $('#div-modal-edit-select-visible').hide();
                $('#div-modal-edit-select-enabled').hide();
                $('#btt-modal-edit-save').hide();                            
                $('#btt-modal-delete').show();                            
                
                $("#modal-edit-data").modal("show");                   
                        
           }
                     
   });
  
   
    /**
    Mostrar modal add
     */
    $('#btn-add').on({click:                
        function (event)
        {            
            event.preventDefault();  
                       
            $('#input-modal-add-code').val('');            
            $('#input-modal-add-name').val('');            
            $('#input-modal-add-description').val('');
            $('#input-modal-add-address').val('');

            $('#input-modal-add-country').val($('#select-countries option:selected').text());
            $('#input-modal-add-city').val($('#select-cities option:selected').text());            
            
            if(!($('#select-countries').val() == -1) && !($('#select-cities').val() == -1)){                        
                $("#modal-add").modal('show');
            }
            else{
                showModalMessage('Choose a country and city .'); //(funciones_js.js)                
            }
                                                                                                                                       
        }//function (event)
    }); // 
    
    /**
    Función que ejecuta la acción de refresch del datatable
     */
    function refreshDataTable()
    {                        
        researchData($('#select-countries').val(), $('#select-cities').val());                                        
    }     

    
    /**Ejecutar la acción de agregar */
    $('#btt-modal-add').on({click:
        function (event)
        {     
            event.preventDefault(); 
                    
            if($('#input-modal-add-code').val() === ''){               
                $("#error-msg").text("Insert code.");                 
                $("#modal-msg").modal("show");                                     
            }                
            else  
            if($('#input-modal-add-name').val() === ''){
                $("#error-msg").text("Insert name.");
                $("#modal-msg").modal("show");
            }
            else  
            if($('#input-modal-add-address').val() === ''){
                $("#error-msg").text("Insert address.");
                $("#modal-msg").modal("show");
            }            
            else{
                //$("form").submit();  
                //$("#form-modal-add-countrie").submit();  
            
                $('#div-confirm-data-text').html('Add company?');
                $('#btt-modal-confirm').addClass('add');            
                $("#modal-confirm-data").modal('show');
            
            }
             
        }
    }); //Fin $('#id-input-send-add-country').on({click:
     
    $('#btt-modal-confirm').on({click:    
            function (event)
            {   
                
                event.preventDefault();  
                                           
                $("#modal-confirm-data").modal('hide');
    
                if($(this).hasClass("edit")){                     
                    $('#btt-modal-confirm').removeClass('edit');
                    updateData('edit');
                }
                else
                if($(this).hasClass("delete")){                   
                    $('#btt-modal-confirm').removeClass('delete'); 
                    updateData('delete');
                }
                else
                if($(this).hasClass("add")){
                    $('#btt-modal-confirm').removeClass('add'); 
                    //$("#form-modal-add-countrie").submit(); 
                    addData();
                }
                            
            }
     });//$('#b
            

    /**
     * Function que ejecuta el ajax de actualización de la data (update, delete)
     *  */ 
    function updateData(action){
                    

            var data = new Object();
                        
            var Obj_Params = new Object();
                      
            //var idbtn = $(this).prop('id');
            
            data.action      = action;
           
            data.id          = $('#input-modal-edit-data-idcompany').val();    
            data.code        = $('#input-modal-edit-data-code').val();                            
            data.name        = $('#input-modal-edit-data-name').val();            
            data.description = $('#input-modal-edit-data-description').val();
            data.address     = $('#input-modal-edit-data-address').val();
            data.enabled     = $('#modal-edit-enabled-select').val();
            data.visible     = $('#modal-edit-visible-select').val();  
            
            
            //Obj_Params.msg = 'Operación exitosa';
            if(!(data.action == '')){
               EjecutarAjaxNew(data, companiesAction(), 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
            }
    }//Fin updateDataCountry

    /**
     * Función que permite agrgar nueva money
     */
    function addData(){
                    

                    var data = new Object();
                                
                    var Obj_Params = new Object();
                              
                    //var idbtn = $(this).prop('id');
        
                    data.action      = 'add'; 
                    data.idcountry   = $('#select-countries').val();
                    data.idcity      = $('#select-cities').val();
                    data.name        = $('#input-modal-add-name').val();                    
                    data.code        = $('#input-modal-add-code').val();                    
                    data.description = $('#input-modal-add-description').val();
                    data.address     = $('#input-modal-add-address').val();
                    data.enabled     = $('#modal-add-enabled-select').val();
                    data.visible     = $('#modal-add-visible-select').val();  

        
                    //Obj_Params.msg = 'Operación exitosa';
                    if(!(data.action == '')){
                       EjecutarAjaxNew(data, companiesAction(), 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
                    }
    }//Fin updateDataCountry


    //**Cargar las ciudades en select-cities */
    function loadcities(datos){
        if(datos['_ok'] == '0'){            
            
            $('#select-cities').empty();
            $('#select-cities').append($('<option>', {
                    value: -1,
                    text: 'City',
                    selected: 'selected',
                    hidden: 'hidden'
            }));            
            for (item in datos._data)
            {                                                                                              
                $('#select-cities').append($('<option>', {
                    value: datos._data[item].id,
                    text: datos._data[item].name,                    
                 }));            
            }            
        }
    }//loadcities

    /**
     * Función que permite buscar cities by country
     */
    function citiesbycountry(){
                    

                    var data = new Object();
                                
                    var Obj_Params = new Object();
                              
                    //var idbtn = $(this).prop('id');
                            
                    data.idcountry   = $('#select-countries').val();                     
                                                
                    EjecutarAjaxNew(data, companiesCitiesbyCountries(), 'post', Obj_Params, loadcities);  //(funciones_js.js)            
                    
    }//Fin citiesbycountry



})//Fin $(function(){

