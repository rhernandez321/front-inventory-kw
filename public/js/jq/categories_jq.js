$(function(){
      
    
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });


    //Leer la variable de php $err_msg y verificar si tiene o no valor
     
    /*if(!(@json($error_msg) == '')){                     
            $("#modal-msg").modal("show");
           
    }*/

     /** Realizar la primera recarga de data */
    researchData('');
    
    
        
    /**Se ejecuta cuando se recibe una respuesta sactifactoria del ajax que actualiza */    
    function successUpdate(datos) {                 
        if(datos['_ok'] == '0'){
            closeModal('.btn-close'); //Indicamos la clase de cerrar modal
            refreshDataTable()
        }
        
        showModalMessage(datos['_msg']); //(funciones_js.js)
    }

   
    /**
    Función que realiza la carga el datatable de la data */
    function researchData($datafind){
        
        //Botones update y delete
        var btn_html = "<button type='button' class='edit btn btn-outline-warning' data-toggle='modaledit' data-target='#modaledit'><span class='fa fa-wrench' aria-hidden='true'></span></button>"
        btn_html = btn_html + "&nbsp;&nbsp;<button type='button' class='delete btn btn-outline-danger' data-toggle='modaledit' data-target='#modaldelete'><span class='fa fa-trash' aria-hidden='true'></span></button>"
                
        var datatable = $('#tablecategories').DataTable({
         
           // processing: true,
           // serverSide: true,
            //serverMethod: 'post',
            responsive: true,
            
            ajax: {url: categoriesResearch(),         
                    type: "post",
                    //dataSrc: "_data",
                    data: {"idcategory": $datafind},
                    dataSrc: function (data) {
                        console.log(data);
                        if (data['_ok'] == '0')
                        {                            
                            console.log(data["_data"]);
                            return data['_data'];
                        }else{
                            console.log(data["_msg"]);
                            showModalMessage(data['_msg']);                            
                            data['_data'] = [];
                            return data['_data']
                        }                           
                    } 
            },
            
            columns: [
                        {data:"id"},
                        {data:"code"},
                        {data:"name"},                        
                        {data:"description"},
                        {
                            bSortable: false,
                            data:"enabled",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }
                        },
                        {   bSortable: false,
                            data:"visible",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }
                        },
                        {  
                            bSortable: false,
                            data: null, 
                            "defaultContent": btn_html                           
                        }
                        
                        
                       /*Concatena campos 
                       { 
                            "data": null,
                            "className": "button",
                            "defaultContent": '<button name="vcvcvc" id="' + {data: "name"} + '" value="id" OnClick="EditRow(this);"  class="btn btn-primary" data-toggle="modal" data-target="#myModal">Editar</button>'
                        }*/                                            
                        
            ],     
            columnDefs: [
                          { "visible": false, "targets": [0] },
                          
                        ],         
            select:true
        });

      //  editar("#tablecountries",datatable);
    }//Fin researchcountries
         

    /**
    Capturar la acción clip de los botones de la tabla countries
     */
    $('#tablecategories').on('click', "[data-toggle='modaledit']", function(){
           
           table = $('#tablecategories').DataTable();  
           var row = $(this).parents('tr')[0]; 
           var data = table.row( row ).data();
                     
           console.log( data);
                     
           if($(this).hasClass("edit")){            
                $('#input-modal-edit-data-idcategory').val(data.id);
                $('#input-modal-edit-data-code').val(data.code);   
                $('#input-modal-edit-data-code').prop('disabled', false);                             
                $('#input-modal-edit-data-name').val(data.name);
                $('#input-modal-edit-data-name').prop('disabled', false);
                $('#input-modal-edit-data-description').val(data.description);
                $('#input-modal-edit-data-description').prop('disabled', false);
                $('#modal-edit-enabled-select').val(data.enabled);
                $('#modal-edit-visible-select').val(data.visible);

                $('#div-modal-edit-select-visible').show();
                $('#div-modal-edit-select-enabled').show();
                $('#btt-modal-edit-save').show();                            
                $('#btt-modal-delete').hide();
                                
                //$('#modal-edit-data').modal({backdrop: 'static', keyboard: false})
            
                $("#modal-edit-data").modal("show");                                 
           }            
           else{
                                
                $('#input-modal-edit-data-idcategory').val(data.id);
                $('#input-modal-edit-data-code').prop('disabled', true);
                $('#input-modal-edit-data-code').val(data.code); 
                $('#input-modal-edit-data-name').prop('disabled', true);
                $('#input-modal-edit-data-name').val(data.name);        
                $('#input-modal-edit-data-description').prop('disabled', true);
                $('#input-modal-edit-data-description').val(data.description);         
                $('#div-modal-edit-select-visible').hide();
                $('#div-modal-edit-select-enabled').hide();
                $('#btt-modal-edit-save').hide();                            
                $('#btt-modal-delete').show();                            
                
                $("#modal-edit-data").modal("show");                   
                        
           }
                     
   });
  
   
    /**
    Mostrar modal add
     */
    $('#btn-add').on({click:                
        function (event)
        {            
            event.preventDefault();  
           
            $('#input-modal-add-code').val('');
            $('#input-modal-add-name').val('');
            $('#input-modal-add-description').val('');
                        
            $("#modal-add-categories").modal('show');
                                                                                                                                       
        }//function (event)
    }); // 
    
    /**
    Función que ejecuta la acción de refresch del datatable
     */
    function refreshDataTable()
    {                 
            if ( $.fn.dataTable.isDataTable( '#tablecategories' ) ) {
               
                $('#tablecategories').DataTable().searching = false;
                $('#tablecategories').DataTable().paging = false;
                $('#tablecategories').DataTable().destroy();
            }       

            researchData($('#input_find_categories').val());                                        
    }     

    
    /**Ejecutar la acción de agregar categories */
    $('#btt-modal-add').on({click:
        function (event)
        {     
        event.preventDefault(); 
        
        if($('#input-modal-add-code').val() === ''){               
                $("#error-msg").text("Insert category code.");                 
                $("#modal-msg").modal("show");                                     
        }   
        else  
        if($('#input-modal-add-name').val() === ''){
            $("#error-msg").text("Insert category name.");
            $("#modal-msg").modal("show");
        }
        else  
        if($('#input-modal-add-description').val() === ''){
            $("#error-msg").text("Insert category description.");
            $("#modal-msg").modal("show");
        } 
        else{
            //$("form").submit();  
            //$("#form-modal-add-countrie").submit();  
            $('#div-confirm-data-text').html('Add Category?');
            $('#btt-modal-confirm').addClass('add');            
            $("#modal-confirm-data").modal('show');
        }     
                                        
            
        }
    }); //Fin $('#id-input-send-add-country').on({click:

        
    $('#btt-modal-confirm').on({click:    
            function (event)
            {   
                
                event.preventDefault();  
                                           
                $("#modal-confirm-data").modal('hide');
    
                if($(this).hasClass("edit")){                     
                    $('#btt-modal-confirm').removeClass('edit');
                    updateData('edit');
                }
                else
                if($(this).hasClass("delete")){                   
                    $('#btt-modal-confirm').removeClass('delete'); 
                    updateData('delete');
                }
                else
                if($(this).hasClass("add")){
                    $('#btt-modal-confirm').removeClass('add'); 
                    //$("#form-modal-add-countrie").submit(); 
                    addData();
                }
                            
            }
     });//$('#b


    /**
    Función que ejecuta la acción de búsqueda de categories
     */
    $('#btn-find-categories').on({click:
         function (event)
         { 
            event.preventDefault(); 
            
            refreshDataTable();            
                                               
           // researchcountries($('#input_find_categories').val());
         }
    })

    //Capturar el evento enter para la búsqueda    
    
    $("#input_find_categories").keypress(function(e) {
        // event.preventDefault(); 
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            //$('#btn-find-colors').click(); 
            refreshDataTable();            
            return false;
        }
    
    });


    /**
     * Function que ejecuta el ajax de actualización de la data (update, delete)
     *  */ 
    function updateData(action){
                    

            var data = new Object();
                        
            var Obj_Params = new Object();
                      
            var idbtn = $(this).prop('id');
          
            data.action      = action;                                                          
            data.id          = $('#input-modal-edit-data-idcategory').val();    
            data.code        = $('#input-modal-edit-data-code').val();                
            data.name        = $('#input-modal-edit-data-name').val();
            data.description = $('#input-modal-edit-data-description').val();
            data.enabled     = $('#modal-edit-enabled-select').val();
            data.visible     = $('#modal-edit-visible-select').val();  
            
            //Obj_Params.msg = 'Operación exitosa';
            if(!(data.action == '')){
               EjecutarAjaxNew(data, categoriesAction(), 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
            }
    }//Fin updateDataCountry

    /**
     * Función que permite agrgar nueva city
     */
    function addData(){
                    

                    var data = new Object();
                                
                    var Obj_Params = new Object();
                              
                    var idbtn = $(this).prop('id');
        
                    data.action      = 'add';                                                                                                  
                    data.code        = $('#input-modal-add-code').val();                
                    data.name        = $('#input-modal-add-name').val();
                    data.description = $('#input-modal-add-description').val();
                    data.enabled     = $('#modal-add-enabled-select').val();
                    data.visible     = $('#modal-add-visible-select').val();  

        
                    //Obj_Params.msg = 'Operación exitosa';
                    if(!(data.action == '')){
                       EjecutarAjaxNew(data, categoriesAction(), 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
                    }
            }//Fin updateDataCountry




})//Fin $(function(){
