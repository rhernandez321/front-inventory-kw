$(function(){
      
    
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    
     /** Realizar la primera recarga de data */
    researchData('');


    /**Elegir el providers y buscar managersbyproviders*/    
    $('#select-providers').on({change: 
         function (event)
         {                               
             researchData($(this).val());                                       
         }
    });//Fin $('#select-provider).on({change: 
        
    /**Se ejecuta cuando se recibe una respuesta sactifactoria del ajax que actualiza */    
    function successUpdate(datos) {          
       if(datos['_ok'] == '0'){
            closeModal('.btn-close'); //Indicamos la clase de cerrar modal
            refreshDataTable()
        }
        showModalMessage(datos['_msg']); //(funciones_js.js)
                
    }

   
    /**
    Función que realiza la carga el datatable de la data */
    function researchData($_idprovider){
                
        //Botones update y delete
        var btn_html = "<button type='button' class='edit btn btn-outline-warning' data-toggle='modaledit' data-target='#modaledit'><span class='fa fa-wrench' aria-hidden='true'></span></button>"
        btn_html = btn_html + "&nbsp;&nbsp;<button type='button' class='delete btn btn-outline-danger' data-toggle='modaledit' data-target='#modaldelete'><span class='fa fa-trash' aria-hidden='true'></span></button>"
                
        var datatable = $('#tablemanagersbyproviders').DataTable({
         
           // processing: true,
           // serverSide: true,
            //serverMethod: 'post',
            responsive: true,
            destroy: true,
            
            ajax: {url: managersbyprovidersResearch(),         
                    type: "post",
                    //dataSrc: "_data",
                    data: {"idprovider":$_idprovider,                           
                          },                    
                    dataSrc: function (data) {
                        console.log(data);
                        if (data['_ok'] == '0')
                        {                            
                            console.log(data["_data"]);
                            return data['_data'];
                        }else{
                            console.log(data["_msg"]);
                            showModalMessage(data['_msg']);                            
                            data['_data'] = [];
                            return data['_data']
                        }                           
                    } 
            },
            
            columns: [
                        {data:"id"},
                        {data:"code"},
                        {data:"name"},
                        {data:"lastname"},
                        {data:"address"},                                                                        
                        {data:"phone1"},                                                
                        {data:"phone2"},                                                
                        {data:"phone3"},                                                                      
                        {data:"note"},                          
                        {
                            bSortable: false,
                            data:"enabled",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }
                        },
                        {   bSortable: false,
                            data:"visible",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }
                        },
                        {  
                            bSortable: false,
                            data: null, 
                            "defaultContent": btn_html                           
                        }
                        
                        
                       /*Concatena campos 
                       { 
                            "data": null,
                            "className": "button",
                            "defaultContent": '<button name="vcvcvc" id="' + {data: "name"} + '" value="id" OnClick="EditRow(this);"  class="btn btn-primary" data-toggle="modal" data-target="#myModal">Editar</button>'
                        }*/                                            
                        
            ],     
            columnDefs: [
                          { "visible": false, "targets": [0] },
                          
                        ],         
            select:true
        });

      //  editar("#tablecountries",datatable);
    }//Fin researchcountries
         

    /**
    Capturar la acción clip de los botones de la tabla countries
     */
    $('#tablemanagersbyproviders').on('click', "[data-toggle='modaledit']", function(){
           
           table = $('#tablemanagersbyproviders').DataTable();  
           var row = $(this).parents('tr')[0]; 
           var data = table.row( row ).data();
                     
           console.log( data);           
           
           $('#input-modal-edit-data-idmanagersbyproviders').val(data.id);        
           $('#input-modal-edit-data-provider').val($('#select-providers option:selected').text());   
    
           
           if($(this).hasClass("edit")){                          
                $('#input-modal-edit-data-code').val(data.code);   
                $('#input-modal-edit-data-code').prop('disabled', false);                                             
                $('#input-modal-edit-data-name').val(data.name);
                $('#input-modal-edit-data-name').prop('disabled', false);            
                $('#input-modal-edit-data-lastname').val(data.lastname);
                $('#input-modal-edit-data-lastname').prop('disabled', false);            
                $('#input-modal-edit-data-note').val(data.note);
                $('#input-modal-edit-data-note').prop('disabled', false);
                $('#input-modal-edit-data-address').val(data.address);
                $('#input-modal-edit-data-address').prop('disabled', false);
                $('#input-modal-edit-data-phone1').val(data.phone1);
                $('#input-modal-edit-data-phone1').prop('disabled', false);
                $('#input-modal-edit-data-phone2').val(data.phone2);
                $('#input-modal-edit-data-phone2').prop('disabled', false);
                $('#input-modal-edit-data-phone3').val(data.phone3);
                $('#input-modal-edit-data-phone3').prop('disabled', false);
                $('#modal-edit-enabled-select').val(data.enabled);
                $('#modal-edit-visible-select').val(data.visible);

                $('#div-modal-edit-select-visible').show();
                $('#div-modal-edit-select-enabled').show();
                $('#btt-modal-edit-save').show();                            
                $('#btt-modal-delete').hide();
                                
                //$('#modal-edit-data').modal({backdrop: 'static', keyboard: false})
            
                $("#modal-edit-data").modal("show");                                 
           }            
           else{                                
               // $('#input-modal-edit-data-idalloy').val(data.id);
                $('#input-modal-edit-data-code').prop('disabled', true);
                $('#input-modal-edit-data-code').val(data.code);                
                $('#input-modal-edit-data-name').prop('disabled', true);
                $('#input-modal-edit-data-name').val(data.name);                        
                $('#input-modal-edit-data-lastname').prop('disabled', true);
                $('#input-modal-edit-data-lastname').val(data.lastname);                        
                $('#input-modal-edit-data-note').prop('disabled', true);
                $('#input-modal-edit-data-note').val(data.note);         
                $('#input-modal-edit-data-address').prop('disabled', true);
                $('#input-modal-edit-data-address').val(data.address);         
                $('#input-modal-edit-data-phone1').prop('disabled', true);
                $('#input-modal-edit-data-phone1').val(data.phone1);         
                $('#input-modal-edit-data-phone2').prop('disabled', true);
                $('#input-modal-edit-data-phone2').val(data.phone2);         
                $('#input-modal-edit-data-phone3').prop('disabled', true);
                $('#input-modal-edit-data-phone3').val(data.phone3);         
                $('#div-modal-edit-select-visible').hide();
                $('#div-modal-edit-select-enabled').hide();
                $('#btt-modal-edit-save').hide();                            
                $('#btt-modal-delete').show();                            
                
                $("#modal-edit-data").modal("show");                   
                        
           }
                     
   });
  
   
    /**
    Mostrar modal add
     */
    $('#btn-add').on({click:                
        function (event)
        {            
            event.preventDefault(); 
             
                       
            $('#input-modal-add-code').val('');            
            $('#input-modal-add-name').val('');            
            $('#input-modal-add-lastname').val('');           
            $('#input-modal-add-note').val('');
            $('#input-modal-add-address').val('');
            $('#input-modal-add-phone1').val('');
            $('#input-modal-add-phone2').val('');
            $('#input-modal-add-phone3').val('');

            $('#input-modal-add-provider').val($('#select-providers option:selected').text());
            
            if(!($('#select-providers').val() == -1)){                        
                $("#modal-add").modal('show');
            }
            else{
                showModalMessage('Choose a provider.'); //(funciones_js.js)                
            }
                                                                                                                                       
        }//function (event)
    }); // 
    
    /**
    Función que ejecuta la acción de refresch del datatable
     */
    function refreshDataTable()
    {                             
            researchData($('#select-providers').val());                                        
    }     

    
    /**Ejecutar la acción de agregar */
    $('#btt-modal-add').on({click:
        function (event)
        {     
            event.preventDefault(); 
                    
            if($('#input-modal-add-code').val() === ''){               
                $("#error-msg").text("Insert code.");                 
                $("#modal-msg").modal("show");                                     
            }                
            else  
            if($('#input-modal-add-name').val() === ''){
                $("#error-msg").text("Insert name.");
                $("#modal-msg").modal("show");
            }
            else  
            if($('#input-modal-add-lastname').val() === ''){
                $("#error-msg").text("Insert lastname.");
                $("#modal-msg").modal("show");
            }
            else  
            if($('#input-modal-add-address').val() === ''){
                $("#error-msg").text("Insert address.");
                $("#modal-msg").modal("show");
            }            
            else  
            if($('#input-modal-add-phone1').val() === ''){
                $("#error-msg").text("Insert phone 1.");
                $("#modal-msg").modal("show");
            }
            else{
                //$("form").submit();  
                //$("#form-modal-add-countrie").submit();  
            
                $('#div-confirm-data-text').html('Add managers by providers?');
                $('#btt-modal-confirm').addClass('add');            
                $("#modal-confirm-data").modal('show');
            
            }
             
        }
    }); //Fin $('#id-input-send-add-managersbyproviders').on({click:
     
    $('#btt-modal-confirm').on({click:    
            function (event)
            {   
                
                event.preventDefault();  
                                           
                $("#modal-confirm-data").modal('hide');
    
                if($(this).hasClass("edit")){                     
                    $('#btt-modal-confirm').removeClass('edit');
                    updateData('edit');
                }
                else
                if($(this).hasClass("delete")){                   
                    $('#btt-modal-confirm').removeClass('delete'); 
                    updateData('delete');
                }
                else
                if($(this).hasClass("add")){
                    $('#btt-modal-confirm').removeClass('add'); 
                    //$("#form-modal-add-countrie").submit(); 
                    addData();
                }
                            
            }
     });//$('#b
            


    function validate_data(){
            if($('#input-modal-edit-data-code').val() === ''){               
                $("#error-msg").text("Insert code.");                 
                $("#modal-msg").modal("show");                                     
                return false;
            }                
            else  
            if($('#input-modal-edit-data-name').val() === ''){
                $("#error-msg").text("Insert name.");
                $("#modal-msg").modal("show");
                return false;
            }
            else  
            if($('#input-modal-edit-data-lastname').val() === ''){
                $("#error-msg").text("Insert lastname.");
                $("#modal-msg").modal("show");
                return false;
            }
            else  
            if($('#input-modal-edit-data-address').val() === ''){
                $("#error-msg").text("Insert address.");
                $("#modal-msg").modal("show");
                return false;
            }            
            else  
            if($('#input-modal-edit-data-phone1').val() === ''){
                $("#error-msg").text("Insert phone 1.");
                $("#modal-msg").modal("show");
                return false;
            }
            else
                return true;

    }


    /**
     * Function que ejecuta el ajax de actualización de la data (update, delete)
     *  */ 
    function updateData(action){
                    

            var data = new Object();
                        
            var Obj_Params = new Object();
                      
            //var idbtn = $(this).prop('id');

            if(validate_data()){
            
                    data.action      = action;
                
                    data.id          = $('#input-modal-edit-data-idmanagersbyproviders').val();    
                    data.code        = $('#input-modal-edit-data-code').val();                            
                    data.name        = $('#input-modal-edit-data-name').val();            
                    data.lastname    = $('#input-modal-edit-data-lastname').val();            
                    data.note        = $('#input-modal-edit-data-note').val();
                    data.address     = $('#input-modal-edit-data-address').val();
                    data.phone1      = $('#input-modal-edit-data-phone1').val();
                    data.phone2      = $('#input-modal-edit-data-phone2').val();
                    data.phone3      = $('#input-modal-edit-data-phone3').val();
                    data.enabled     = $('#modal-edit-enabled-select').val();
                    data.visible     = $('#modal-edit-visible-select').val();  
                    
                    
                    //Obj_Params.msg = 'Operación exitosa';
                    if(!(data.action == '')){
                        EjecutarAjaxNew(data, managersbyprovidersAction(), 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
                    }
            }
    }//Fin updateDataCountry

    /**
     * Función que permite agrgar nueva money
     */
    function addData(){
                    

                    var data = new Object();
                                
                    var Obj_Params = new Object();
                              
                    //var idbtn = $(this).prop('id');
        
                    data.action      = 'add'; 
                    data.idprovider  = $('#select-providers').val();                    
                    data.name        = $('#input-modal-add-name').val();                    
                    data.lastname    = $('#input-modal-add-lastname').val();                    
                    data.code        = $('#input-modal-add-code').val();                    
                    data.note        = $('#input-modal-add-note').val();
                    data.address     = $('#input-modal-add-address').val();
                    data.phone1      = $('#input-modal-add-phone1').val();
                    data.phone2      = $('#input-modal-add-phone2').val();
                    data.phone3      = $('#input-modal-add-phone3').val();                    
                    data.enabled     = $('#modal-add-enabled-select').val();
                    data.visible     = $('#modal-add-visible-select').val();  

        
                    //Obj_Params.msg = 'Operación exitosa';
                    if(!(data.action == '')){
                       EjecutarAjaxNew(data, managersbyprovidersAction(), 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
                    }
    }//Fin updateDataCountry

})//Fin $(function(){