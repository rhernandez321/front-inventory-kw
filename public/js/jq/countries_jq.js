$(function(){
      
    
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });


         //Leer la variable de php $err_msg y verificar si tiene o no valor
     
/*     if(!(@json($error_msg) == '')){                     
            $("#modal-msg").modal("show");
                        
            /* $("#error-msg").modal('show', {                    
                 backdrop: 'static',   // This disable for click outside event
                 keyboard: true       // This for keyboard event
              })*-/
     }
*/


    researchcountries('');

    
    /**Se ejecuta cuando se recibe una respuesta sactifactoria del ajax que actualiza */    
    function successUpdateCountries(datos) {  
       
        if(datos['_ok'] == '0'){            
            closeModal('.btn-close'); //Indicamos la clase de cerrar modal
            refreshDataTableCountries()
        }
        
        showModalMessage(datos['_msg']); //(funciones_js.js)
    }

    /**
    Función que realiza la carga el datatable coutries */
    function researchcountries($datafind){
        
        //Agregar botones  update y delete
        var btn_html = "<button type='button' class='edit btn btn-outline-warning' data-toggle='modaledit' data-target='#modaledit'><span class='fa fa-wrench' aria-hidden='true'></span></button>"
        
        btn_html = btn_html + "&nbsp;&nbsp;<button type='button' class='delete btn btn-outline-danger' data-toggle='modaledit' data-target='#modaldelete'><span class='fa fa-trash' aria-hidden='true'></span></button>"
        
        var datatable = $('#tablecountries').DataTable({
          //var table = $('#tablecountries').DataTable({
       
            //processing: true,
            //serverSide: true,
            //serverMethod: 'post',

            destroy: true,
            responsive: true,
            
            //El original
            //ajax: {url: "{{ route('countries_research') }}",         

            //countries_route: Variable global que contiene la ruta countries_research
            ajax: {url: countriesRoute(), //countries_route,         
                    type: "post",
                    data: {"find_country": $datafind},
                    //dataSrc: "_data",
                   
                    dataSrc: function (data) {
                        console.log(data);
                        if (data['_ok'] == '0')
                        {                            
                            console.log(data["_data"]);
                            return data['_data'];
                        }else{
                            console.log(data["_msg"]);
                            showModalMessage(data['_msg']);                            
                            data['_data'] = [];
                            return data['_data']
                        }                           
                    } 
                                            
            },
                    
            columns: [
                        {data:"id"},
                        {data:"countrycode"},
                        {data:"name"},
                        {
                            bSortable: false,
                            data:"enabled",
                            
                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }                            
                        },
                        {   bSortable: false,
                            data:"visible",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                            }
                            }
                        },
                        {  

                            bSortable: false,
                            data: null, 
                            "defaultContent":btn_html

                        }
                        
                        
                      
                        
            ],     
            columnDefs: [
                          { "visible": false, "targets": [0] },                                                    
                        ],         
            select:true
        });

      //  editar("#tablecountries",datatable);
    }//Fin researchcountries

    
/**
    Capturar la acción clip de los botones de la tabla countries
     */
    $('#tablecountries').on('click', "[data-toggle='modaledit']", function(){
           
           table = $('#tablecountries').DataTable();  
           var row = $(this).parents('tr')[0]; 
           var data = table.row( row ).data();
                     
           console.log( data);
                      
           if($(this).hasClass("edit")){
                
                $('#input-modal-edit-data-idcountry').val(data.id);
                $('#input-modal-edit-data-countrycode').val(data.countrycode);   
                $('#input-modal-edit-data-countrycode').prop('disabled', false);                             
                $('#input-modal-edit-data-countryname').val(data.name);
                $('#input-modal-edit-data-countryname').prop('disabled', false);

                $('#modal-edit-enabled-select').val(data.enabled);
                $('#modal-edit-visible-select').val(data.visible);

                $('#div-modal-edit-select-visible').show();
                $('#div-modal-edit-select-enabled').show();
                $('#btt-modal-edit-save').show(); 
                                 
                $('#btt-modal-delete').hide();
                                
                //$('#modal-edit-data').modal({backdrop: 'static', keyboard: false})
            
                $("#modal-edit-data").modal("show");                                 
           }            
           else{
                                
                $('#input-modal-edit-data-idcountry').val(data.id);
                $('#input-modal-edit-data-countrycode').prop('disabled', true);
                $('#input-modal-edit-data-countrycode').val(data.countrycode); 
                $('#input-modal-edit-data-countryname').prop('disabled', true);
                $('#input-modal-edit-data-countryname').val(data.name);                
                $('#div-modal-edit-select-visible').hide();
                $('#div-modal-edit-select-enabled').hide();
                $('#btt-modal-edit-save').hide();                            
                $('#btt-modal-delete').show();                            
                
                $("#modal-edit-data").modal("show");                   
                        
           }
                     
   });
  
   
    /**
    Ocultar o mostrar div add-countries
     */
    $('#btn-add-country').on({click:                
        function (event)
        {            
            event.preventDefault(); 
           
            $('#input-modal-add-countrycode').val('');
            $('#input-modal-add-countryname').val('');
            $("#modal-add-countries").modal('show');
                                                                                                                                       
        }//function (event)
    }); // 

    /**
    Función que ejecuta la acción de refresch del datatable
     */
    function refreshDataTableCountries()
    {                 
            /*if ( $.fn.dataTable.isDataTable( '#tablecountries' ) ) {
               
                $('#tablecountries').DataTable().searching = false;
                $('#tablecountries').DataTable().paging = false;
                $('#tablecountries').DataTable().destroy();
            }       */
            
            researchcountries($('#input_find_country').val());                                        
    }     

    
    /**
    Función que ejecuta la acción de búsqueda de countries cuando el usuario indica la data a buscar
     */
    $('#btn-find-countries').on({click:
         function (event)
         {
            event.preventDefault();
                      
            /*if ( $.fn.dataTable.isDataTable( '#tablecountries' ) ) {
               
                $('#tablecountries').DataTable().paging = false;
                $('#tablecountries').DataTable().searching = false;
                $('#tablecountries').DataTable().destroy();
            }*/
                       
            researchcountries($('#input_find_country').val());
         }
    })     

  
    //Capturar el evento enter para la búsqueda
    $("#input_find_country").keypress(function(e) {
       // event.preventDefault(); 
       
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            refreshDataTableCountries();            
            return false;
        }
      
    });


    /**Ejecutar la acción de agregar countries */
    $('#btt-modal-add-countries').on({click:
        function (event)
        {     
            event.preventDefault();          
            
            if($('#input-modal-add-countrycode').val() === ''){       
            
                    $("#error-msg").text("Insert country code");                 
                    $("#modal-msg").modal("show");                                     
            }   
            else  
            if($('#input-modal-add-countryname').val() === ''){
                $("#error-msg").text("Insert country name");
                $("#modal-msg").modal("show");
            } 
            else{
                //$("form").submit();  
                //$("#form-modal-add-countrie").submit();  
                $('#div-confirm-data-text').html('Add Country?');            
                $('#btt-modal-confirm').addClass('add');
                $("#modal-confirm-data").modal('show');
            }     
                                        
            
        }
    }); //Fin $('#id-input-send-add-country').on({click:

 
    $('#btt-modal-confirm').on({click:    
            function (event)
            {   
                event.preventDefault();  
               
                $("#modal-confirm-data").modal('hide');
                
                if($(this).hasClass("edit")){                          
                    $('#btt-modal-confirm').removeClass('edit');
                    updateDataCountries('edit');
                }
                else
                if($(this).hasClass("delete")){                          
                    $('#btt-modal-confirm').removeClass('delete'); 
                    updateDataCountries('delete');
                }
                else
                if($(this).hasClass("add")){                    
                    $('#btt-modal-confirm').removeClass('add'); 
                    $("#form-modal-add-countrie").submit();                   
                }
                            
            }
     });//$('#b


    function updateDataCountries(action){
                    

            var data = new Object();
                        
            var Obj_Params = new Object();
                      
            var idbtn = $(this).prop('id');

                       
            data.action  = action;                                                          
            data.id          = $('#input-modal-edit-data-idcountry').val();    
            data.countrycode = $('#input-modal-edit-data-countrycode').val();                
            data.name        = $('#input-modal-edit-data-countryname').val();
            data.enabled     = $('#modal-edit-enabled-select').val();
            data.visible     = $('#modal-edit-visible-select').val();  

            Obj_Params.msg = 'Operación exitosa';
            if(!(data.action == '')){
                
               //EjecutarAjaxNew(data, '{{ route("countries.action") }}', 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
               EjecutarAjaxNew(data, countriesAction(), 'post', Obj_Params, successUpdateCountries);  //(funciones_js.js)            
            }
    }//Fin updateDataCountry


})//Fin $(function(){
