$(function(){
     
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

   //Manejo de datetimepicker
    $('.input-group.date').datepicker({                  
        format: 'yyyy-mm-dd',
        endDate: '-0d',
        todayBtn: "linked",
        todayHighlight: true,        
        autoclose: true,        
    });
    
  
     //$(".custom-file-input").on("change", function() {
   /* $("#customFileLang").on("change", function() {
        //var fileName = $(this).val().split("\\").pop();
        
        var fileName = $(this).val();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        
     });
*/
    /**Elegir el category y buscar subcategories*/
     $('#form-mp-select-category').on({change: 
         function (event)
         {               
            findSubcategoriesByCategories();            
            
         }
    });//Fin $('#select_countries').on({change: 

    /**Elegir el wharehouse y buscar locations asociados*/
    $('#form-mp-select-wharehouse').on({change: 
         function (event)
         {                                   
            findLocationsByWharehouse();            
         }
    });//Fin $('#select_countries').on({change:     

    /**Elegir location y buscar sections*/
    $('#form-mp-select-locations').on({change: 
         function (event)
         {                                   
            findSectionsByLocations();            
         }
    });//Fin $('#select_countries').on({change:     

    
    /**Se ejecuta cuando se recibe una respuesta sactifactoria del ajax que actualiza */    
    function successUpdate(datos) {              
       if('0' == datos['_ok']){
            closeModal('.btn-close'); //Indicamos la clase de cerrar modal
            refreshDataTable()
        }
        showModalMessage(datos['_msg']); //(funciones_js.js)                
    }
        
    /**
    Función que envía la orden de actualización (add, edit, delete)
     */
    $('#btn-add').on({click:
         function (event)
         {
           // event.preventDefault(); 
            
            updateData("add");
                                                                      
         }
    })
    
    //Previsualizar imagen
    
    $('#customFileLang').change(function(e) {
        previewImage(e);         
    });

    function previewImage(e){
        
        var file = e.target.files[0],
        
        imageType = /image.*/;
      
        if (!file.type.match(imageType))
         return;
    
        var reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
    }

    function fileOnload(e) {
        var result=e.target.result;
        $('#previewImg').attr("src",result);
    }

    /**
     * Buscar subcategories by categories
     */
    function findSubcategoriesByCategories(){
                                        
                    var data = new Object();
                                
                    var Obj_Params = new Object();
                              
                    //var idbtn = $(this).prop('id');
                    
                    data.idcategory  = $('#form-mp-select-category').val();                     
                    data.enabled  = 1;

                    Obj_Params.selectoptions = "form-mp-select-subcategory";
                   // alert(data.idcategory);                 
                    EjecutarAjaxNew(data, masterproductsSubcategories(), 'post', Obj_Params, loaddataintoselect);  //(funciones_js.js)            
                    
    }//Fin findSubcategoriesByCategories

    /**
     * Buscar locations by wharehouses
     */
    function findLocationsByWharehouse(){
                    
                    var data = new Object();
                                
                    var Obj_Params = new Object();
                              
                    //var idbtn = $(this).prop('id');
                            
                    data.idwharehouse  = $('#form-mp-select-wharehouse').val();                     
                    data.enabled  = 1;

                    Obj_Params.selectoptions = "form-mp-select-locations";
                   
                    EjecutarAjaxNew(data, masterproducts_locations(), 'post', Obj_Params, loaddataintoselect);  //(funciones_js.js)            
                                
    }//Fin findLocationsByWharehouse


    /**
     * Buscar section by locations
     */
    function findSectionsByLocations(){
                    
                    var data = new Object();
                                
                    var Obj_Params = new Object();
                              
                    //var idbtn = $(this).prop('id');
                            
                    data.idlocation  = $('#form-mp-select-locations').val();                     
                    data.enabled  = 1;

                    Obj_Params.selectoptions = "form-mp-select-sections";
                   
                    EjecutarAjaxNew(data, masterproductsSections(), 'post', Obj_Params, loaddataintoselect);  //(funciones_js.js)            
                                
    }//Fin findSectionsByLocations


    //**Cargar data */
    function loaddataintoselect(datos, obj){        
        
        if(datos['_ok'] == '0'){            
            let identificador = '#' + obj.selectoptions;
        
            //$('#form-mp-select-subcategory').empty();
            $(identificador).empty();
           /* $(identificador).append($('<option>', {
                    value: -1,
                    text: '',
                    selected: "selected"
            }));  */          

            $(identificador).append($('<option>', {                    
                    selected: "disabled"
                    
            }));
           
            for (item in datos._data)
            {                                                                                              
                $(identificador).append($('<option>', {
                    value: datos._data[item].id,
                    text: datos._data[item].name,                    
                 }));            
            }            
        }
        else
            showModalMessage(datos['_msg']); //(funciones_js.js)
    }//loaddata


    //**Cargar data input*/
    function loaddatainput(datos, obj){
       
        if(datos['_ok'] == '0'){            
            let identificador = '#' + obj.input;
               
            $(identificador).val(datos._data.factor);
        
        }
        else
            showModalMessage(datos['_msg']); //(funciones_js.js)
    }//loaddata


    //Se ha confirmado la actuaización de prodcut
    function ContirmUpdateProduct(datos){

        if('0' == datos['_ok']){
            //Remover la clase de validación de boostrap
            $('#form-master-products').removeClass('was-validated');
            
            $('#form-mp-input-code').val('');
            $('#form-mp-input-datepurchase').val('')
            $('#form-mp-input-nrofactura').val('')
            $('#form-mp-select-provider').val();
            $('#form-mp-select-color').val('');
            $('#form-mp-select-presentation').val('');
            $('#form-mp-select-wharehouse').val('');
            $('#form-mp-select-category').val('');
            $('#form-mp-select-subcategory').val(''); 
            $('#form-mp-select-review').val(''); 
            $('#form-mp-input-name').val('');
            $('#form-mp-input-description').val('');            
            $('#form-mp-input-qtv').val('');
            $('#form-mp-input-cost').val('');  
            $('#form-mp-input-price').val('');
            //$('#form-mp-select-moneytypes').val(-1);
            //$('#form-mp-input-moneyfactor').val('');
            $('#form-mp-select-enabled').val(1);
            //$('#form-mp-select-visible').val(1);                            
            $('#form-mp-select-status').val('');
            $('#form-mp-select-alloys').val('');  
            $('#form-mp-select-unit').val(''); 
            $('#form-mp-select-locations').val('');
            $('#form-mp-select-sections').val(''); 
            $('#customFileLang').val(''); 
        }
                
        showModalMessage(datos['_msg']); //(funciones_js.js)                

    }

    //Función que recive un array de elementos y verifica si los mismos contienen datos
    function validate_noempty(_elements){
        //alert(_elements.length);
        var ok=true;
        var element='';

        for(i=0; i < _elements.length; i++){
            
            element = '#' + _elements[i];
            if((null === $(element).val()) || ('' == $(element).val().trim())){            
                ok=false
                break;
            }
           // break;
        }
        return ok
    }//Fin validate_noempty

    /**
     * Function que ejecuta el ajax de actualización de la data (update, delete)
     *  */ 
    function updateData(action){
                    

                    var data = new Object();
                                
                    var Obj_Params = new Object();
                    var input = [];
                              
                    //var idbtn = $(this).prop('id');
        
                    
                     if("add" == action){
                         data.id = null;
                     }    
                     else{
                         data.id  = -1; //Aquí el id del producto a eliminar o modificar  
                     }
                     data.action      = action;
                                                    
                     data.idcompany       =   '1', //Provisional, hasta resolver lo de escoger la compañía 
                     data.iduser          =   '1', //Provisional, hasta resolver lo del usuario logueado
                     data.code            = $('#form-mp-input-code').val();
                     data.idprovider      = $('#form-mp-select-provider').val();
                     data.idcolor         = $('#form-mp-select-color').val();
                     data.idpresentation  = $('#form-mp-select-presentation').val();
                     data.idwharehouse    = $('#form-mp-select-wharehouse').val();
                     data.idcategory      = $('#form-mp-select-category').val();
                     data.idsubcategory   = $('#form-mp-select-subcategory').val(); 
                     data.idreview        = $('#form-mp-select-review').val(); 
                     data.name            = $('#form-mp-input-name').val();
                     data.description     = $('#form-mp-input-description').val();
                     data.qtv             = $('#form-mp-input-qtv').val();
                     data.cost            = $('#form-mp-input-cost').val();  
                     data.price           = $('#form-mp-input-price').val();                    
                     //data.idmoneytype     = $('#form-mp-input-moneytypes').val();
                     //data.moneyfactor     = $('#form-mp-input-moneyfactor').val();
                     data.enabled         = $('#form-mp-select-enabled').val();
                     //data.visible         = $('#form-mp-select-visible').val();                    
                     data.visible         = 1;                    
                     data.idstatus        = $('#form-mp-select-status').val();
                     data.idalloy         = $('#form-mp-select-alloys').val();  
                     data.idunit          = $('#form-mp-select-unit').val(); 
                     data.idlocation      = $('#form-mp-select-locations').val();
                     data.idsection       = $('#form-mp-select-sections').val(); 
                     data.nrobill          = $('#form-mp-input-nrofactura').val(); 
                     data.datebill        = $('#form-mp-input-datepurchase').val();
                     data.rutaimg         = $('.custom-file-label').html();//$('#customFileLang').val('').html();
                            
                     input_array = ['form-mp-input-nrofactura', 'form-mp-input-datepurchase','form-mp-input-code', 'form-mp-input-name', 'form-mp-select-category', 'form-mp-select-subcategory', 
                                    'form-mp-select-unit', 'form-mp-select-status', 'form-mp-input-qtv', 'form-mp-input-cost', 'form-mp-input-price',
                                    'form-mp-select-provider', 'form-mp-select-wharehouse', 'form-mp-select-locations',
                                    'form-mp-select-sections',
                                   ];
                                        

                     if(!('' == data.action)){
                        
                         if(validate_noempty(input_array))
                         {
                             EjecutarAjaxNew(data, masterproductsAction(), 'post', Obj_Params, ContirmUpdateProduct);  //(funciones_js.js)            
                         }                                
                         
                     }
                   
                   
    }//Fin updateData



})//Fin $(function(){