$(function(){
      
    
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });


    //Leer la variable de php $err_msg y verificar si tiene o no valor
/*     
    if(!(@json($error_msg) == '')){                     
            $("#modal-msg").modal("show");
           
     }
*/
     /** Realizar la primera recarga de data */
    researchData('');

    /**Se ejecuta cuando se recibe una respuesta sactifactoria del ajax que actualiza */    
    function successUpdate(datos) {          
       if(datos['_ok'] == '0'){
            closeModal('.btn-close'); //Indicamos la clase de cerrar modal
            refreshDataTable()
        }
        showModalMessage(datos['_msg']); //(funciones_js.js)
                
    }

   
    /**
    Función que realiza la carga el datatable de la data */
    function researchData($datafind){
                        
        //Botones update y delete
        var btn_html = "<button type='button' class='edit btn btn-outline-warning' data-toggle='modaledit' data-target='#modaledit'><span class='fa fa-wrench' aria-hidden='true'></span></button>"
        btn_html = btn_html + "&nbsp;&nbsp;<button type='button' class='delete btn btn-outline-danger' data-toggle='modaledit' data-target='#modaldelete'><span class='fa fa-trash' aria-hidden='true'></span></button>"
                
        var datatable = $('#tablemovementstypes').DataTable({
         
           // processing: true,
           // serverSide: true,
            //serverMethod: 'post',
            responsive: true,
            destroy: true,
            
            ajax: {url: movementsTypesResearch(),         
                    type: "post",
                    //dataSrc: "_data",
                    data: {"datafind":$datafind                           
                          },                    
                    dataSrc: function (data) {
                        console.log(data);
                        if (data['_ok'] == '0')
                        {                            
                            console.log(data["_data"]);
                            return data['_data'];
                        }else{
                            console.log(data["_msg"]);
                            showModalMessage(data['_msg']);                            
                            data['_data'] = [];
                            return data['_data']
                        }                           
                    } 
            },
            
            columns: [
                        {data:"id"},                        
                        {data:"code"},
                        {data:"name"},
                        {data:"description"},                                                                
                        {
                            bSortable: false,
                            data:"enabled",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }
                        },
                        {   bSortable: false,
                            data:"visible",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }
                        },
                        {  
                            bSortable: false,
                            data: null, 
                            "defaultContent": btn_html                           
                        }
                        
                        
                       /*Concatena campos 
                       { 
                            "data": null,
                            "className": "button",
                            "defaultContent": '<button name="vcvcvc" id="' + {data: "name"} + '" value="id" OnClick="EditRow(this);"  class="btn btn-primary" data-toggle="modal" data-target="#myModal">Editar</button>'
                        }*/                                            
                        
            ],     
            columnDefs: [
                          { "visible": false, "targets": [0] },
                          
                        ],         
            select:true
        });

      //  editar("#tablecountries",datatable);
    }//Fin researchcountries
         

    /**
    Capturar la acción clip de los botones de la tabla countries
     */
    $('#tablemovementstypes').on('click', "[data-toggle='modaledit']", function(){
           
           table = $('#tablemovementstypes').DataTable();  
           var row = $(this).parents('tr')[0]; 
           var data = table.row( row ).data();
                     
           console.log( data);           
           
           $('#input-modal-edit-data-idmovementstypes').val(data.id);                   
           $('#input_find_movementstypes').val('');
           
           if($(this).hasClass("edit")){                                        
                $('#input-modal-edit-data-code').val(data.code);
                $('#input-modal-edit-data-code').prop('disabled', true);            
                $('#input-modal-edit-data-name').val(data.name);
                $('#input-modal-edit-data-name').prop('disabled', false);            
                $('#input-modal-edit-data-description').val(data.description);
                $('#input-modal-edit-data-description').prop('disabled', false);
                $('#modal-edit-enabled-select').val(data.enabled);
                $('#modal-edit-visible-select').val(data.visible);

                $('#div-modal-edit-select-visible').show();
                $('#div-modal-edit-select-enabled').show();
                $('#btt-modal-edit-save').show();                            
                $('#btt-modal-delete').hide();
                                
                //$('#modal-edit-data').modal({backdrop: 'static', keyboard: false})
            
                $("#modal-edit-data").modal("show");                                 
           }            
           else{                                
               
                $('#input-modal-edit-data-code').prop('disabled', true);
                $('#input-modal-edit-data-code').val(data.code);                        
                $('#input-modal-edit-data-name').prop('disabled', true);
                $('#input-modal-edit-data-name').val(data.name);                        
                $('#input-modal-edit-data-description').prop('disabled', true);
                $('#input-modal-edit-data-description').val(data.description);                         
                $('#div-modal-edit-select-visible').hide();
                $('#div-modal-edit-select-enabled').hide();
                $('#btt-modal-edit-save').hide();                            
                $('#btt-modal-delete').show();                            
                
                $("#modal-edit-data").modal("show");                   
                        
           }
                     
   });
  
   
    /**
    Mostrar modal add
     */
    $('#btn-add').on({click:                
        function (event)
        {            
            event.preventDefault();  
           
            $('#input_find_movementstypes').val('');

            $('#input-modal-add-code').val('');            
            $('#input-modal-add-name').val('');            
            $('#input-modal-add-description').val('');
                        
            $("#modal-add").modal('show');                                                                                                                                                   

            //Verificar si existen lo movimientos básicos
            verifyMovemenstTypes();
        }//function (event)
    }); // 

        
    /**
    Función que ejecuta la acción de refresch del datatable
     */
    function refreshDataTable()
    {       /*          
            if ( $.fn.dataTable.isDataTable( '#tablemovementstypes' ) ) {
               
                $('#tablemovementstypes').DataTable().searching = false;
                $('#tablemovementstypes').DataTable().paging = false;
                $('#tablemovementstypes').DataTable().destroy();
            }       
            */
            researchData($('#input_find_movementstypes').val());                                        
    }     


    /**
    Función que ejecuta la acción de búsqueda
     */
    $('#btn-find-movementstypes').on({click:
         function (event)
         {
            event.preventDefault(); 
                    
            refreshDataTable();            
                                                          
         }
    })


    //Capturar el evento enter para la búsqueda
    $("#input_find_movementstypes").keypress(function(e) {
       // event.preventDefault(); 
       
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            refreshDataTable();            
            return false;
        }
      
    });

    /**Ejecutar la acción de agregar */
    $('#btt-modal-add').on({click:
        function (event)
        {     
            event.preventDefault(); 
            
           if($('#input-modal-add-code').val() == ''){
                $("#error-msg").text("Insert code.");
                $("#modal-msg").modal("show");
            }
            else
            //if($('#input-modal-add-countrycode').val() === ''){
            if($('#input-modal-add-name').val() === ''){                
                $("#error-msg").text("Insert name.");                
                $("#modal-msg").modal("show");
            }
            else  
            if($('#input-modal-add-description').val() == ''){
                $("#error-msg").text("Insert description.");
                $("#modal-msg").modal("show");
            }                        
            else{
                //$("form").submit();  
                //$("#form-modal-add-countrie").submit();  
            
                $('#div-confirm-data-text').html('Add movement type?');
                $('#btt-modal-confirm').addClass('add');            
                $("#modal-confirm-data").modal('show');
            
            }

           //$("#modal-confirm-data").modal('show');  
        }
    }); //Fin $('#id-input-send-add-country').on({click:
     
    $('#btt-modal-confirm').on({click:    
            function (event)
            {   
                
                event.preventDefault();  
                                           
                $("#modal-confirm-data").modal('hide');
    
                if($(this).hasClass("edit")){                     
                    $('#btt-modal-confirm').removeClass('edit');
                    updateData('edit');
                }
                else
                if($(this).hasClass("delete")){                   
                    $('#btt-modal-confirm').removeClass('delete'); 
                    updateData('delete');
                }
                else
                if($(this).hasClass("add")){
                    $('#btt-modal-confirm').removeClass('add'); 
                    //$("#form-modal-add-countrie").submit(); 
                    addData();
                }
                            
            }
     });//$('#b
            


    function validate_data(){
             
            if($('#input-modal-edit-data-name').val() === ''){
                $("#error-msg").text("Insert name.");
                $("#modal-msg").modal("show");
                return false;
            }
            else  
            if($('#input-modal-edit-data-description').val() === ''){
                $("#error-msg").text("Insert description.");
                $("#modal-msg").modal("show");
                return false;
            }                        
            else
                return true;

    }


    /**
     * Function que ejecuta el ajax de actualización de la data (update, delete)
     *  */ 
    function updateData(action){
                    

            var data = new Object();
                        
            var Obj_Params = new Object();
                      
            //var idbtn = $(this).prop('id');

            if(validate_data()){
            
                    data.action      = action;
                
                    data.id          = $('#input-modal-edit-data-idmovementstypes').val();                       
                    data.code        = $('#input-modal-edit-data-code').val();            
                    data.name        = $('#input-modal-edit-data-name').val();            
                    data.description = $('#input-modal-edit-data-description').val();                    
                    data.enabled     = $('#modal-edit-enabled-select').val();
                    data.visible     = $('#modal-edit-visible-select').val();  
                    
                    
                    //Obj_Params.msg = 'Operación exitosa';
                    if(!(data.action == '')){
                        EjecutarAjaxNew(data, movementsTypesAction(), 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
                    }
            }
    }//Fin updateDataCountry

    /**
     * Función que permite agrgar nueva money
     */
    function addData(){
                    
                    var data = new Object();
                              
                    var Obj_Params = new Object();
                                                
                    data.action         = 'add';                     
                    data.code           = $('#input-modal-add-code').val();                                        
                    data.name           = $('#input-modal-add-name').val();
                    data.description    = $('#input-modal-add-description').val();                    
                    data.enabled        = $('#modal-add-enabled-select').val();
                    data.visible        = $('#modal-add-visible-select').val();  
                  
                    //Obj_Params.msg = 'Operación exitosa';
                    if(!(data.action == '')){                       
                       EjecutarAjaxNew(data, movementsTypesAction(), 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
                    }
    }//Fin updateDataCountry

    //Mostrar el movementstype a crear
    function showMovementsExits(_datos){
        var other = 'other'

        //alert(_datos['_data']);
        
        if(other.toUpperCase() == _datos['_data'].toUpperCase()){
            $('#input-modal-add-code').prop('disabled', false);
            $('#input-modal-add-code').val('');
        }
        else{
            $('#input-modal-add-code').prop('disabled', true);
            $('#input-modal-add-code').val(_datos['_data']);
        }
        
    }

    function verifyMovemenstTypes(){
         var data = new Object();
        
         var Obj_Params = new Object();
            
        
         //Obj_Params.msg = 'Operación exitosa';
         if(!(data.action == '')){                       
            EjecutarAjaxNew(data, movementsTypesExist(), 'post', Obj_Params, showMovementsExits);  //(funciones_js.js)            
         }
    }//verifyMovemenstTypes

})//Fin $(function(){