
//Bloque JQuery
$(function(){

          
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});


//Leer la variable de php $err_msg y verificar si tiene o no valor
 /*
if(!(@json($error_msg) == '')){                     
        $("#modal-msg").modal("show");
       
 }
*/
 /** Realizar la primera recarga de data */
researchData('');
    
/**Se ejecuta cuando se recibe una respuesta sactifactoria del ajax que actualiza */    
function successUpdate(datos) {          
   if(datos['_ok'] == '0'){
        closeModal('.btn-close'); //Indicamos la clase de cerrar modal
        refreshDataTable()
    }
    showModalMessage(datos['_msg']); //(funciones_js.js)
            
}


/**
Función que realiza la carga el datatable de la data */
function researchData($datafind){
            
    //Botones update y delete
    var btn_html = "<button type='button' class='edit btn btn-outline-warning' data-toggle='modaledit' data-target='#modaledit'><span class='fa fa-wrench' aria-hidden='true'></span></button>"
    btn_html = btn_html + "&nbsp;&nbsp;<button type='button' class='delete btn btn-outline-danger' data-toggle='modaledit' data-target='#modaldelete'><span class='fa fa-trash' aria-hidden='true'></span></button>"
            
    var datatable = $('#tablemoneys').DataTable({
     
       // processing: true,
       // serverSide: true,
        //serverMethod: 'post',
        responsive: true,
        destroy: true,
        
        ajax: {url: moneyResearch(),         
                type: "post",
                //dataSrc: "_data",
                data: {"iddata": $datafind},                    
                dataSrc: function (data) {
                    console.log(data);
                    if (data['_ok'] == '0')
                    {                            
                        console.log(data["_data"]);
                        return data['_data'];
                    }else{
                        console.log(data["_msg"]);
                        showModalMessage(data['_msg']);                            
                        data['_data'] = [];
                        return data['_data']
                    }                           
                } 
        },
        
        columns: [
                    {data:"id"},
                    {data:"code"},
                    {data:"name"},
                    {data:"simbolo"},       
                    {data:"factor"},                 
                    {data:"description"},                        
                    {
                        bSortable: false,
                        data:"enabled",

                        render: function(data, type, row) {
                                        switch(data) {
                                        case 0 : return "No"; break;
                                        case 1 : return "Yes"; break;
                                        //default : return "N/A"; break;
                                    }
                        }
                    },
                    {   bSortable: false,
                        data:"visible",

                        render: function(data, type, row) {
                                        switch(data) {
                                        case 0 : return "No"; break;
                                        case 1 : return "Yes"; break;
                                        //default : return "N/A"; break;
                                    }
                        }
                    },
                    {   bSortable: false,
                        data:"mdefault",

                        render: function(data, type, row) {
                                        switch(data) {
                                        case 0 : return "No"; break;
                                        case 1 : return "Yes"; break;
                                        //default : return "N/A"; break;
                                    }
                        }
                    },
                    {  
                        bSortable: false,
                        data: null, 
                        "defaultContent": btn_html                           
                    }
                    
                    
                   /*Concatena campos 
                   { 
                        "data": null,
                        "className": "button",
                        "defaultContent": '<button name="vcvcvc" id="' + {data: "name"} + '" value="id" OnClick="EditRow(this);"  class="btn btn-primary" data-toggle="modal" data-target="#myModal">Editar</button>'
                    }*/                                            
                    
        ],     
        columnDefs: [
                      { "visible": false, "targets": [0] },
                      
                    ],         
        select:true
    });

  //  editar("#tablecountries",datatable);
}//Fin researchcountries
     

/**
Capturar la acción clip de los botones de la tabla countries
 */
$('#tablemoneys').on('click', "[data-toggle='modaledit']", function(){
       
       table = $('#tablemoneys').DataTable();  
       var row = $(this).parents('tr')[0]; 
       var data = table.row( row ).data();
                 
       console.log( data);           
       $('#input-modal-edit-data-idmoneys').val(data.id);          
       if($(this).hasClass("edit")){                            
            $('#input-modal-edit-data-code').val(data.code);   
            $('#input-modal-edit-data-code').prop('disabled', false);                             
            $('#input-modal-edit-data-simbol').val(data.simbolo);   
            $('#input-modal-edit-data-simbol').prop('disabled', false);                             
            $('#input-modal-edit-data-name').val(data.name);
            $('#input-modal-edit-data-name').prop('disabled', false);
            $('#input-modal-edit-data-factor').val(data.factor);
            $('#input-modal-edit-data-factor').prop('disabled', false);
            $('#input-modal-edit-data-description').val(data.description);
            $('#input-modal-edit-data-description').prop('disabled', false);
            $('#modal-edit-enabled-select').val(data.enabled);
            $('#modal-edit-visible-select').val(data.visible);
            $('#modal-edit-default-select').val(data.mdefault);

            $('#div-modal-edit-select-visible').show();
            $('#div-modal-edit-select-enabled').show();
            $('#modal-edit-default-select').show();
            $('#btt-modal-edit-save').show();                            
            $('#btt-modal-delete').hide();
                            
            //$('#modal-edit-data').modal({backdrop: 'static', keyboard: false})
        
            $("#modal-edit-data").modal("show");                                 
       }            
       else{                                
           // $('#input-modal-edit-data-idalloy').val(data.id);
            $('#input-modal-edit-data-code').prop('disabled', true);
            $('#input-modal-edit-data-code').val(data.code); 
            $('#input-modal-edit-data-simbol').prop('disabled', true);
            $('#input-modal-edit-data-simbol').val(data.simbolo); 
            $('#input-modal-edit-data-name').prop('disabled', true);
            $('#input-modal-edit-data-name').val(data.name);        
            $('#input-modal-edit-data-factor').val(data.factor);
            $('#input-modal-edit-data-factor').prop('disabled', true);
            $('#input-modal-edit-data-description').prop('disabled', true);
            $('#input-modal-edit-data-description').val(data.description);         
            $('#div-modal-edit-select-visible').hide();
            $('#div-modal-edit-select-enabled').hide();
            $('#modal-edit-default-select').hide();
            $('#btt-modal-edit-save').hide();                            
            $('#btt-modal-delete').show();                            
            
            $("#modal-edit-data").modal("show");                   
                    
       }
                 
});


/**
Mostrar modal add
 */
$('#btn-add').on({click:                
    function (event)
    {            
        event.preventDefault();  
      
        $('#input-modal-add-code').val('');
        $('#input-modal-add-simbol').val('');
        $('#input-modal-add-name').val('');
        $('#input-modal-add-factor').val('');
        $('#input-modal-add-description').val('');
        $('#modal-add-enabled-select').val(1);
        $('#modal-add-visible-select').val(1);
        $('#modal-add-default-select').val(0);
                    
        $("#modal-add-moneys").modal('show');
                                                                                                                                   
    }//function (event)
}); // 

/**
Función que ejecuta la acción de refresch del datatable
 */
function refreshDataTable()
{                 
       /* if ( $.fn.dataTable.isDataTable( '#tablemoneys' ) ) {
           
            $('#tablemoneys').DataTable().searching = false;
            $('#tablemoneys').DataTable().paging = false;
            $('#tablemoneys').DataTable().destroy();
        }       
        */
        researchData($('#input_find_moneys').val());                                        
}     


/**Ejecutar la acción de agregar */
$('#btt-modal-add').on({click:
    function (event)
    {     
        event.preventDefault(); 
                
        if($('#input-modal-add-code').val() === ''){               
            $("#error-msg").text("Insert money code.");                 
            $("#modal-msg").modal("show");                                     
        }   
        /*else
        if($('#input-modal-add-simbol').val() === ''){               
            $("#error-msg").text("Insert money simbol.");                 
            $("#modal-msg").modal("show");                                     
        }  */ 
        else  
        if($('#input-modal-add-name').val() === ''){
            $("#error-msg").text("Insert money name.");
            $("#modal-msg").modal("show");
        }
        else  
        if($('#input-modal-add-factor').val() === ''){
            $("#error-msg").text("Insert money factor.");
            $("#modal-msg").modal("show");
        }        
        /*else
        if($('#input-modal-add-description').val() === ''){
            $("#error-msg").text("Insert money description.");
            $("#modal-msg").modal("show");
        }*/
        else{
            //$("form").submit();  
            //$("#form-modal-add-countrie").submit();  
        
            $('#div-confirm-data-text').html('Add moneys?');
            $('#btt-modal-confirm').addClass('add');            
            $("#modal-confirm-data").modal('show');
        
        }
         
    }
}); //Fin $('#id-input-send-add-country').on({click:
 
$('#btt-modal-confirm').on({click:    
        function (event)
        {   
            
            event.preventDefault();  
                                       
            $("#modal-confirm-data").modal('hide');

            if($(this).hasClass("edit")){                     
                $('#btt-modal-confirm').removeClass('edit');
                updateData('edit');
            }
            else
            if($(this).hasClass("delete")){                   
                $('#btt-modal-confirm').removeClass('delete'); 
                updateData('delete');
            }
            else
            if($(this).hasClass("add")){
                $('#btt-modal-confirm').removeClass('add'); 
                //$("#form-modal-add-countrie").submit(); 
                addData();
            }
                        
        }
 });//$('#b


/**
Función que ejecuta la acción de búsqueda
 */
$('#btn-find-moneys').on({click:
     function (event)
     {
        event.preventDefault(); 
        
        refreshDataTable();            
                                           
       // researchcountries($('#input_find_categories').val());
     }
})

//Capturar el evento enter para la búsqueda
$("#input_find_moneys").keypress(function(e) {
    // event.preventDefault(); 
    
     var code = (e.keyCode ? e.keyCode : e.which);
     if(code==13){
        refreshDataTable();            
         return false;
     }
   
});

/**
 * Function que ejecuta el ajax de actualización de la data (update, delete)
 *  */ 
function updateData(action){
                

        var data = new Object();
                    
        var Obj_Params = new Object();
                  
        //var idbtn = $(this).prop('id');
        
        data.action      = action;                                                          
        data.id          = $('#input-modal-edit-data-idmoneys').val();    
        data.code        = $('#input-modal-edit-data-code').val();                
        data.simbolo     = $('#input-modal-edit-data-simbol').val();
        data.name        = $('#input-modal-edit-data-name').val();
        data.factor      = $('#input-modal-edit-data-factor').val();
        data.description = $('#input-modal-edit-data-description').val();
        data.enabled     = $('#modal-edit-enabled-select').val();
        data.visible     = $('#modal-edit-visible-select').val();  
        data.default     = $('#modal-edit-default-select').val();  
        
        //Obj_Params.msg = 'Operación exitosa';
        if(!(data.action == '')){
           
           EjecutarAjaxNew(data, moneyAction(), 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
        }
}//Fin updateDataCountry

/**
 * Función que permite agrgar nueva money
 */
function addData(){
                

                var data = new Object();
                            
                var Obj_Params = new Object();
                          
                //var idbtn = $(this).prop('id');
    
                data.action      = 'add';                                                                                                                      
                data.name        = $('#input-modal-add-name').val();
                data.factor      = $('#input-modal-add-factor').val();
                data.code        = $('#input-modal-add-code').val();
                data.simbolo     = $('#input-modal-add-simbol').val();
                data.description = $('#input-modal-add-description').val();
                data.enabled     = $('#modal-add-enabled-select').val();
                data.visible     = $('#modal-add-visible-select').val();  
                data.default     = $('#modal-edit-default-select').val();  

    
                //Obj_Params.msg = 'Operación exitosa';
                if(!(data.action == '')){
                   EjecutarAjaxNew(data, moneyAction(), 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
                }
}//Fin updateDataCountry


});//Fin $(function(){

