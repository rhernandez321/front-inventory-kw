
//Bloque JQuery
$(function(){
      
    
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });


    //Leer la variable de php $err_msg y verificar si tiene o no valor
   /*  
    if(!(@json($error_msg) == '')){                     
            $("#modal-msg").modal("show");
           
    }*/

     /** Realizar la primera recarga de data */
    researchData('');
    
    /**Buscar cities by country seleccionada */
    $('#select-countries').on({change: 
         function (event)
         {                            
            
             /*$('#tablecitiesbycountries').DataTable().paging = false;
             $('#tablecitiesbycountries').DataTable().searching = false;
             $('#tablecitiesbycountries').DataTable().destroy();*/
             researchData($(this).val());             
         }
    });//Fin $('#select_idioma').on({change: 
    
        
    /**Se ejecuta cuando se recibe una respuesta sactifactoria del ajax que actualiza */    
    function successUpdate(datos) {            
        if(datos['_ok'] == '0'){
            closeModal('.btn-close'); //Indicamos la clase de cerrar modal
            refreshDataTable()
        }
        
        showModalMessage(datos['_msg']); //(funciones_js.js)
    }

   
    /**
    Función que realiza la carga el datatable de la data */
    function researchData($datafind){

        //Botones update y delete
        var btn_html = "<button type='button' class='edit btn btn-outline-warning' data-toggle='modaledit' data-target='#modaledit'><span class='fa fa-wrench' aria-hidden='true'></span></button>"
        btn_html = btn_html + "&nbsp;&nbsp;<button type='button' class='delete btn btn-outline-danger' data-toggle='modaledit' data-target='#modaldelete'><span class='fa fa-trash' aria-hidden='true'></span></button>"
                
        var datatable = $('#tablecitiesbycountries').DataTable({
         
           // processing: true,
           // serverSide: true,
            //serverMethod: 'post',
            responsive: true,
            destroy: true,
            
            ajax: {url: citiesByCountriesResearch(),         
                    type: "post",
                    //dataSrc: "_data",
                    data: {"idcountry": $datafind},
                    dataSrc: function (data) {
                        console.log(data);
                        if (data['_ok'] == '0')
                        {                            
                            console.log(data["_data"]);
                            return data['_data'];
                        }else{
                            console.log(data["_msg"]);
                            showModalMessage(data['_msg']);                            
                            data['_data'] = [];
                            return data['_data']
                        }                           
                    }                                       
            },
            
            columns: [
                        {data:"id"},
                        {data:"code"},
                        {data:"name"},                        
                        {data:"postalcode"},
                        {
                            bSortable: false,
                            data:"enabled",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }
                        },
                        {   bSortable: false,
                            data:"visible",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }
                        },
                        {  
                            bSortable: false,
                            data: null, 
                            "defaultContent": btn_html                           
                        }
                        
                        
                       /*Concatena campos 
                       { 
                            "data": null,
                            "className": "button",
                            "defaultContent": '<button name="vcvcvc" id="' + {data: "name"} + '" value="id" OnClick="EditRow(this);"  class="btn btn-primary" data-toggle="modal" data-target="#myModal">Editar</button>'
                        }*/                                            
                        
            ],     
            columnDefs: [
                          { "visible": false, "targets": [0] },
                          
                        ],         
            select:true
        });

      //  editar("#tablecountries",datatable);
    }//Fin researchcountries
         

    /**
    Capturar la acción clip de los botones de la tabla countries
     */
    $('#tablecitiesbycountries').on('click', "[data-toggle='modaledit']", function(){
           
           table = $('#tablecitiesbycountries').DataTable();  
           var row = $(this).parents('tr')[0]; 
           var data = table.row( row ).data();
                     
           console.log( data);
                     
           if($(this).hasClass("edit")){            
                $('#input-modal-edit-data-idcity').val(data.id);
                $('#input-modal-edit-data-citycode').val(data.code);   
                $('#input-modal-edit-data-citycode').prop('disabled', false);                             
                $('#input-modal-edit-data-cityname').val(data.name);
                $('#input-modal-edit-data-cityname').prop('disabled', false);
                $('#input-modal-edit-data-postalcode').val(data.postalcode);
                $('#input-modal-edit-data-postalcode').prop('disabled', false);
                $('#modal-edit-enabled-select').val(data.enabled);
                $('#modal-edit-visible-select').val(data.visible);

                $('#div-modal-edit-select-visible').show();
                $('#div-modal-edit-select-enabled').show();
                $('#btt-modal-edit-save').show();                            
                $('#btt-modal-delete').hide();
                                
                //$('#modal-edit-data').modal({backdrop: 'static', keyboard: false})
            
                $("#modal-edit-data").modal("show");                                 
           }            
           else{
                                
                $('#input-modal-edit-data-idcity').val(data.id);
                $('#input-modal-edit-data-citycode').prop('disabled', true);
                $('#input-modal-edit-data-citycode').val(data.code); 
                $('#input-modal-edit-data-cityname').prop('disabled', true);
                $('#input-modal-edit-data-cityname').val(data.name);        
                $('#input-modal-edit-data-citypostalcode').prop('disabled', true);
                $('#input-modal-edit-data-citypostalcode').val(data.postalcode);         
                $('#div-modal-edit-select-visible').hide();
                $('#div-modal-edit-select-enabled').hide();
                $('#btt-modal-edit-save').hide();                            
                $('#btt-modal-delete').show();                            
                
                $("#modal-edit-data").modal("show");                   
                        
           }
                     
   });
  
   
    /**
    Mostrar modal add cities
     */
    $('#btn-add-cities').on({click:                
        function (event)
        {            
            event.preventDefault();            
            $('#input-modal-add-citiescode').val('');
            $('#input-modal-add-citiesname').val('');
            $('#input-modal-add-citiespostalcode').val('');
            
            if(!($('#select-countries').val() == -1)){
                $("#modal-add-cities").modal('show');
            }
                                                                                                                                       
        }//function (event)
    }); // 

    
    /**
    Función que ejecuta la acción de refresch del datatable
     */
    function refreshDataTable()
    {                 
           /* if ( $.fn.dataTable.isDataTable( '#tablecitiesbycountries' ) ) {
               
                $('#tablecitiesbycountries').DataTable().searching = false;
                $('#tablecitiesbycountries').DataTable().paging = false;
                $('#tablecitiesbycountries').DataTable().destroy();
            }
                    */               
            researchData($('#select-countries').val());         
    }     

    
    /**Ejecutar la acción de agregar cities */
    $('#btt-modal-add-cities').on({click:
        function (event)
        {     
        event.preventDefault();   
                        
        
        if($('#input-modal-add-citiescode').val() === ''){               
                $("#error-msg").text("Insert city code");                 
                $("#modal-msg").modal("show");                                     
        }   
        else  
        if($('#input-modal-add-citiesname').val() === ''){
            $("#error-msg").text("Insert city name");
            $("#modal-msg").modal("show");
        }
        else  
        if($('#input-modal-add-citiespostalcode').val() === ''){
            $("#error-msg").text("Insert postal code");
            $("#modal-msg").modal("show");
        } 
        else{
            //$("form").submit();  
            //$("#form-modal-add-countrie").submit();  
            $('#div-confirm-data-text').html('Add City?');
            $('#btt-modal-confirm').addClass('add');                        
            
            $("#modal-confirm-data").modal('show');
        }     
                                        
            
        }
    }); //Fin $('#id-input-send-add-country').on({click:

        
    $('#btt-modal-confirm').on({click:    
            function (event)
            {   
                
                event.preventDefault();  
                                               
                $("#modal-confirm-data").modal('hide');
                                
                if($(this).hasClass("edit")){                     
                    $('#btt-modal-confirm').removeClass('edit');
                    updateData('edit');
                }
                else
                if($(this).hasClass("delete")){                   
                    $('#btt-modal-confirm').removeClass('delete'); 
                    updateData('delete');
                }
                else
                if($(this).hasClass("add")){
                    $('#btt-modal-confirm').removeClass('add'); 
                    //$("#form-modal-add-countrie").submit(); 
                    
                    addData('add');
                }
                            
            }
     });//$('#btt-modal-confirm


    /**
     * Function que ejecuta el ajax de actualización de la data (update, delete)
     *  */ 
    function updateData(action){
                    

            var data = new Object();
                        
            var Obj_Params = new Object();
                      
            var idbtn = $(this).prop('id');
          
            data.action      = action;                                                          
            data.id          = $('#input-modal-edit-data-idcity').val();    
            data.code        = $('#input-modal-edit-data-citycode').val();                
            data.name        = $('#input-modal-edit-data-cityname').val();
            data.postalcode  = $('#input-modal-edit-data-postalcode').val();
            data.enabled     = $('#modal-edit-enabled-select').val();
            data.visible     = $('#modal-edit-visible-select').val();  
            
            //Obj_Params.msg = 'Operación exitosa';
            if(!(data.action == '')){
               //EjecutarAjaxNew(data, '{{ route("citiesbycountries.action") }}', 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
               EjecutarAjaxNew(data, citiesByCountriesAction(), 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
            }
    }//Fin updateDataCountry

    /**
     * Función que permite agrgar nueva city
     */
    function addData(action){
                    

                    var data = new Object();
                                
                    var Obj_Params = new Object();
                              
                    var idbtn = $(this).prop('id');
        
                    data.action      = action;                                                                              
                    data.idcountry   = $('#select-countries').val();  
                    data.code        = $('#input-modal-add-citiescode').val();                
                    data.name        = $('#input-modal-add-citiesname').val();
                    data.postalcode  = $('#input-modal-add-citiespostalcode').val();
                    data.enabled     = $('#modal-add-enabled-select').val();
                    data.visible     = $('#modal-add-visible-select').val();  

        
                    //Obj_Params.msg = 'Operación exitosa';
                    if(!(data.action == '')){
                       //EjecutarAjaxNew(data, '{{ route("citiesbycountries.action") }}', 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
                       EjecutarAjaxNew(data, citiesByCountriesAction(), 'post', Obj_Params, successUpdate);  //(funciones_js.js)                                   
                    }
            }//Fin updateDataCountry




});//Fin $(function(){

