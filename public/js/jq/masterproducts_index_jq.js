$(function(){
      
    
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });


     /** Realizar la primera recarga de data */
     //Buscar todos los productos habilitados
     //El primer parámetro corresponde acompany
     //Habilitados = 1; Deshabilitado = 0, all = todos
    researchData('', '1');

    
    /**Se ejecuta cuando se recibe una respuesta sactifactoria del ajax que actualiza */    
    function successUpdate(datos) {          
       if(datos['_ok'] == '0'){
            closeModal('.btn-close'); //Indicamos la clase de cerrar modal
            refreshDataTable()
        }
        showModalMessage(datos['_msg']); //(funciones_js.js)                
    }

   
    /**
    Función que realiza la carga del datatable con la data */
    function researchData($_data, $_enabled){
                
        //Botones update y delete
        var btn_html = "<button type='button' class='edit btn btn-outline-warning' data-toggle='modaledit' data-target='#modaledit'><span class='fa fa-wrench' aria-hidden='true'></span></button>"
        btn_html = btn_html + "&nbsp;&nbsp;<button type='button' class='delete btn btn-outline-danger' data-toggle='modaledit' data-target='#modaldelete'><span class='fa fa-trash' aria-hidden='true'></span></button>"
                
        var datatable = $('#tablemasterproducts').DataTable({
         
            processing: true,
            serverSide: true,
            //serverMethod: 'post',
            responsive: true,
            destroy: true,

            ajax: {url: masterproductsSearch(),         
                    type: "post",
                    //dataSrc: "_data",
                    data: {"data":$_data,
                           "enabled":$_enabled,
                          },                    
                    dataSrc: function (data) {
                        console.log(data);
                        if (data['_ok'] == '0')
                        {                            
                            console.log(data["_data"]);
                            return data['_data'];
                        }else{
                            console.log(data["_msg"]);
                            showModalMessage(data['_msg']);                            
                            data['_data'] = [];
                            return data['_data']
                        }                           
                    } 
            },             
            columns: [
                        {data:"id"},
                        {data:"code"},
                        {data:"name"},
                        {data:"description"},
                        {data: "provider_name"},
                        {data:"color"},                                                
                        {data:"presentation"},                                                
                        {data:"unit"},                                                                      
                        {data:"category"},                          
                        {data:"subcategory"},                          
                        {data:"review"},                          
                        {data:"qtv"},                          
                        {data:"cost"},                          
                        {data:"price"},                          
                        {data:"alloy"}, 
                        {data:"status_name"},                                                  
                        {
                            bSortable: false,
                            data:"enabled",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }
                        },
                        /*{   bSortable: false,
                            data:"visible",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }
                        },*/
                        {  
                            bSortable: false,
                            data: null, 
                            "defaultContent": btn_html                           
                        }/*,
                          //Para mostrar imagen
                        {
                               "render": function (data, type, JsonResultRow, meta) {
                               return '<img src="Content/'+JsonResultRow.ImageAddress+'">';
                               }
                        }
                        */
                                                
                       /*Concatena campos 
                       { 
                            "data": null,
                            "className": "button",
                            "defaultContent": '<button name="vcvcvc" id="' + {data: "name"} + '" value="id" OnClick="EditRow(this);"  class="btn btn-primary" data-toggle="modal" data-target="#myModal">Editar</button>'
                        }*/                                            
                        
            ],     
            columnDefs: [
                          { "visible": false, "targets": [0] },
                          
                        ],         
            select:true
        });

      //  editar("#tablecountries",datatable);
    }//Fin researchcountries
         

    /**
    Función que ejecuta la acción de búsqueda
     */
    $('#btn-search').on({click:
         function (event)
         {
            event.preventDefault(); 

            //alert("Hola");
            refreshDataTable();            
                                               
           // researchcountries($('#input_find_categories').val());
         }
    })

   //Capturar el evento enter para la búsqueda
    $("#input_find_products").keypress(function(e) {
       // event.preventDefault(); 
       
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            refreshDataTable();            
            return false;
        }
      
    });

    
    /**
    Función que ejecuta la acción de refresch del datatable
     */
    function refreshDataTable(_table)
    {       
     
            //El primer parámetro corresponde a company
            //Habilitados = 1; Deshabilitado = 0, all = todos
            researchData($('#input_find_products').val(), '1');                                      
    }     

       

})//Fin $(function(){
