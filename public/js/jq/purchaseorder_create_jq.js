

/**Calcular tax */
function taxCalculate(price){
    var neto = 0;
    var totalgral = 0;
    var tax = 0;
    var totaltax = 0;
    
   // alert("Neto: " + $('#spanneto').html() + price);
    neto      = parseFloat(parseFloat($('#spanneto').html()) + parseFloat(price)).toFixed(2);
    totalgral = parseFloat($('#spantotal').html());
    tax       = $('#select-option-taxes').val();

    totaltax  = parseFloat(((neto * tax) / 100)).toFixed(2);
    totalgral = parseFloat(neto + totaltax).toFixed(2);
     
    $('#spanneto').html(neto);
    $('#spantotal').html(totalgral);
    $('#spantax').html(totaltax);
}

/**
 * Obtener fecha actual
 */
function dateActual(){
    var formattedDate = new Date(); 
    var d = formattedDate.getDate(); 
    var m = formattedDate.getMonth(); m += 1; // javascript months are 0-11
    var y = formattedDate.getFullYear();
    
    if(d.toString.length == 1){
        d = '0' + d;
    }

    if(m.toString.length == 1){
        m = '0' + m;
    }

    var f = y + "/" + m + "/" + d;
    return f;
 }

           
$(function(){
      
    
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });



    //Leer la variable de php $err_msg y verificar si tiene o no valor
     
    /*if(!(@json($error_msg) == '')){                     
            $("#modal-msg").modal("show");
           
     }*/

     
     //Mostrar fecha
     $("#input-purchaseorder-date").val(dateActual());

     /** Realizar la primera recarga de data */
     searchProviders(1);
    
        
    /**Se ejecuta cuando se recibe una respuesta sactifactoria del ajax que actualiza */    
    function successUpdate(datos, obj) {                 
       
       if(datos['_ok'] == '0'){            
            if(obj.action == 'confirmed'){
                $('#' + obj.divshow).show();
                $('#' + obj.divhide).hide();
            }
            else
            if(obj.action == 'canceled'){
                $('#' + obj.divhide).hide();
                $('#' + obj.divaddproducts).hide();
                $('#' + obj.divmsg).show();
                $('#' + obj.selectProvider).prop('disabled', true);                              
                $('#' + obj.inputCostShipping).prop('disabled', true);    
                
                showModalMessage(datos['_msg']);
            }  
            else
            if(obj.action == 'edit'){
                showModalMessage(datos['_msg']); //(funciones_js.js)    
            }
            /*closeModal('.btn-close'); //Indicamos la clase de cerrar modal
            refreshDataTable()*/
        }
        else{
            showModalMessage(datos['_msg']); //(funciones_js.js)
        }        
    }

   
    /**
    Función que realiza la carga el datatable de la data */
    function researchData($datafind){
        
        //Botones update 
        var btn_html = "<button type='button' class='edit btn btn-outline-success' data-toggle='modaledit' data-target='#modaledit'><span class='fa fa-shopping-cart' aria-hidden='true'></span></button>"
                
        var datatable = $('#tableproducts').DataTable({
         
           // processing: true,
           // serverSide: true,
            //serverMethod: 'post',
            responsive: true,
            destroy: true,
            
            ajax: {url: purcharseorderProducts(),         
                    type: "post",
                    //dataSrc: "_data",
                    data: {"data": $datafind},                    
                    dataSrc: function (data) {
                        console.log(data);
                        if (data['_ok'] == '0')
                        {                            
                            console.log(data["_data"]);
                            return data['_data'];
                        }else{
                            console.log(data["_msg"]);
                            showModalMessage(data['_msg']);                            
                            data['_data'] = [];
                            return data['_data']
                        }                           
                    } 
            },
            
            columns: [
                        {data:"id"},
                        {data:"code"},
                        {data:"name"},                        
                        {data:"provider_name"},
                        {
                            bSortable: false,
                            data: null,                             
                            
                            render: function (data, type, row) {
                                        input = '<div>';
                                        input += '<input id="cant'+ data.id + '" class="form-control" type="number" size="5"  value="1"/>';                                        
                                        input += '</div>';                                        
                                        return input;
                            }  
                        },
                        {   bSortable: false,
                            data:"null",

                            bSortable: false,
                            data: null,                             
                        
                            render: function (data, type, row) {                                        
                                        input = '<div class="input-group" >';
                                        input += '<div class="input-group-prepend">';                                                                
                                        input += '<span class="input-group-text">' + data.moneytype + '</span>'
                                        input += '</div>';
                                        input += '<input  id="cost'+ data.id + '" class="form-control" type="number" step="0.01" size="5"  value="'+ data.cost  +'"/>';                                        
                                        input += '</div>'
                                                
                                        return input;
                            }  
                        },
                        {  
                            bSortable: false,
                            data: null, 
                            "defaultContent": btn_html
                            
                        },
                        
            ],     
            columnDefs: [
                          { "visible": false, "targets": [0] },
                          {
                            "width": "10%",
                            "targets": [4]
                          },                          
                          {
                            "width": "20%",
                            "targets": [5]
                          }
                        ],         
            select:true
        });

      //  editar("#tablecountries",datatable);
      
    }//Fin researchcountries
         

    /**
    Capturar la acción clip de los botones de la tabla
     */
    $('#tableproducts').on('click', "[data-toggle='modaledit']", function(){
           
           table = $('#tableproducts').DataTable();  
           var row = $(this).parents('tr')[0]; 
           var data = table.row( row ).data();
           var inputcant = '';          
           var inputcost = '';
           var row = null;
           var totalbyproducts = 0;
           var qtv = 0;
           var cost = 0;
           var factor = 1;
           var qtvunit = 0;
           
           console.log( data);   
           inputcant = 'cant' + data.id;  
           inputcost = 'cost' + data.id;

                    
           if(('' == $('#' + inputcant).val().trim()) || ('' == $('#' + inputcost).val().trim()) || ($('#' + inputcost).val() <= 0)){
               showModalMessage("Invalid operation."); //(funciones_js.js)
               return false;
           }
           
           totalbyproducts = parseFloat($('#' + inputcant).val()).toFixed(2) * parseFloat($('#' + inputcost).val()).toFixed(2);
           
                     
           /*btndelete =  '<button data-idproduct=' + data.id + ' data-price="' + $('#' + inputcost).val() + '" type="button" class="deletep btn btn-outline-danger" data-toggle="modaledit" data-target="#modaldelete">';
           btndelete += '<span class="float-right fa fa-trash" aria-hidden="true"><i></i></span>'        
           btndelete += '</button>';

           newrow =  '<tr id="' + data.id + '">';
           newrow += '<td>' + data.code + '</td>';
           newrow += '<td class="text-center">' + $('#' + inputcant).val() + '</td>';
           newrow += '<td>' + data.name + '</td>';
           newrow += '<td><span class="float-right">' + $('#' + inputcost).val() + '</span></td>';
           newrow += '<td><span class="float-right">' + totalbyproducts + '</span></td>';
           newrow += '<td>' + btndelete + '</td>';
           newrow += '</tr>';*/
           
           
           //$('#tablepurcharseorderlist').append(newrow);
           
           /*$('#rowneto').before(newrow); 
                      
           taxCalculate(totalbyproducts);*/
           row =  findProductPurcharseorderList(data.code);
           
           if(null == row){    
               //Add nuevo producto 
               
               purcharseOrderDetails('add', 
                                    -1, 
                                    data.id, 
                                    data.code, 
                                    data.name, 
                                    $('#' + inputcant).val(), 
                                    parseFloat($('#' + inputcost).val()).toFixed(2), 
                                    parseFloat(totalbyproducts).toFixed(2),                                     
                                    -1,
                                    factor,
                                    0);
           }
           else{ 
               
               qtv = parseFloat(row.find("td").eq(1).html());
               
               if((qtv == 0) && (parseFloat($('#' + inputcant).val()) < 0)){
                 showModalMessage("Invalid operation."); //(funciones_js.js)
               }               
               else{
                    if((parseFloat($('#' + inputcant).val()) < 0) && (Math.abs($('#' + inputcant).val())) > qtv){
                        showModalMessage("Invalid operation."); //(funciones_js.js)                                       
                    }
                    else
                    if($('#' + inputcost).val() < 0){
                        showModalMessage("Invalid operation."); //(funciones_js.js)                                       
                    }
                    else{
                         //qtv general               
                         qtv = qtv + parseFloat($('#' + inputcant).val());
                         //qtv solicitados
                         qtvunit = parseFloat($('#' + inputcant).val());

                         if(parseFloat($('#' + inputcant).val()) < 0){
                           factor = -1;
                         }
                         
                         cost = parseFloat($('#' + inputcost).val()).toFixed(2);
                                                  
                         purcharseOrderDetails('edit', 
                                                row.data('tr_idpodetail'),
                                                -1, 
                                                '', 
                                                '', 
                                                qtv,                                                
                                                cost, 
                                                totalbyproducts,                                                 
                                                row, 
                                                factor, 
                                                qtvunit);
                    }
             }
           }
           
   });
  
   
    /**
    Mostrar modal search products
     */
    $('#searchproducts_all').on({click:                    
        function (event)
        {            
            event.preventDefault();              
            $("#modal-search-products").modal('show');
            researchData('');
                                                                                                                                       
        }//function (event)
    }); // 
    
    $('#modal-btn-search-products').on({click:                
        function (event)
        {            
            event.preventDefault();  
           
            researchData($("#modal-input-find-products").val());
                                                                                                                                                               
        }//function (event)
    });

    //Capturar el evento enter para la búsqueda
    $("#modal-input-find-products").keypress(function(e) {
         //event.preventDefault(); 
        
         var code = (e.keyCode ? e.keyCode : e.which);
         if(code==13){
            researchData($("#modal-input-find-products").val());
            return false;
         }
       
    });
        
    /**Escoger tax*/
    $('#select-option-taxes').on({change: 
        function (event)
        {                                
            taxCalculate(0);
        }
   });//Fin $('#taxes').on({change: 

   /*
     Actualizar la tabla de productos seleccionados
    */
   //function updatetablepurcharseorderlist(code, qtv, cost){
    function findProductPurcharseorderList(code){    
       var qtvaux = 0;
       /*var totalcost = 0;
       var ok = false;
       var span = '';*/
       var row = null;
       

       $('#tablepurcharseorderlist tr').each(function () {
        
           if($(this).find("td").eq(0).html() == code){
            
               /*qtvaux = parseFloat($(this).find("td").eq(1).html());
               qtvaux = qtvaux + qtv;
              
               totalcost = qtvaux * cost;
              
               //$(this).find("td").eq(1).html(qtvaux);
               //$(this).find("td").eq(4).html(totalcost);

               span = '<span class="float-right">' + qtvaux + '</span>';
               $(this).find("td").eq(1).html(span);
              
               span = '<span class="float-right">' + totalcost + '</span>';
               $(this).find("td").eq(4).html(span);*/
               
               
               /*$(this).find("td").eq(1).text(qtvaux);
               $(this).find("td").eq(4).text(totalcost);*/
              
               
               row = $(this);
               
               return;
               
           }
       });

      
      return row;
   }

   //Para capturar el click de los botones delete creados dinámicamente
   $("#tablepurcharseorderlist").on("click", ".deletep", function(){
     var price = 0;
     var factor = -1;  
     var qtv = 0;
     
          
     if(parseFloat($(this).data('price')) < 0){
         price = 0;
     }
     else{
        price = parseFloat($(this).data('price')) * -1;
     }

     //$(this).closest('tr').remove();
     //taxCalculate(price);
  
     qtv = $(this).closest('tr').find("td:eq(1)").html();
      
     purcharseOrderDetails('delete', 
                           $(this).data('id_po_detail'), 
                           -1, 
                           '', 
                           '', 
                           qtv, 
                           price, 
                           0,                                                     
                           $(this).closest('tr'), 
                           factor,
                           0);
     
     //Funciona
     //$("#" + $(this).data('idproduct')).remove();
   });


   $('#btn-new-po').on({click:
    function (event)
    {     
        event.preventDefault(); 
        
        if($('#select-option-providers').val() == -1){
            showModalMessage("Select provider."); //(funciones_js.js)
        }
        else
        if((null === $('#input-purchaseorder-date').val()) || ('' == $('#input-purchaseorder-date').val().trim())){                    
            showModalMessage("Invalid date."); //(funciones_js.js)
        } 
        else{
            newPurcharseOrder();
            
            //Estas opciones son para modificar la cabecera del purcharse order
            /*editPurcharseOrder('edit');
            editPurcharseOrder('confirmed');
            editPurcharseOrder('canceled');*/
        }        
    }
   });


          
    
    $('#btt-modal-confirm').on({click:    
            function (event)
            {   
                
                event.preventDefault();  
                
                $("#modal-confirm-data").modal('hide');
    
                if($(this).hasClass("confirm")){                     
                    //$('#btt-modal-confirm').removeClass('confirm');
                    //updateData('edit');
                    editPurcharseOrder('confirmed');
                }
                else
                if($(this).hasClass("cancel")){   
                    
                   // $('#btt-modal-confirm').removeClass('cancel'); 
                    editPurcharseOrder('canceled');                    
                }
                else
                if($(this).hasClass("edit")){
                    //$('#btt-modal-confirm').removeClass('add'); 
                    //$("#form-modal-add-countrie").submit(); 
                    editPurcharseOrder('edit');
                }
                            
            }
     });//$('#b
     
    //Ejecutar acciones del botón confirm 
    $('#btn-confirm-po').on({click:
        function (event)        
        {                 
            event.preventDefault(); 

           /* if($('#select-option-providers').val() == -1){
                showModalMessage("Select provider."); //(funciones_js.js)
            }
            else*/
            if((null === $('#input-nro-purchaseorder').val()) || ('' == $('#input-nro-purchaseorder').val().trim())){                    
                showModalMessage("Invalid purcharse order number."); //(funciones_js.js)
            }
            else{
                $('#div-confirm-data-text').html('Confimr purchar order?'); 
                $('#btt-modal-confirm').removeClass('cancel');
                $('#btt-modal-confirm').removeClass('edit');
                $('#btt-modal-confirm').addClass('confirm');                          
                $("#modal-confirm-data").modal('show');
            }
            
        }
    });

    
    //Ejecutar acciones del botón update
    $('#btn-update-po').on({click:
        function (event)        
        {                 
            event.preventDefault(); 

           /* if($('#select-option-providers').val() == -1){
                showModalMessage("Select provider."); //(funciones_js.js)
            }
            else*/
            if((null === $('#input-nro-purchaseorder').val()) || ('' == $('#input-nro-purchaseorder').val().trim())){                    
                showModalMessage("Invalid purcharse order number."); //(funciones_js.js)
            }
            else{
                $('#div-confirm-data-text').html('Confimr purchar order?'); 
                $('#btt-modal-confirm').removeClass('cancel');
                $('#btt-modal-confirm').removeClass('confirm');
                $('#btt-modal-confirm').addClass('edit');                          
                $("#modal-confirm-data").modal('show');
            }
            
        }
    });

    //Ejecutar acciones del bóton cancel
    $('#btn-cancel-po').on({click:
        function (event)        
        {                 
            event.preventDefault(); 

            /*if($('#select-option-providers').val() == -1){
                showModalMessage("Select provider."); //(funciones_js.js)
            }
            else*/
            if((null === $('#input-nro-purchaseorder').val()) || ('' == $('#input-nro-purchaseorder').val().trim())){                    
                showModalMessage("Invalid purcharse order number."); //(funciones_js.js)
            }
            else{
                
                $('#div-confirm-data-text').html('Cancel purchar order?');
                $('#btt-modal-confirm').removeClass('confirm'); 
                $('#btt-modal-confirm').removeClass('edit');
                $('#btt-modal-confirm').addClass('cancel');                          
                $("#modal-confirm-data").modal('show');
            }
            
        }
    });

    /**
     * Cargar el select-options de providers
     */
    function loadProviders(datos, obj){
        
        if(datos['_ok'] == '0'){            
            
            let element = '#' + obj.selectoptions;
            $(element).empty();
            
            $(element).append($('<option>', {                    
                    value: obj.option,
                    text: obj.text,
                    selected: 'selected',
                    hidden: 'hidden'
            }));            

            for (item in datos._data)
            {                                                                                              
                $(element).append($('<option>', {
                    value: datos._data[item].id,
                    text: datos._data[item].name,                    
                 }));            
            }            
        }
        else
            showModalMessage(datos['_msg']); //(funciones_js.js)
    }//loadProviders

    /**
     * Buscar todos los providers segón el status
     * status: 0= Disabled / 1 = Enabled
     */
    function searchProviders(status){
        var data = new Object();
                                
        var Obj_Params = new Object();

        Obj_Params.selectoptions = "select-option-providers";
        Obj_Params.option = -1;
        Obj_Params.text = "Providers";
        
                              
        data.enabled     = status;
       
        

        EjecutarAjaxNew(data, purcharseorderProviders(), 'post', Obj_Params, loadProviders);  //(funciones_js.js)                    
    }

    /**
     * Confirmación de purcharse order creada. Sólo la cabecera
     */
    function newPurcharseOrderOk(datos, obj)
    {
        if(datos['_ok'] == '0'){
            $('#div-new-po').hide();
            $('#div-addproducts').show();
            $('#input-nro-purchaseorder').val(datos['_data']);
            
            $('#searchproducts_all').click(); 
        }
        else{
            showModalMessage(datos['_msg']); //(funciones_js.js)            
        }
        
    }


    /**
     * Generar una nueva orden de compra
     */
    function newPurcharseOrder(){

        var data = new Object();
                                
        var Obj_Params = null;

        data.action  = 'add';
        
        //data.idcompany       = $this->m_company;                        
        data.idprovider      = $('#select-option-providers').val();                        
        data.providername    = $('#select-option-providers option:selected').text();
        data.totalcost       = 0; //$('#spanneto').html();
        data.costofshipping  = $('#input-cost-of-shipping').val();
        data.tax             = $('#spantax').html();
        data.ptax            = $('#select-option-taxes').val(); 
        data.ttalgral        = 0; //$('#spantotal').html();
        data.idmoneytype     = $('#input-idmoney').val();
        data.factormoney     = $('#input-factormoney').val();                            
        data.canceled        = 0;
        data.confirmed       = 0;  
                            
        EjecutarAjaxNew(data, purcharseorderAction(), 'post', Obj_Params, newPurcharseOrderOk);  //(funciones_js.js)                    

    }

    
    function editPurcharseOrder(action){

        var data = new Object();
                                
        var Obj_Params = new Object();

      
                              
        //data.action    = 'edit';
        //data.subaction = subaction;
        data.action    = action;
        data.id = $('#input-nro-purchaseorder').val();

        if(action == 'confirmed'){           
            data.confirmed  = 1; 
            data.costofshipping = $('#input-cost-of-shipping').val();
            data.totalcost      = $('#spanneto').html();
            data.tax            = $('#spantax').html();
            data.ptax           = $('#select-option-taxes').val();
            
            Obj_Params.action = 'confirmed'; 
            Obj_Params.divshow = 'div-btn-update-po';
            Obj_Params.divhide = 'div-btn-confirm-po';
        }
        else
        if(action == 'canceled'){
            
            Obj_Params.action            = 'canceled'; 
            Obj_Params.divhide           = 'div-btn-update-po';
            Obj_Params.divmsg            = 'div-msg-canceled';
            Obj_Params.selectProvider    = 'select-option-providers';                              
            Obj_Params.inputCostShipping = 'input-cost-of-shipping';  
            Obj_Params.divaddproducts   = 'div-addproducts';
            data.canceled  = 1;   
        }
        else
        if(action == 'edit'){
            Obj_Params.action    = 'edit'; 
            //data.idcompany       = $this->m_company;                        
            data.idprovider      = $('#select-option-providers').val();                        
            data.providername    = $('#select-option-providers option:selected').text();
            data.totalcost        = $('#spanneto').html();
            data.costofshipping  = $('#input-cost-of-shipping').val();
            data.tax             = $('#spantax').html();
            data.ptax           = $('#select-option-taxes').val();
            //data.ttalgral        = $('#spantotal').html();
            //data.idmoneytype     = $('#input-idmoney').val();
            //data.factormoney     = $('#input-factormoney').val();                  
        }
                            
        EjecutarAjaxNew(data, purcharseorderAction(), 'post', Obj_Params, successUpdate);  //(funciones_js.js)                    

    }

    //Recibe respuesta del ajax y muetra el producto agregado o modificado
    function successPurcharseOrderDetails(datos, obj){
        var idindex = -1;
        var btndelete = '';
        var newrow = '';
        var row = null;
        var qtv = 0;
        var span = "";

        if(datos['_ok'] == '0'){
                
                if(obj.action == 'add'){
                
                    idindex = datos['_data'];
                    
                    btndelete =  '<button data-id_po_detail="' + idindex + '" data-price="' + obj.unitcost + '" type="button" class="deletep btn btn-outline-danger" data-toggle="modaledit" data-target="#modaldelete">';
                    btndelete += '<span class="float-right fa fa-trash" aria-hidden="true"><i></i></span>'        
                    btndelete += '</button>';

                    console.log(btndelete);

                    newrow =  '<tr data-tr_idpodetail="' + idindex + '" id="' + obj.idproduct + '">';
                    newrow += '<td>' + obj.productcode + '</td>';
                    newrow += '<td class="text-center">' + obj.qtv + '</td>';
                    newrow += '<td>' + obj.productname + '</td>';
                    newrow += '<td><span id="spanunitcost' + idindex + '" class="float-right">' + obj.unitcost + '</span></td>';
                    newrow += '<td><span id="spantotalbyproduct' + idindex + '" class="float-right">' + obj.totalbyproducts + '</span></td>';
                    newrow += '<td>' + btndelete + '</td>';
                    newrow += '</tr>';
                    
                    $('#rowneto').before(newrow); 
                    taxCalculate(obj.totalbyproducts);
                }
                else                
                if(obj.action == 'edit'){
                    
                    qtv = parseFloat(obj.qtv);
                                
                    obj.row.find("td").eq(1).html(qtv);

                    span = '<span id="spanunitcost' + obj.row.data('tr_idpodetail') + '" class="float-right">' + obj.unitcost + '</span>';
                    obj.row.find("td").eq(3).html(span);
                                   
                    totalcost = parseFloat(qtv * parseFloat(obj.unitcost)).toFixed(2);
                    
                    span = '<span id="spantotalbyproduct' + obj.row.data('tr_idpodetail') + '" class="float-right">' + totalcost + '</span>';
                   
                    obj.row.find("td").eq(4).html(span);
                    
                    //taxCalculate(obj.unitcost * obj.factor);
                    taxCalculate((obj.unitcost * obj.qtvunit));

                }
                else
                if(obj.action == 'delete'){                             
                    taxCalculate(obj.unitcost * obj.qtv);
                    obj.row.remove();                    
                }
        }
        else{
            showModalMessage(datos['_msg']); //(funciones_js.js)
        }        
    }

    /**
     * Función que permite agregar, modificar y eliminar productos al purcharse order
     * action: Recibe la acción a ejecutar
     * id: index del item a modificar
     * idproduct: Id del producto a agregar o modificar
     * productcode: Código del producto
     * productname: Nombre del producto
     * unitcost: Costo por unidad
     * qtv: Cantidad general
     * totalbyproducts: Monto total de prodcuto por cantidad
     * totalbyproducts, Nueva fila a agregar
     * row: fila a modificar visualmente
     * factor: Multiplicar por 1 ó -1 para agregar o sustraer
     * qtvunit: Cantidad solicitada
     */    
    
    function purcharseOrderDetails(action, id, idproduct, productcode, productname, qtv, unitcost, totalbyproducts, row, factor, qtvunit){

        var data = new Object();
                                
        var Obj_Params = new Object();
        var ptax = 0;
        var costofshipping = 0;
                
        data.action           = action;
        Obj_Params.action          = action;               
             
        data.idpurcharseorder = $('#input-nro-purchaseorder').val();

       
        costofshipping = $('#input-cost-of-shipping').val()
        ptax           = $('#select-option-taxes').val();

       
        if((null === costofshipping) || ("" === costofshipping))  
        {
            costofshipping = 0;
        }

        //tax = parseFloat(tax).toFixed(2);
        costofshipping = parseFloat(costofshipping).toFixed(2);


        if(action == 'add'){ 

            data.idproduct        = idproduct,            
            data.productcode      = productcode,
            data.productname      = productname,
            data.unitcost         = unitcost,
            data.qtv              = qtv,
            data.totalcost        = totalbyproducts,
            
            data.costofshipping   = costofshipping,
            data.ptax             = ptax,
                
            Obj_Params.idproduct       = idproduct;
            Obj_Params.productcode     = productcode; 
            Obj_Params.productname     = productname; 
            Obj_Params.qtv             = qtv; 
            Obj_Params.qtvunit   = qtvunit;
            Obj_Params.unitcost        = unitcost;
            Obj_Params.totalbyproducts = totalbyproducts;

        }
        else
        if(action == 'edit'){
            
            data.id         = id;
            data.qtv        = qtv;
            data.qtvunit    = qtvunit;
            data.unitcost   = unitcost;
            data.totalcost  = unitcost * qtv;
            data.ptax       = ptax;

            Obj_Params.row       = row;
            Obj_Params.qtv       = qtv;
            Obj_Params.qtvunit   = qtvunit;
            Obj_Params.unitcost  = unitcost;
            Obj_Params.totalcost = totalbyproducts;
            Obj_Params.factor    = factor;
            //console.log("id: " + data.id + " qtv: " + data.qtv + ' totalcost: ' + data.totalcost)
        }
        else
        if(action == 'delete')
        {                  
            alert("aquí: " + factor);
            data.id               = id;
            data.idpurchaseorder  = $('#input-nro-purchaseorder').val();
            data.costofshipping   = costofshipping;
            data.ptax             = ptax;
            data.qtv              = qtv;
            data.totalcost        = unitcost;
            data.factor           = factor
           // data.totalcost      = $('#spanneto').html();

            Obj_Params.qtv       = qtv;  
            Obj_Params.unitcost = unitcost;          
            data.ptax             = ptax;
            Obj_Params.row      = row;
        }
                            
        EjecutarAjaxNew(data, purcharseorderActionDetails(), 'post', Obj_Params, successPurcharseOrderDetails);  //(funciones_js.js)                    

    }
    //purcharseorderActionDetails()


})//Fin $(function(){
