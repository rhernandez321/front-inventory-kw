$(function(){
      
    
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

        
    //$(".custom-file-input").on("change", function() {    
    //Bloque para visualizar imagen        
    $("#customFileLang").on("change", function(e) {
         previewImage(e);          
    }); 
    
    function previewImage(e){        
        
        var file = e.target.files[0],
        
        imageType = /image.*/;
      
        if (!file.type.match(imageType))
         return;
    
        var reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
    }

    function fileOnload(e) {
        var result=e.target.result;
        $('#previewImg').attr("src",result);
    }

    //Fin bloque para visualizar imagen
})//Fin $(function(){