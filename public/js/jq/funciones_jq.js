
$(function(){
    //Convertir a mayúscula las letras pulsadas.
    $(".upper").keyup(function(){       
        $(this).val($(this).val().toUpperCase());
    });

    
    $('.solonro').keydown(function(event) {
         
        // Desactivamos cualquier combinación con shift
        if(event.shiftKey){
            event.preventDefault();
        }
         // Solo Numeros del 0 a 9 
       if (event.keyCode < 48 || event.keyCode > 57)
           //Solo Teclado Numerico 0 a 9
           if (event.keyCode < 96 || event.keyCode > 105)
               /*  
                   No permite ingresar pulsaciones a menos que sean los siguietes
                   KeyCode Permitidos
                   keycode 8 Retroceso
                   keycode 37 Flecha Derecha
                   keycode 39  Flecha Izquierda
                   keycode 46 Suprimir
               */
               if(event.keyCode != 46 && event.keyCode != 8 && event.keyCode != 9 && event.keyCode != 37 && event.keyCode != 39)
                   event.preventDefault();
    });



   /**Update data datatable */    
   /**Ejecurtar las acciones de los botones edit y delte del datatable */
   $('#btt-modal-edit-save, #btt-modal-delete').on({click:
        function (event)
        {     
        event.preventDefault();

        var idbtn = $(this).prop('id');

        if(idbtn == 'btt-modal-edit-save'){                 
            //$('#btt-modal-confirm').removeClass('delete');
            //if(validate_edit_data()){

                
                $('#btt-modal-confirm').addClass('edit');  
                $('#div-confirm-data-text').html('Confirm Update?');        
                
                $("#modal-confirm-data").modal("show");
            //}
        }
        else
        if(idbtn == 'btt-modal-delete'){
            //$('#btt-modal-confirm').removeClass('edit');                      
            $('#btt-modal-confirm').addClass('delete');          
            $('#div-confirm-data-text').html('Confirm Delete Data?');
            $("#modal-confirm-data").modal("show");
        }

        }
    });//$('#btt-modal-edit-save').on({click:


});//Fin function

