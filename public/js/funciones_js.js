
    /**Mostrar mensaje de operación*/    
    function showModalMessage(msg){     
        $("#error-msg").text(msg);                 
        $("#modal-msg").modal("show");
    }

    /**Cerrar la ventana modal asociada al botón btn */
    function closeModal(obj) {           
        $(obj).click();//Llamada al evento click del botón close de la modal, por lo que no se debe preocupar por qué clases agregar o qué clases sacar.
        $('.modal-backdrop').remove();//eliminamos el backdrop del modal
    }

 


    //****** Bloque de ejecución de la función ajax ********* 
   // Función que permite ejecutar peticiones ajax.
   // Autor: Arístides Cortesía
   // Fecha: Lunes 19/11/2018
   // Parámetros:
   // _data   : Recibe un objeto javascript con los datos a enviar.
   // _url    : Recibe la url a llamar.
   // _type   : Recibe el tipo de petición a realizar.
   // _objs    : Recibe los parámetros de configuración de la petición ajax así
   //         : como todos aquellos objetos que se modificarân una vez que el ajax filnalice.
   // callback: Recibe la funcón que se ejecutará una vez que el ajax recibe respuesta del servidor.
   function EjecutarAjaxNew(_data, _url, _type, objs, callback){             
       var datosjson;
       var err_msg;
                               
       if (_data != 0 || _data != "") {                
           datosjson = JSON.stringify(_data);                
       } else {
           datosjson = "";                
       }
                   
       $.ajax({                   
               url: _url,
               data: datosjson,                    
               type: _type,
               dataType: "json",
               contentType: "application/json; charset=utf-8",                    
              /* beforeSend: function (){
                   //$('section').append('Entra 1 ');
                   mostrar('div_msg', "Enviando data", true, "alert alert-info", true);
               }*/
           }).done(function(datos){     
                          
              callback(datos, objs);   
               
           }).fail( function( jqXHR, textStatus, errorThrown ) {
               
               if (jqXHR.status === 0) {                                                
                   err_msg = 'Not connect: Verify Network.';
               } else if (jqXHR.status == 404) {                                                    
                       err_msg = 'Requested page not found [404]';
                       
               } else if (jqXHR.status == 500) {                                        
                       err_msg = 'Internal Server Error [500].';
               } else if (textStatus === 'parsererror') {                                        
                       err_msg = 'Requested JSON parse failed.';
               } else if (textStatus === 'timeout') {                                        
                       err_msg = 'Time out error.';
               } else if (textStatus === 'abort') {                                        
                      err_msg = 'abort.';
                       
               } else {                                               
                      err_msg = 'Uncaught Error: ' + jqXHR.responseText;                           
               }
               showModalMessage(err_msg)                                                                  
           });
        
   }//Fin EjecutarAjaxNew
 /////////////////////////////////
