(function() {
    'use strict';
    
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        //form.addEventListener('submit', function(event) { //Original
          //Modificado por Arístides para que pueda escuchar botones con acciones ajax  
        var btn = document.querySelector('.btn-send');
        
        btn.addEventListener('click', function(event) {
          
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
  