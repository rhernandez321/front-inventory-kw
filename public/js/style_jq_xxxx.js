$(function(){
      
    
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });


    researchcountries('');

    
    /**Cerrar modal. Se cerrará la que corresponda a la orden del botón indicado. */
    function closeModal(btn) {
        $('#' + btn).click(); //Esto simula un click sobre el botón close de la modal, por lo que no se debe preocupar por qué clases agregar o qué clases sacar.
        $('.modal-backdrop').remove();//eliminamos el backdrop del modal
    }

    /**Se ejecuta cuando se recibe una respuesta sactifactoria del ajax que actualiza */
    function successUpdate() {        
        closeModal('btt-modal-edit-close');         
        showModalMessage("successful operation"); //(funciones_js.js)
        $('#btn-find-countries').click();        
    }

    /**Mostrar mensaje de operación*/    
  /*  function showModalMessage(msg){
        $("#error-msg").text(msg);                 
        $("#modal-msg").modal("show");
    }  
*/
    /**
    Función que realiza la carga el datatable coutries */
    function researchcountries($datafind){
        
        var datatable = $('#tablecountries').DataTable({
          //var table = $('#tablecountries').DataTable({
       
            //processing: true,
            //serverSide: true,
            //serverMethod: 'post',
            
            ajax: {url: "{{route('countries_research')}}",         
                    type: "post",
                    dataSrc: "_data",
                    data: {"find_country": $datafind}
                   
            },
                    
            columns: [
                        {data:"id"},
                        {data:"countrycode"},
                        {data:"name"},
                        {
                            bSortable: false,
                            data:"enabled",
                            
                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                        }
                            }                            
                        },
                        {   bSortable: false,
                            data:"visible",

                            render: function(data, type, row) {
                                            switch(data) {
                                            case 0 : return "No"; break;
                                            case 1 : return "Yes"; break;
                                            //default : return "N/A"; break;
                                            }
                            }
                        },
                        {  
                            bSortable: false,
                            data: null, 
                            "defaultContent":"<button type='button' class='edit btn btn-outline-warning' data-toggle='modaledit' data-target='#modaledit'><span class='fa fa-wrench' aria-hidden='true'></span></button>"
                        },
                        {
                            bSortable: false,
                            data: null, 
                            "defaultContent":"<button type='button' class='delete btn btn-outline-danger' data-toggle='modaledit' data-target='#modaldelete'><span class='fa fa-trash' aria-hidden='true'></span></button>"
                        }
                        
                        
                       /*Concatena campos 
                       { 
                            "data": null,
                            "className": "button",
                            "defaultContent": '<button name="vcvcvc" id="' + {data: "name"} + '" value="id" OnClick="EditRow(this);"  class="btn btn-primary" data-toggle="modal" data-target="#myModal">Editar</button>'
                        }*/                                            
                        
            ],     
            columnDefs: [
                          { "visible": false, "targets": [0] },                          
                        ],         
            select:true
        });

      //  editar("#tablecountries",datatable);
    }//Fin researchcountries

    
     //Leer la variable de php $err_msg y verificar si tiene o no valor
     
  /*   if(!(json($error_msg) == '')){                     
            $("#modal-msg").modal("show");
                        
            /* $("#error-msg").modal('show', {                    
                 backdrop: 'static',   // This disable for click outside event
                 keyboard: true       // This for keyboard event
              })*-/
     }})
*/

/**
    Capturar la acción clip de los botones de la tabla countries
     */
    $('#tablecountries').on('click', "[data-toggle='modaledit']", function(){
           
           table = $('#tablecountries').DataTable();  
           var row = $(this).parents('tr')[0]; 
           var data = table.row( row ).data();
                     
           console.log( data);
                     
           if($(this).hasClass("edit")){
                
                $('#input-modal-edit-data-idcountry').val(data.id); 
                $('#input-modal-edit-data-countrycode').val(data.countrycode);   
                $('#input-modal-edit-data-countrycode').prop('disabled', false);                             
                $('#input-modal-edit-data-countryname').val(data.name);
                $('#input-modal-edit-data-countryname').prop('disabled', false);

                $('#modal-edit-enabled-select').val(data.enabled);
                $('#modal-edit-visible-select').val(data.visible);

                $('#div-modal-edit-select-visible').show();
                $('#div-modal-edit-select-enabled').show();
                $('#btt-modal-edit-save').show();                            
                $('#btt-modal-delete-country').hide();
                                
                //$('#modal-edit-data').modal({backdrop: 'static', keyboard: false})
            
                $("#modal-edit-data").modal("show");                                 
           }            
           else{
                                
                $('#input-modal-edit-data-idcountry').val(data.id);
                $('#input-modal-edit-data-countrycode').prop('disabled', true);
                $('#input-modal-edit-data-countrycode').val(data.countrycode); 
                $('#input-modal-edit-data-countryname').prop('disabled', true);
                $('#input-modal-edit-data-countryname').val(data.name);                
                $('#div-modal-edit-select-visible').hide();
                $('#div-modal-edit-select-enabled').hide();
                $('#btt-modal-edit-save').hide();                            
                $('#btt-modal-delete-country').show();                            
                
                $("#modal-edit-data").modal("show");                   
                        
           }
                     
   });
  
   
    /**
    Ocultar o mostrar div add-countries
     */
    $('#btn-add-country').on({click:                
        function (event)
        {            
            event.preventDefault(); 
           
            $('#input-modal-add-countrycode').val('');
            $('#input-modal-add-countryname').val('');
            $("#modal-add-countries").modal('show');
                                                                                                                                       
        }//function (event)
    }); // 

    
    /**
    Función que ejecuta la acción de búsqueda de countries cuando el usuario indica la data a buscar
     */
    $('#btn-find-countries').on({click:
         function (event)
         {
            event.preventDefault(); 
            
            if ( $.fn.dataTable.isDataTable( '#tablecountries' ) ) {
               
                $('#tablecountries').DataTable().paging = false;
                $('#tablecountries').DataTable().searching = false;
                $('#tablecountries').DataTable().destroy();
            }
                       
            researchcountries($('#input_find_country').val());
         }
    })     

    
/////////////////////////////////////////
    /**Ejecutar la acción de agregar countries */
    $('#btt-modal-add-countries').on({click:
        function (event)
        {     
        event.preventDefault();          
        
        if($('#input-modal-add-countrycode').val() === ''){       
        
                $("#error-msg").text("Insert country code");                 
                $("#modal-msg").modal("show");                                     
        }   
        else  
        if($('#input-modal-add-countryname').val() === ''){
            $("#error-msg").text("Insert country name");
            $("#modal-msg").modal("show");
        } 
        else{
            //$("form").submit();  
            //$("#form-modal-add-countrie").submit();  
            $('#div-confirm-data-text').html('Add Country?');
            $('#btt-modal-confirm').addClass('add-country');
            $("#modal-confirm-data").modal('show');
        }     
                                        
            
        }
    }); //Fin $('#id-input-send-add-country').on({click:
//////////////////////////////////////////    
 
    $('#btt-modal-confirm').on({click:    
            function (event)
            {   
                event.preventDefault();  
               
                $("#modal-confirm-data").modal('hide');

                if($(this).hasClass("edit-country")){                     
                    $('#btt-modal-confirm').removeClass('edit-country');
                    updateDataCountries('edit');
                }
                else
                if($(this).hasClass("delete-country")){                   
                    $('#btt-modal-confirm').removeClass('delete-country'); 
                    updateDataCountries('delete');
                }
                else
                if($(this).hasClass("add-country")){
                    $('#btt-modal-confirm').removeClass('add-country'); 
                    $("#form-modal-add-countrie").submit(); 
                }
                            
            }
     });//$('#b


    /**Update data country */    
    $('#btt-modal-edit-save, #btt-modal-delete-country').on({click:
         function (event)
         {     
            event.preventDefault();

            var idbtn = $(this).prop('id');

            if(idbtn == 'btt-modal-edit-save'){
                //$('#btt-modal-confirm').removeClass('delete');
                $('#btt-modal-confirm').addClass('edit-country');  
                $('#div-confirm-data-text').html('Confirm Update Data?');        

                $("#modal-confirm-data").modal("show");
            }
            else
            if(idbtn == 'btt-modal-delete-country'){
                //$('#btt-modal-confirm').removeClass('edit');
                $('#btt-modal-confirm').addClass('delete-country');          
                $('#div-confirm-data-text').html('Confirm Delete Data?');
                $("#modal-confirm-data").modal("show");
            }

         }
    });//$('#btt-modal-edit-save').on({click:

    function updateDataCountries(action){
                    

        var data = new Object();
                    
        var Obj_Params = new Object();
                  
        var idbtn = $(this).prop('id');

                   
        data.action  = action;                                                          
        data.id          = $('#input-modal-edit-data-idcountry').val();    
        data.countrycode = $('#input-modal-edit-data-countrycode').val();                
        data.name        = $('#input-modal-edit-data-countryname').val();
        data.enabled     = $('#modal-edit-enabled-select').val();
        data.visible     = $('#modal-edit-visible-select').val();  

        Obj_Params.msg = 'Operación exitosa';
        if(!(data.action == '')){
           EjecutarAjaxNew(data, '{{ route("countries.action") }}', 'post', Obj_Params, successUpdate);  //(funciones_js.js)            
        }
    }//Fin updateDataCountry
});
