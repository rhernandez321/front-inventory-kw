<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcategorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategorys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 30);
            $table->string('name', 30);
            $table->string('description', 150)->nullable();
            $table->boolean('enabled')->default(0);
            $table->boolean('visible')->default(0);
            $table->bigInteger('id_user')->unsigned();
            $table->bigInteger('id_category')->unsigned();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP(0)'));
            $table->unique(array('id_category', 'code'));

            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('id_category')->references('id')->on('categorys')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcategorys');
    }
}
