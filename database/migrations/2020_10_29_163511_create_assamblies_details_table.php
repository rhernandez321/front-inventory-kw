<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssambliesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assamblies_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 30);                        
            $table->bigInteger('id_assembly')->unsigned(); 
            $table->bigInteger('id_product')->unsigned();
            $table->bigInteger('id_provider')->unsigned();
            $table->bigInteger('id_color')->unsigned();  
            $table->bigInteger('id_presentation')->unsigned();
            $table->bigInteger('id_wharehouse')->unsigned();  
            $table->bigInteger('id_category')->unsigned()->nullable();
            $table->bigInteger('id_subcategory')->unsigned()->nullable();
            $table->bigInteger('id_review')->unsigned()->nullable();   
            $table->bigInteger('id_status')->unsigned()->nullable();   
            $table->bigInteger('id_alloy')->unsigned()->nullable();   
            $table->bigInteger('id_location')->unsigned();
            $table->bigInteger('id_unittype')->unsigned();
            $table->bigInteger('id_moneytype')->unsigned();
            $table->decimal('qtv', 18, 3)->default(0);            
            $table->decimal('cost', 18, 3)->default(0);            
            $table->decimal('price', 18, 3)->default(0);            
            $table->decimal('factormoney', 18, 3)->default(0);                        
                        
            $table->boolean('removed')->default(0); //Indica si el producto fue removido.
            
            $table->bigInteger('id_user')->unsigned();
            $table->bigInteger('id_user_removed')->unsigned()->nullabled();
            
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP(0)'));               
                     
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade');
            //Para guardar el usuario que removió el producto
            $table->foreign('id_user_removed')->references('id')->on('users')->onUpdate('cascade');

            $table->foreign('id_product')->references('id')->on('master_products')->onUpdate('cascade');            
            $table->foreign('id_provider')->references('id')->on('providers')->onUpdate('cascade');            
            $table->foreign('id_color')->references('id')->on('colors')->onUpdate('cascade');
            $table->foreign('id_presentation')->references('id')->on('presentations')->onUpdate('cascade');
            $table->foreign('id_wharehouse')->references('id')->on('wharehouses')->onUpdate('cascade');
            $table->foreign('id_category')->references('id')->on('categorys')->onUpdate('cascade');
            $table->foreign('id_subcategory')->references('id')->on('subcategorys')->onUpdate('cascade');
            $table->foreign('id_review')->references('id')->on('reviews')->onUpdate('cascade');
            $table->foreign('id_moneytype')->references('id')->on('moneys')->onUpdate('cascade');
            $table->foreign('id_status')->references('id')->on('status')->onUpdate('cascade');
            $table->foreign('id_alloy')->references('id')->on('alloys')->onUpdate('cascade');
            $table->foreign('id_unittype')->references('id')->on('unittypes')->onUpdate('cascade');            
            $table->foreign('id_location')->references('id')->on('locationbywharehouses')->onUpdate('cascade');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assamblies_details');
    }
}
