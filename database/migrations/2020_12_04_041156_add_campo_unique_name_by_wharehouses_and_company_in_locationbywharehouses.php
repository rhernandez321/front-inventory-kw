<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampoUniqueNameByWharehousesAndCompanyInLocationbywharehouses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locationbywharehouses', function (Blueprint $table) {
            //
            $table->unique(['name', 'id_company', 'id_wharehouse'], 'locationwhco_name_idcompany_idwharehouses_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locationbywharehouses', function (Blueprint $table) {
            //
        });
    }
}
