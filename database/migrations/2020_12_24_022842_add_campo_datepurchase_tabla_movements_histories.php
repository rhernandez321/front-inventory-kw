<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampoDatepurchaseTablaMovementsHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movements_histories', function (Blueprint $table) {
            //
            $table->datetime('datepurchase')->nullable()->after('nro_bill');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movements_histories', function (Blueprint $table) {
            //
            $table->dropColumn('datepurchase');
        });
    }
}
