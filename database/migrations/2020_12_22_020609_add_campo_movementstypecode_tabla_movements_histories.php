<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampoMovementstypecodeTablaMovementsHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movements_histories', function (Blueprint $table) {
            //
            $table->string('movementstypescode', 30)->after('id_movementtype');
                                  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movements_histories', function (Blueprint $table) {
            //
            $table->dropColumn('movementstypescode');            
        });
    }
}
