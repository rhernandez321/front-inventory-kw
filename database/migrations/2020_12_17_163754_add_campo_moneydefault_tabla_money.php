<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampoMoneydefaultTablaMoney extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('moneys', function (Blueprint $table) {
            //
            $table->boolean('mdefault')->after('visible')->default(0);                        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('moneys', function (Blueprint $table) {
            //
            $table->dropColumn('mdefault');
        });
    }
}
