<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignkeyTableMasterproducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_products', function (Blueprint $table) {
           
            //idproviders
            $table->foreign('idprov')->references('id')->on('providers')->onUpdate('cascade');            
            //idcolors
            $table->foreign('idcol')->references('id')->on('colors')->onUpdate('cascade');            
            //idpresentation
            $table->foreign('idpre')->references('id')->on('presentations')->onUpdate('cascade');            
            //idwharehouse
            $table->foreign('idwh')->references('id')->on('wharehouses')->onUpdate('cascade');            
            //idcategories
            $table->foreign('idcat')->references('id')->on('categorys')->onUpdate('cascade');            
            //idsubcategories
            $table->foreign('idscat')->references('id')->on('subcategorys')->onUpdate('cascade');            
            //idreviews
            $table->foreign('idrv')->references('id')->on('reviews')->onUpdate('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_products', function (Blueprint $table) {
            //
        });
    }
}
