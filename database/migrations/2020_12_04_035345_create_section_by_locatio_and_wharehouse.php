<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionByLocatioAndWharehouse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {           
            $table->bigIncrements('id');
            $table->string('code', 30)->unique();
            $table->string('name', 30);            
            $table->string('description', 250)->nullable();                                                   
            
            $table->bigInteger('id_user')->unsigned();
            $table->bigInteger('id_location')->unsigned();
            $table->bigInteger('id_wharehouse')->unsigned();
            $table->boolean('enabled')->default(0);
            $table->boolean('visible')->default(0);
                        
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP(0)'));

            $table->unique(array('name', 'id_location', 'id_wharehouse'));
           
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('id_wharehouse')->references('id')->on('wharehouses')->onUpdate('cascade');
            $table->foreign('id_location')->references('id')->on('locationbywharehouses')->onUpdate('cascade');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section');
    }
}
