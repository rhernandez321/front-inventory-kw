<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampoNameTableLocationbywharehouse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locationbywharehouses', function (Blueprint $table) {
            //
            $table->string('name', 50)->after('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locationbywharehouses', function (Blueprint $table) {
            //           
            $table->dropColumn('name');
        });
    }
}
