<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIduserTableRolls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rolls', function (Blueprint $table) {
            //
            $table->bigInteger('id_user')->unsigned();                        
                                  
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rolls', function (Blueprint $table) {
            //
        });
    }
}
