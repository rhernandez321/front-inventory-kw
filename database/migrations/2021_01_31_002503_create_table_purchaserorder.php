<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePurchaserorder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchaseorder', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('refcode', 30)->unique();
            $table->bigInteger('idprovider')->unsigned(); 
            $table->bigInteger('idcompany')->unsigned();
            $table->string('providername', 40);
            $table->string('provideraddress', 150);
            $table->decimal('totalcost', 18, 3)->default(0);
            $table->decimal('costofshipping', 18, 3)->default(0);
            $table->float('tax')->default(0);
            $table->bigInteger('idmoneytype')->unsigned();            
            $table->decimal('factormoney', 18, 3)->default(0);                        
            $table->boolean('canceled')->nullable();                        
            $table->dateTime('datecanceled')->nullabled();               
            
            $table->bigInteger('iduser')->unsigned();
            $table->bigInteger('idusercanceled')->unsigned()->nullabled();
            
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP(0)'));               
                     
            $table->foreign('iduser')->references('id')->on('users')->onUpdate('cascade');
            //Para guardar el usuario que canceló la orden
            $table->foreign('idusercanceled')->references('id')->on('users')->onUpdate('cascade');

            $table->foreign('idprovider')->references('id')->on('providers')->onUpdate('cascade');            
            $table->foreign('idcompany')->references('id')->on('companies')->onUpdate('cascade');            
            $table->foreign('idmoneytype')->references('id')->on('moneys')->onUpdate('cascade');
           
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchaseorder');
    }
}
