<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropForeignkeyTableMasterproducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_products', function (Blueprint $table) {
            //
            $table->dropForeign(['idprov']);
            $table->dropForeign(['idcol']);
            $table->dropForeign(['idpre']);
            $table->dropForeign(['idwh']);
            $table->dropForeign(['idcat']);
            $table->dropForeign(['idscat']);
            $table->dropForeign(['idrv']);
            
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_products', function (Blueprint $table) {
            //
           
        });
    }
}
