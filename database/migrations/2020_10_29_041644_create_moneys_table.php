<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoneysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moneys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 30)->unique();
            $table->string('description', 150)->nullable();
            $table->string('simbolo', 150)->nullable();
            $table->decimal('factor', 11, 2)->default(0);
            $table->boolean('enabled')->default(0);
            $table->boolean('visible')->default(0);
            $table->bigInteger('id_user')->unsigned();            
            
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP(0)'));
            
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moneys');
    }
}
