<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 30);
            $table->bigInteger('idprov')->unsigned(); //id proveedor
            $table->bigInteger('idcol')->unsigned()->nullable();  //id color           
            $table->bigInteger('idpre')->unsigned();  //idpresentación          
            $table->bigInteger('idwh')->unsigned();   //id wharehouse         
            $table->bigInteger('idcat')->unsigned()->nullable();  //id categoría          
            $table->bigInteger('idscat')->unsigned()->nullable(); //id subcategoría           
            $table->bigInteger('idrv')->unsigned()->nullable();   //id revisión         
            $table->string('name', 30);            
            $table->string('description', 250)->nullable();            
            $table->decimal('qtv', 18, 3)->default(0);            
            $table->decimal('cost', 18, 3)->default(0);            
            $table->decimal('price', 18, 3)->default(0);            
            $table->decimal('factormoney', 18, 3)->default(0);                        
            $table->boolean('enabled')->default(0);
            $table->boolean('visible')->default(0);

            $table->bigInteger('id_user')->unsigned();
            $table->bigInteger('id_moneytype')->unsigned();
            $table->bigInteger('id_status')->unsigned();
            $table->bigInteger('id_alloy')->unsigned()->nullable();
            $table->bigInteger('id_unit')->unsigned();
            //$table->bigInteger('id_typemovement')->unsigned();
            $table->bigInteger('id_location')->unsigned();
            
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP(0)'));

            $table->unique(array(
            'idprov',
            'idcol',
            'idpre',
            'idwh',
            'idcat',
            'idscat',
            'idrv'
        ));            
                     
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('idprov')->references('id')->on('providers')->onUpdate('cascade');            
            $table->foreign('idcol')->references('id')->on('colors')->onUpdate('cascade');
            $table->foreign('idpre')->references('id')->on('presentations')->onUpdate('cascade');
            $table->foreign('idwh')->references('id')->on('wharehouses')->onUpdate('cascade');
            $table->foreign('idcat')->references('id')->on('categorys')->onUpdate('cascade');
            $table->foreign('idscat')->references('id')->on('subcategorys')->onUpdate('cascade');
            $table->foreign('idrv')->references('id')->on('reviews')->onUpdate('cascade');
            $table->foreign('id_moneytype')->references('id')->on('moneys')->onUpdate('cascade');
            $table->foreign('id_status')->references('id')->on('status')->onUpdate('cascade');
            $table->foreign('id_alloy')->references('id')->on('alloys')->onUpdate('cascade');
            $table->foreign('id_unit')->references('id')->on('unittypes')->onUpdate('cascade');
            //$table->foreign('id_typemovement')->references('id')->on('movementstypes')->onUpdate('cascade');
            $table->foreign('id_location')->references('id')->on('locationbywharehouses')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_products');
    }
}
