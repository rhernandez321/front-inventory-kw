<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampoIdcompanyTablaReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reviews', function (Blueprint $table) {
            //

            $table->dropUnique(['name']);
            $table->bigInteger('id_company')->unsigned()->after('visible');                        
                        
            $table->unique(['name', 'id_company'], 'reviews_name_idcompany_unique');

            $table->foreign('id_company')->references('id')->on('companies')->onUpdate('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reviews', function (Blueprint $table) {
            //
        });
    }
}
