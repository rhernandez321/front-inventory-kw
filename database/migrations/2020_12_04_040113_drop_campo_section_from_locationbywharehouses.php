<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropCampoSectionFromLocationbywharehouses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locationbywharehouses', function (Blueprint $table) {
            //
            $table->dropUnique('name_section_idcompany_idwharehouse_unique');
            $table->dropColumn('section');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locationbywharehouses', function (Blueprint $table) {
            //
        });
    }
}
