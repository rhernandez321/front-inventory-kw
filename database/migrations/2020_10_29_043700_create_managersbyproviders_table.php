<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManagersbyprovidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('managersbyproviders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 30);
            $table->string('name', 30);
            $table->string('lastname', 30);
            $table->string('note', 250)->nullable();            
            $table->string('address', 150)->nullable();            
            $table->string('phone1', 20)->nullable();            
            $table->string('phone2', 20)->nullable();            
            $table->string('phone3', 20)->nullable();            
            $table->boolean('enabled')->default(0);
            $table->boolean('visible')->default(0);

            $table->bigInteger('id_user')->unsigned();            
            $table->bigInteger('id_provider')->unsigned();            
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP(0)'));

            $table->unique(array('id_provider', 'code'));            

                      
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('id_provider')->references('id')->on('providers')->onUpdate('cascade');            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('managersbyproviders');
    }
}
