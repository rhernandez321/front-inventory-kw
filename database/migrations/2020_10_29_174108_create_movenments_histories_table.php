<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovenmentsHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movements_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 30);                        
            $table->string('code', 30);                        
            $table->bigInteger('id_product')->unsigned();  
            $table->bigInteger('id_origin_wharehouse')->unsigned()->nullable();  
            $table->bigInteger('id_destination_wharehouse')->unsigned()->nullable();  
            $table->bigInteger('id_assembly')->unsigned()->nullable(); 
            $table->bigInteger('id_provider')->unsigned();
            $table->bigInteger('id_color')->unsigned()->nullable();  
            $table->bigInteger('id_presentation')->unsigned()->nullable();         
            $table->bigInteger('id_category')->unsigned()->nullable();
            $table->bigInteger('id_subcategory')->unsigned()->nullable();
            $table->bigInteger('id_review')->unsigned()->nullable();   
            $table->bigInteger('id_status')->unsigned()->nullable();   
            $table->bigInteger('id_alloy')->unsigned()->nullable();   
            $table->bigInteger('id_location')->unsigned()->nullable();
            $table->bigInteger('id_movementtype')->unsigned();
            $table->bigInteger('id_unittype')->unsigned();
            $table->bigInteger('id_moneytype')->unsigned();
            $table->decimal('qtv', 18, 3)->default(0);            
            $table->decimal('cost', 18, 3)->default(0);            
            $table->decimal('price', 18, 3)->default(0);            
            $table->decimal('moneyfactor', 18, 3)->default(0);                                                           
            
            $table->bigInteger('id_user')->unsigned();
                        
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP(0)'));               
                     
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade');
            
            $table->foreign('id_product')->references('id')->on('master_products')->onUpdate('cascade');
            $table->foreign('id_origin_wharehouse')->references('id')->on('wharehouses')->onUpdate('cascade');
            $table->foreign('id_destination_wharehouse')->references('id')->on('wharehouses')->onUpdate('cascade');            
            $table->foreign('id_provider')->references('id')->on('providers')->onUpdate('cascade');            
            $table->foreign('id_color')->references('id')->on('colors')->onUpdate('cascade');
            $table->foreign('id_presentation')->references('id')->on('presentations')->onUpdate('cascade');
            $table->foreign('id_assembly')->references('id')->on('assemblies')->onUpdate('cascade');
            $table->foreign('id_category')->references('id')->on('categorys')->onUpdate('cascade');
            $table->foreign('id_subcategory')->references('id')->on('subcategorys')->onUpdate('cascade');
            $table->foreign('id_review')->references('id')->on('reviews')->onUpdate('cascade');
            $table->foreign('id_moneytype')->references('id')->on('moneys')->onUpdate('cascade');
            $table->foreign('id_status')->references('id')->on('status')->onUpdate('cascade');
            $table->foreign('id_alloy')->references('id')->on('alloys')->onUpdate('cascade');
            $table->foreign('id_unittype')->references('id')->on('unittypes')->onUpdate('cascade');            
            $table->foreign('id_location')->references('id')->on('locationbywharehouses')->onUpdate('cascade');
            $table->foreign('id_movementtype')->references('id')->on('movementstypes')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movements_histories');
    }
}
