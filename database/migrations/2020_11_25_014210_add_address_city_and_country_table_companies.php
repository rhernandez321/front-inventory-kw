<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAddressCityAndCountryTableCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            //
            $table->string('address', 150)->after('description');
            $table->bigInteger('id_country')->unsigned()->after('address');                                                         
            $table->bigInteger('id_city')->unsigned()->after('id_country');

            $table->foreign('id_country')->references('id')->on('countries')->onUpdate('cascade');
            $table->foreign('id_city')->references('id')->on('citiesbycountries')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            //
        });
    }
}
