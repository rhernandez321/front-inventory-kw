<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssembliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assemblies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 30)->unique();
            $table->string('name', 30)->unique();
            $table->string('description', 150)->nullable();
            $table->decimal('precio', 18, 3)->default(0);            
            $table->decimal('costo', 18, 3)->default(0);            
            $table->boolean('enabled')->default(0);
            $table->boolean('visible')->default(0);
            $table->bigInteger('id_review')->unsigned();
            $table->bigInteger('id_color')->unsigned();
            $table->bigInteger('id_user')->unsigned();
            
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP(0)'));
            
            $table->foreign('id_review')->references('id')->on('reviews')->onUpdate('cascade'); 
            $table->foreign('id_color')->references('id')->on('colors')->onUpdate('cascade'); 
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assemblies');
    }
}
