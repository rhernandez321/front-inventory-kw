<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDetallesPurchaserorder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detailspurchaseorder', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idproduct')->unsigned();                        
            $table->string('productcode', 40);
            $table->string('productname', 40);                        
            $table->decimal('unitcost', 18, 3)->default(0);
            $table->integer('qtv')->default(0); 
            $table->integer('totalcost')->default(0);
            
            
            $table->foreign('idproduct')->references('id')->on('master_products')->onUpdate('cascade');            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detailspurchaseorder');
    }
}
