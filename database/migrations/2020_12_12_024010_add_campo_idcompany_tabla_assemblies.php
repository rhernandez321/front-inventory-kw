<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampoIdcompanyTablaAssemblies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assemblies', function (Blueprint $table) {
            //

            $table->bigInteger('id_company')->unsigned()->after('id_user');                        
            
            $table->unique(['code', 'id_company'], 'assemblies_code_idcompany_unique');
            $table->unique(['name', 'id_company'], 'assemblies_name_idcompany_unique');

            $table->foreign('id_company')->references('id')->on('companies')->onUpdate('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assemblies', function (Blueprint $table) {
            //
            $table->dropForeign(['id_company']);
            
            $table->dropUnique('assemblies_code_idcompany_unique');
            $table->dropUnique('assemblies_name_idcompany_unique');

            $table->dropColumn('id_company');
        });
    }
}
