<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampoIdcompanyTablaMovementshistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movements_histories', function (Blueprint $table) {
            //
            $table->bigInteger('id_company')->unsigned()->after('id_user');                        
            
            $table->foreign('id_company')->references('id')->on('companies')->onUpdate('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movements_histories', function (Blueprint $table) {
            //
            $table->dropForeign(['id_company']);
            $table->dropColumn('id_company');
        });
    }
}
