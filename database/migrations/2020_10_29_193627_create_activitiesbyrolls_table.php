<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitiesbyrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activitiesbyrolls', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->Integer('id_page')->unique();
            $table->string('id_object', 30);
            $table->string('action_name', 30)->unique();            
            $table->string('object_name', 50)->nullable();            
            $table->string('nota', 150)->nullable();                        
            $table->boolean('enabled')->default(0);
            $table->boolean('visible')->default(0);

            $table->bigInteger('id_user')->unsigned();                        
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP(0)'));

            $table->unique(array('id_page', 'id_object'));            
            $table->unique(array('id_page', 'action_name'));            
            $table->unique(array('id_page', 'object_name'));            

                      
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade');            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activitiesbyrolls');
    }
}
