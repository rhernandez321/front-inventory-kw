<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampoNrofacturaTablaMovementHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movements_histories', function (Blueprint $table) {
            //
            $table->bigInteger('nro_bill')->after('id_product');

            $table->unique(['nro_bill', 'id_product', 'id_company'], 'movementstypes_nrobill_id_company_product_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movements_histories', function (Blueprint $table) {
            //
            $table->dropUnique('movementstypes_nrobill_id_company_product_unique');
            $table->dropColumn('nro_bill');
        });
    }
}
