<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarCampoCodeTablaMovementstypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movementstypes', function (Blueprint $table) {
            //
            $table->string('code', 30)->after('id');
            
            $table->unique(['code'], 'movementstypes_code_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movementstypes', function (Blueprint $table) {
            //            
            $table->dropUnique('movementstypes_code_unique');
            $table->dropColumn('code');            
        });
    }
}
