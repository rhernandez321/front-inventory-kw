<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserByRollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_by_rolls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_rol')->unsigned();
            $table->bigInteger('id_user')->unsigned();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP(0)'));

            $table->foreign('id_rol')->references('id')->on('rolls')->onUpdate('cascade');
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade');
            $table->unique(array('id_rol', 'id_user'));
           

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_by_rolls');
    }
}
