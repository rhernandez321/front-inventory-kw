<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampoCompanyTableCategorys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorys', function (Blueprint $table) {
            //
            $table->bigInteger('id_company')->unsigned()->after('visible');                        
            
            $table->dropUnique(['code']);
            $table->dropUnique(['name']);

            $table->unique(array('id_company', 'code'));
            $table->unique(array('id_company', 'name'));
            $table->foreign('id_company')->references('id')->on('companies')->onUpdate('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorys', function (Blueprint $table) {
            //
            $table->dropForeign(['id_company']);
            $table->dropColumn('id_company');
        });
    }
}
