<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropUniquePostalcodeToCitiesbycountry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Eliminar condición de campo único a postalcode
        Schema::table('citiesbycountries', function (Blueprint $table) {
            $table->dropUnique(['postalcode']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('citiesbycountries', function (Blueprint $table) {
            //
        });
    }
}
