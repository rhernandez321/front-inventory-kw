<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampoIdcompanyTablaMoney extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('moneys', function (Blueprint $table) {
            //
           // $table->dropUnique(['code']);
            $table->bigInteger('id_company')->unsigned()->after('visible');                        
                        
            //$table->unique(['name', 'id_company'], 'moneys_name_idcompany_unique');
            $table->unique(['code', 'id_company'], 'moneys_code_idcompany_unique');

            $table->foreign('id_company')->references('id')->on('companies')->onUpdate('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('moneys', function (Blueprint $table) {
            //
            $table->dropForeign(['id_company']);
            
            $table->dropUnique( 'moneys_code_idcompany_unique');
            //$table->dropUnique('moneys_name_idcompany_unique');

            $table->dropColumn('id_company');
        });
    }
}
