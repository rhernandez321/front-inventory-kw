<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::resource('countries', 'CountriesController');
Route::resource('citiesbycountries', 'CitiesbyCountriesController');
Route::resource('colors', 'ColorsController');
Route::resource('moneys', 'MoneysController');
Route::resource('categories', 'CategoriesController');
Route::resource('subcategories', 'SubCategoriesController');
Route::resource('alloys', 'AlloysController');
Route::resource('colors', 'ColorsController');
Route::resource('moneys', 'MoneysController');
Route::resource('status', 'StatusController');
Route::resource('unittypes', 'UnitTypesController');
Route::resource('reviews', 'ReviewsController');
Route::resource('rolls', 'RollsController');
Route::resource('presentations', 'PresentationsController');
Route::resource('wharehouse', 'WhareHouseController');
Route::resource('companies', 'CompaniesController');
Route::resource('providers', 'ProvidersController');
Route::resource('assemblies', 'AssembliesController');
Route::resource('movementstypes', 'MovementstypesController');
Route::resource('managersbyproviders', 'ManagersbyProvidersController');
Route::resource('locationbywharehouses', 'LocationbyWharehousesController');
Route::resource('masterproducts', 'MasterProductsController');
Route::resource('sections', 'SectionsController');
Route::resource('purchaseorder', 'PurchaseOrderController');



Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/countries', 'HomeController@indexCountries')->name('countries');
//Route::get('/citiesbycountries', 'HomeController@indexCitiesbycountries')->name('citiesbycountries');


Route::post('countries/action', 'CountriesController@action')->name('countries.action');
Route::post('countries-research', 'CountriesController@countries_research')->name('countries_research');
Route::get('findcountry', 'CountriesController@findcountry')->name('findcountry');

Route::get('citiesbycountries', 'CitiesbyCountriesController@index')->name('citiesbycountries');
Route::post('citiesbycountries_research', 'CitiesbyCountriesController@citiesbycountries_research')->name('citiesbycountries_research');
Route::post('citiesbycountries_action', 'CitiesbyCountriesController@action')->name('citiesbycountries.action');

Route::post('categories_research', 'CategoriesController@categories_research')->name('categories_research');
Route::post('categories_action', 'CategoriesController@action')->name('categories.action');

Route::post('subcategories_research', 'SubCategoriesController@subcategories_research')->name('subcategories_research');
Route::post('subcategories_action', 'SubCategoriesController@action')->name('subcategories.action');

Route::post('alloys_research', 'AlloysController@alloys_research')->name('alloys_research');
Route::post('alloys_action', 'AlloysController@action')->name('alloys.action');

Route::post('colors_research', 'ColorsController@colors_research')->name('colors_research');
Route::post('colors_action', 'ColorsController@action')->name('colors.action');

Route::post('moneys_research', 'MoneysController@moneys_research')->name('moneys_research');
Route::post('moneys_action', 'MoneysController@action')->name('moneys.action');

Route::post('status_research', 'StatusController@status_research')->name('status_research');
Route::post('status_action', 'StatusController@action')->name('status.action');

Route::post('unittypes_research', 'UnitTypesController@unittypes_research')->name('unittypes_research');
Route::post('unittypes_action', 'UnitTypesController@action')->name('unittypes.action');

Route::post('reviews_research', 'ReviewsController@reviews_research')->name('reviews_research');
Route::post('reviews_action', 'ReviewsController@action')->name('reviews.action');

Route::post('rolls_research', 'RollsController@rolls_research')->name('rolls_research');
Route::post('rolls_action', 'RollsController@action')->name('rolls.action');

Route::post('presentations_research', 'PresentationsController@presentations_research')->name('presentations_research');
Route::post('presentations_action', 'PresentationsController@action')->name('presentations.action');

Route::post('wharehouse_research', 'WhareHouseController@research')->name('wharehouse_research');
Route::post('wharehouse_action', 'WhareHouseController@action')->name('wharehouse.action');

Route::post('companies_research', 'CompaniesController@research')->name('companies_research');
Route::post('companies_action', 'CompaniesController@action')->name('companies.action');
Route::post('companies_citiesbycountries', 'CompaniesController@citiesbycountries')->name('companies_citiesbycountries');

Route::post('providers_research', 'ProvidersController@research')->name('providers_research');
Route::post('providers_action', 'ProvidersController@action')->name('providers.action');
Route::post('providers_citiesbycountries', 'ProvidersController@findcitiesbycountries')->name('providers_citiesbycountries');

Route::post('movementstypes_research', 'MovementstypesController@research')->name('movementstypes_research');
Route::post('movementstypes_action', 'MovementstypesController@action')->name('movementstypes.action');
Route::post('movementstypes_exist', 'MovementstypesController@movementstypes_exist')->name('movementstypes_exist');

Route::post('managersbyproviders_research', 'ManagersbyProvidersController@research')->name('managersbyproviders_research');
Route::post('managersbyproviders_action', 'ManagersbyProvidersController@action')->name('managersbyproviders.action');
Route::post('wharehousebycompany', 'ManagersbyProvidersController@wharehousebycompany')->name('wharehousebycompany');

Route::post('locationbywharehouses_research', 'LocationbyWharehousesController@research')->name('locationbywharehouses_research');
Route::post('locationbywharehouses_action', 'LocationbyWharehousesController@action')->name('locationbywharehouses.action');
//Route::post('wharehousebycompany', 'LocationbyWharehousesController@wharehousebycompany')->name('wharehousebycompany');

Route::post('masterproducts_research', 'MasterProductsController@research')->name('masterproducts_research');
Route::post('masterproducts_action', 'MasterProductsController@action')->name('masterproducts.action');
Route::post('masterproducts_search', 'MasterProductsController@products_search')->name('masterproducts_search');
Route::post('masterproducts_subcategories', 'MasterProductsController@subcategories')->name('masterproducts_subcategories');
Route::post('masterproducts_locations', 'MasterProductsController@locations')->name('masterproducts_locations');
Route::post('masterproducts_sections', 'MasterProductsController@sections')->name('masterproducts_sections');
Route::post('masterproducts_action', 'MasterProductsController@action')->name('masterproducts.action');

Route::post('sections_research', 'SectionsController@research')->name('sections_research');
Route::post('sections_action', 'SectionsController@action')->name('sections.action');
Route::post('sections_locationsbywharehousesandcompany', 'SectionsController@locationsbywharehouseandcompany')->name('sections_locationsbywharehousesandcompany');

Route::post('assemblies_research', 'AssembliesController@research')->name('assemblies_research');
Route::post('assemblies_action', 'AssembliesController@action')->name('assemblies.action');

Route::post('purcharseorderproducts', 'PurchaseOrderController@searchProducts')->name('purcharseorderproducts');
Route::post('purcharseorderproviders', 'PurchaseOrderController@searchProviders')->name('purcharseorderproviders');
Route::post('purcharseorder_action', 'PurchaseOrderController@action')->name('purcharseorder.action');
Route::post('purcharseorder_actiondetails', 'PurchaseOrderController@actiondetails')->name('purcharseorder.actiondetails');



